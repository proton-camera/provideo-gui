#!/usr/bin/env groovy

// Before checking in this file you should validate the syntax with:
// curl --user $USER -X POST -F "jenkinsfile=<Jenkinsfile" http://jenkins.office.dreamchip.de/pipeline-model-converter/validate

pipeline {
    agent {
        docker {
            label 'docker && location-gbs && provideo-products'
            image 'docker-de-gbs01.office.dreamchip.de/dreamchip/provideo:bullseye-slim'
            args '''
                -v ${WORKSPACE}/:/projects
                -e PATH="/opt/mxe/usr/bin:${PATH}"
                -e USERINFO="jenkins:2139 jenkins-group:10101"
                --privileged
            '''
            alwaysPull true
        }
    }
    options {
        allowBrokenBuildClaiming()
        buildDiscarder logRotator(artifactDaysToKeepStr: '',
                                  artifactNumToKeepStr: '',
                                  daysToKeepStr: '',
                                  numToKeepStr: '')
        disableConcurrentBuilds()
        quietPeriod 300
    }
    triggers {
        pollSCM 'H/15 * * * *'
    }
    stages {
        stage('build linux debug') {
            steps {
                dir("build-linux-debug") {
                    sh "rm -rf *"
                    sh "cmake -DCMAKE_BUILD_TYPE=Debug -G Ninja -S ../application -B ."
                    sh "ninja -j 4"
                }
            }
        }
        stage('build mxe release') {
            steps {
                dir("build-mxe-release") {
                    sh '''
                         rm -rf *
                         i686-w64-mingw32.static-cmake --no-warn-unused-cli -G Ninja -S ../application/ -B ./
                         ninja -j 4
                       '''
                }
            }
            post {
                success {
                    // create deploy directory => easier for archiveArtefacts
                    dir("build-mxe-release") {
                        sh "mkdir deploy"
                        sh "cp ProVideo.exe deploy/"
                        sh "cp ../doc/user_manual.pdf 'deploy/ProVideo GUI Manual.pdf'"
                        sh "mkdir deploy/tools_and_configs"
                        sh "cp ../application/tools_and_configs/flashloader.exe deploy/tools_and_configs/"
                        sh "cp ../application/tools_and_configs/SupportedLenses.txt deploy/tools_and_configs/"
                        dir("deploy") {
                            archiveArtifacts "*"
                            archiveArtifacts "tools_and_configs/*"
                        }
                    }
                }
            }
        }
        stage('lint') {
            steps {
                dir("build-linux-debug") {
                    catchError(buildResult: 'UNSTABLE', stageResult: 'FAILURE') {
                        sh "ninja lint"
                    }
                }
            }
        }
    }
    post {
        always {
            // publish warnings
            recordIssues ignoreQualityGate: true, \
            tools: [clang(), cmake()],            \
            trendChartType: 'TOOLS_ONLY'
        }
        changed {
            script {
                if (currentBuild.currentResult == 'SUCCESS') {
                    emailext(
                        subject: '[Fixed] ${DEFAULT_SUBJECT}',
                        body: '${DEFAULT_CONTENT}',
                        recipientProviders: [culprits()],
                        to: '${DEFAULT_RECIPIENTS}',
                        replyTo: '${DEFAULT_REPLYTO}'
                    )
                }
                else {
                    emailext(
                        subject: '${DEFAULT_SUBJECT}',
                        body: '${DEFAULT_CONTENT}',
                        recipientProviders: [culprits()],
                        to: '${DEFAULT_RECIPIENTS}',
                        replyTo: '${DEFAULT_REPLYTO}'
                    )
                }
            }
        }
        success {
            // change displayName to the tag, if the current version has a tag
            catchError(buildResult: 'SUCCESS', stageResult: 'SUCCESS') {
                script {
                    MY_TAG = sh(script: "git describe --tags --exact-match ${env.GIT_COMMIT}", returnStdout: true).trim()
                    currentBuild.displayName = "${MY_TAG}"
                    currentBuild.description = ""
                    currentBuild.keepLog = true
                }
            }
        }
    }
}
