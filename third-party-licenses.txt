THIRD-PARTY SOFTWARE LICENSES

-------- Qt Framework --------

Qt is licensed under GPLv3, for more information view:
https://www.qt.io/download-open-source

-------- QCustomPlot --------

QCustomPlot is licensed under GPLv3, for more information view:
http://qcustomplot.com/

The license file can be found in:
./libraries/qcustomplot/GPL.txt

This license applies to all files in the following folder:
./libraries/qcustomplot/

-------- csvparser / csvwriter --------

The csvparser and csvwriter are available for free without any restrictions on sourceforge:
https://sourceforge.net/projects/cccsvparser/
https://sourceforge.net/projects/cccsvwriter/

This applies to the following files:
./libraries/include/csv/csvparser.h
./libraries/csv/csvparser.c
./libraries/include/csv/csvwriter.h
./libraries/csv/csvwriter.c

-------- embUnit --------

embUnit is licensed under the MIT license, for more informatin view:
https://sourceforge.net/projects/embunit/

-------- QDarkStyleSheet --------

This program uses the "Dark Style" style sheet by Colin Duquesnoy which is licensed under MIT license and can be obtained on GitHub:
https://github.com/ColinDuquesnoy/QDarkStyleSheet

This applies to the files under:
./resource/style/

-------- Qt-RangeSlider --------

Qt-RangeSlider is licensed under the MIT license, for more information view:
https://github.com/ThisIsClark/Qt-RangeSlider

The license file can be found in:
./libraries/rangeslider/LICENSE

This license applies to all files in the following folder:
./libraries/rangeslider/
