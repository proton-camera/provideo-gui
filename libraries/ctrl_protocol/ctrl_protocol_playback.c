/*
 * Copyright (C) 2017 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    ctrl_protocol_playback.c
 *
 * @brief   Implementation of generic playback engine functions
 *
 *****************************************************************************/
#include <stdio.h>
#include <errno.h>

#include <ctrl_channel/ctrl_channel.h>
#include <ctrl_protocol/ctrl_protocol.h>

#include "ctrl_protocol_priv.h"

/**
 * @brief Macro for type-casting to function driver
 *****************************************************************************/
#define PLAYBACK_DRV(drv) ((ctrl_protocol_playback_drv_t *)(drv))

/*
 * ctrl_protocol_get_rec
 *****************************************************************************/
int ctrl_protocol_get_rec(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                          uint8_t *const id)
{
    CHECK_HANDLE(protocol)
    CHECK_DRV_FUNC(PLAYBACK_DRV(protocol->drv), get_rec)
    CHECK_NOT_NULL(id)
    return (PLAYBACK_DRV(protocol->drv)->get_rec(protocol->ctx, channel, id));
}

/*
 * ctrl_protocol_set_rec
 *****************************************************************************/
int ctrl_protocol_set_rec(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                          uint8_t const id)
{
    CHECK_HANDLE(protocol)
    CHECK_DRV_FUNC(PLAYBACK_DRV(protocol->drv), set_rec)
    return (PLAYBACK_DRV(protocol->drv)->set_rec(protocol->ctx, channel, id));
}

/*
 * ctrl_protocol_get_rec_mode
 *****************************************************************************/
int ctrl_protocol_get_rec_mode(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                               uint8_t *const mode)
{
    CHECK_HANDLE(protocol)
    CHECK_DRV_FUNC(PLAYBACK_DRV(protocol->drv), get_rec_mode)
    CHECK_NOT_NULL(mode)
    return (PLAYBACK_DRV(protocol->drv)->get_rec_mode(protocol->ctx, channel, mode));
}

/*
 * ctrl_protocol_set_rec_mode
 *****************************************************************************/
int ctrl_protocol_set_rec_mode(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                               uint8_t const mode)
{
    CHECK_HANDLE(protocol)
    CHECK_DRV_FUNC(PLAYBACK_DRV(protocol->drv), set_rec_mode)
    return (PLAYBACK_DRV(protocol->drv)->set_rec_mode(protocol->ctx, channel, mode));
}

/*
 * ctrl_protocol_rec_stop
 *****************************************************************************/
int ctrl_protocol_rec_stop(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel)
{
    CHECK_HANDLE(protocol)
    CHECK_DRV_FUNC(PLAYBACK_DRV(protocol->drv), rec_stop)
    return (PLAYBACK_DRV(protocol->drv)->rec_stop(protocol->ctx, channel));
}

/*
 * ctrl_protocol_get_play
 *****************************************************************************/
int ctrl_protocol_get_play(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                           int const no, int16_t *const values)
{
    CHECK_HANDLE(protocol)
    CHECK_DRV_FUNC(PLAYBACK_DRV(protocol->drv), get_play)
    CHECK_NOT_NULL(no)
    CHECK_NOT_NULL(values)
    return (PLAYBACK_DRV(protocol->drv)->get_play(protocol->ctx, channel, no, values));
}

/*
 * ctrl_protocol_set_play
 *****************************************************************************/
int ctrl_protocol_set_play(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                           int const no, int16_t *const values)
{
    CHECK_HANDLE(protocol)
    CHECK_DRV_FUNC(PLAYBACK_DRV(protocol->drv), set_play)
    CHECK_NOT_NULL(no)
    CHECK_NOT_NULL(values)
    return (PLAYBACK_DRV(protocol->drv)->set_play(protocol->ctx, channel, no, values));
}

/*
 * ctrl_protocol_get_play_mode
 *****************************************************************************/
int ctrl_protocol_get_play_mode(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                uint8_t *const mode)
{
    CHECK_HANDLE(protocol)
    CHECK_DRV_FUNC(PLAYBACK_DRV(protocol->drv), get_play_mode)
    CHECK_NOT_NULL(mode)
    return (PLAYBACK_DRV(protocol->drv)->get_play_mode(protocol->ctx, channel, mode));
}

/*
 * ctrl_protocol_set_play_mode
 *****************************************************************************/
int ctrl_protocol_set_play_mode(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                uint8_t const mode)
{
    CHECK_HANDLE(protocol)
    CHECK_DRV_FUNC(PLAYBACK_DRV(protocol->drv), set_play_mode)
    return (PLAYBACK_DRV(protocol->drv)->set_play_mode(protocol->ctx, channel, mode));
}

/*
 * ctrl_protocol_pause
 *****************************************************************************/
int ctrl_protocol_pause(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel)
{
    CHECK_HANDLE(protocol)
    CHECK_DRV_FUNC(PLAYBACK_DRV(protocol->drv), pause)
    return (PLAYBACK_DRV(protocol->drv)->pause(protocol->ctx, channel));
}

/*
 * ctrl_protocol_stop
 *****************************************************************************/
int ctrl_protocol_stop(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel)
{
    CHECK_HANDLE(protocol)
    CHECK_DRV_FUNC(PLAYBACK_DRV(protocol->drv), stop)
    return (PLAYBACK_DRV(protocol->drv)->stop(protocol->ctx, channel));
}

/*
 * ctrl_protocol_get_stop_mode
 *****************************************************************************/
int ctrl_protocol_get_stop_mode(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                uint8_t *const mode)
{
    CHECK_HANDLE(protocol)
    CHECK_DRV_FUNC(PLAYBACK_DRV(protocol->drv), get_stop_mode)
    CHECK_NOT_NULL(mode)
    return (PLAYBACK_DRV(protocol->drv)->get_stop_mode(protocol->ctx, channel, mode));
}

/*
 * ctrl_protocol_set_stop_mode
 *****************************************************************************/
int ctrl_protocol_set_stop_mode(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                uint8_t const mode)
{
    CHECK_HANDLE(protocol)
    CHECK_DRV_FUNC(PLAYBACK_DRV(protocol->drv), set_stop_mode)
    return (PLAYBACK_DRV(protocol->drv)->set_stop_mode(protocol->ctx, channel, mode));
}

/*
 * ctrl_protocol_set_seek
 *****************************************************************************/
int ctrl_protocol_set_seek(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                           int const no, int32_t *const values)
{
    CHECK_HANDLE(protocol)
    CHECK_DRV_FUNC(PLAYBACK_DRV(protocol->drv), set_seek)
    CHECK_NOT_NULL(no)
    CHECK_NOT_NULL(values)
    return (PLAYBACK_DRV(protocol->drv)->set_seek(protocol->ctx, channel, no, values));
}

/*
 * ctrl_protocol_get_pos
 *****************************************************************************/
int ctrl_protocol_get_pos(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                          uint32_t *const pos)
{
    CHECK_HANDLE(protocol)
    CHECK_DRV_FUNC(PLAYBACK_DRV(protocol->drv), get_pos)
    CHECK_NOT_NULL(pos)
    return (PLAYBACK_DRV(protocol->drv)->get_pos(protocol->ctx, channel, pos));
}

/*
 * ctrl_protocol_mark_in
 *****************************************************************************/
int ctrl_protocol_mark_in(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                          uint32_t const pos)
{
    CHECK_HANDLE(protocol)
    CHECK_DRV_FUNC(PLAYBACK_DRV(protocol->drv), mark_in)
    return (PLAYBACK_DRV(protocol->drv)->mark_in(protocol->ctx, channel, pos));
}

/*
 * ctrl_protocol_mark_out
 *****************************************************************************/
int ctrl_protocol_mark_out(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                           uint32_t const pos)
{
    CHECK_HANDLE(protocol)
    CHECK_DRV_FUNC(PLAYBACK_DRV(protocol->drv), mark_out)
    return (PLAYBACK_DRV(protocol->drv)->mark_out(protocol->ctx, channel, pos));
}

/*
 * ctrl_protocol_get_mark_pos
 *****************************************************************************/
int ctrl_protocol_get_mark_pos(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                               int const no, uint32_t *const values)
{
    CHECK_HANDLE(protocol)
    CHECK_DRV_FUNC(PLAYBACK_DRV(protocol->drv), get_mark_pos)
    CHECK_NOT_NULL(no)
    CHECK_NOT_NULL(values)
    return (PLAYBACK_DRV(protocol->drv)->get_mark_pos(protocol->ctx, channel, no, values));
}

/*
 * ctrl_protocol_get_status
 *****************************************************************************/
int ctrl_protocol_get_status(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                             int const no, uint8_t *const values)
{
    CHECK_HANDLE(protocol)
    CHECK_DRV_FUNC(PLAYBACK_DRV(protocol->drv), get_status)
    CHECK_NOT_NULL(no)
    CHECK_NOT_NULL(values)
    return (PLAYBACK_DRV(protocol->drv)->get_status(protocol->ctx, channel, no, values));
}

/*
 * ctrl_protocol_set_free
 *****************************************************************************/
int ctrl_protocol_set_free(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                           uint8_t const id)
{
    CHECK_HANDLE(protocol)
    CHECK_DRV_FUNC(PLAYBACK_DRV(protocol->drv), set_free)
    return (PLAYBACK_DRV(protocol->drv)->set_free(protocol->ctx, channel, id));
}

/*
 * ctrl_protocol_get_count
 *****************************************************************************/
int ctrl_protocol_get_count(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                            uint8_t *const num)
{
    CHECK_HANDLE(protocol)
    CHECK_DRV_FUNC(PLAYBACK_DRV(protocol->drv), get_count)
    CHECK_NOT_NULL(num)
    return (PLAYBACK_DRV(protocol->drv)->get_count(protocol->ctx, channel, num));
}

/*
 * ctrl_protocol_set_count
 *****************************************************************************/
int ctrl_protocol_set_count(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                            uint8_t const num)
{
    CHECK_HANDLE(protocol)
    CHECK_DRV_FUNC(PLAYBACK_DRV(protocol->drv), set_count)
    return (PLAYBACK_DRV(protocol->drv)->set_count(protocol->ctx, channel, num));
}

/*
 * ctrl_protocol_set_count
 *****************************************************************************/
int ctrl_protocol_get_health(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                             int const no, uint8_t *const values)
{
    CHECK_HANDLE(protocol)
    CHECK_DRV_FUNC(PLAYBACK_DRV(protocol->drv), get_health)
    CHECK_NOT_NULL(no)
    CHECK_NOT_NULL(values)
    return (PLAYBACK_DRV(protocol->drv)->get_health(protocol->ctx, channel, no, values));
}

/*
 * ctrl_protocol_playback_register
 *****************************************************************************/
int ctrl_protocol_playback_register(ctrl_protocol_handle_t handle, void *const ctx,
                                    ctrl_protocol_playback_drv_t *const drv)
{
    CHECK_HANDLE(handle)

    handle->drv = drv;
    handle->ctx = ctx;

    return (0);
}

/*
 * ctrl_protocol_playback_unregister
 *****************************************************************************/
int ctrl_protocol_playback_unregister(ctrl_protocol_handle_t handle)
{
    CHECK_HANDLE(handle)

    handle->drv = NULL;
    handle->ctx = NULL;

    return (0);
}
