/*
 * Copyright (C) 2019 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    ctrl_protocol_lens.c
 *
 * @brief   Implementation of lens control functions
 *
 *****************************************************************************/
#include <stdio.h>
#include <errno.h>

#include <ctrl_channel/ctrl_channel.h>
#include <ctrl_protocol/ctrl_protocol.h>

#include "ctrl_protocol_priv.h"

/**
 * @brief Macro for type-casting to function driver
 *****************************************************************************/
#define LENS_DRV(drv) ((ctrl_protocol_lens_drv_t *)(drv))

/*
 * ctrl_protocol_get_lens_settings
 *****************************************************************************/
int ctrl_protocol_get_lens_settings(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                    int num, int32_t *values)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), get_lens_settings);
    CHECK_NOT_NULL(values);
    return (LENS_DRV(protocol->drv)->get_lens_settings(protocol->ctx, channel, num, values));
}

/*
 * ctrl_protocol_set_lens_settings
 *****************************************************************************/
int ctrl_protocol_set_lens_settings(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                    int num, int32_t *values)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), set_lens_settings);
    return (LENS_DRV(protocol->drv)->set_lens_settings(protocol->ctx, channel, num, values));
}

/*
 * ctrl_protocol_get_lens_active
 *****************************************************************************/
int ctrl_protocol_get_lens_active(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                  int32_t *act)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), get_lens_active);
    CHECK_NOT_NULL(act);
    return (LENS_DRV(protocol->drv)->get_lens_active(protocol->ctx, channel, act));
}

/*
 * ctrl_protocol_set_lens_active
 *****************************************************************************/
int ctrl_protocol_set_lens_active(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                  int32_t act)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), set_lens_active);
    return (LENS_DRV(protocol->drv)->set_lens_active(protocol->ctx, channel, act));
}

/*
 * ctrl_protocol_get_lens_invert
 *****************************************************************************/
int ctrl_protocol_get_lens_invert(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                  int num, int32_t *values)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), get_lens_invert);
    CHECK_NOT_NULL(values);
    return (LENS_DRV(protocol->drv)->get_lens_invert(protocol->ctx, channel, num, values));
}

/*
 * ctrl_protocol_set_lens_invert
 *****************************************************************************/
int ctrl_protocol_set_lens_invert(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                  int num, int32_t *values)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), set_lens_invert);
    return (LENS_DRV(protocol->drv)->set_lens_invert(protocol->ctx, channel, num, values));
}

/*
 * ctrl_protocol_get_lens_auto_torque
 *****************************************************************************/
int ctrl_protocol_get_lens_auto_torque(ctrl_protocol_handle_t protocol,
                                       ctrl_channel_handle_t channel, uint8_t *enable)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), get_lens_auto_torque);
    CHECK_NOT_NULL(enable);
    return (LENS_DRV(protocol->drv)->get_lens_auto_torque(protocol->ctx, channel, enable));
}

/*
 * ctrl_protocol_set_lens_auto_torque
 *****************************************************************************/
int ctrl_protocol_set_lens_auto_torque(ctrl_protocol_handle_t protocol,
                                       ctrl_channel_handle_t channel, uint8_t enable)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), set_lens_auto_torque);
    return (LENS_DRV(protocol->drv)->set_lens_auto_torque(protocol->ctx, channel, enable));
}

/*
 * ctrl_protocol_get_lens_focus_motor_position
 *****************************************************************************/
int ctrl_protocol_get_lens_focus_motor_position(ctrl_protocol_handle_t protocol,
                                                ctrl_channel_handle_t channel, int32_t *pos)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), get_lens_focus_motor_position);
    CHECK_NOT_NULL(pos);
    return (LENS_DRV(protocol->drv)->get_lens_focus_motor_position(protocol->ctx, channel, pos));
}

/*
 * ctrl_protocol_set_lens_focus_motor_position
 *****************************************************************************/
int ctrl_protocol_set_lens_focus_motor_position(ctrl_protocol_handle_t protocol,
                                                ctrl_channel_handle_t channel, int32_t pos)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), set_lens_focus_motor_position);
    return (LENS_DRV(protocol->drv)->set_lens_focus_motor_position(protocol->ctx, channel, pos));
}

/*
 * ctrl_protocol_get_lens_focus_position
 *****************************************************************************/
int ctrl_protocol_get_lens_focus_position(ctrl_protocol_handle_t protocol,
                                          ctrl_channel_handle_t channel, int32_t *pos)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), get_lens_focus_position);
    CHECK_NOT_NULL(pos);
    return (LENS_DRV(protocol->drv)->get_lens_focus_position(protocol->ctx, channel, pos));
}

/*
 * ctrl_protocol_set_lens_focus_position
 *****************************************************************************/
int ctrl_protocol_set_lens_focus_position(ctrl_protocol_handle_t protocol,
                                          ctrl_channel_handle_t channel, int32_t pos)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), set_lens_focus_position);
    return (LENS_DRV(protocol->drv)->set_lens_focus_position(protocol->ctx, channel, pos));
}

/*
 * ctrl_protocol_get_lens_focus_settings
 *****************************************************************************/
int ctrl_protocol_get_lens_focus_settings(ctrl_protocol_handle_t protocol,
                                          ctrl_channel_handle_t channel, int num, int32_t *values)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), get_lens_focus_settings);
    CHECK_NOT_NULL(values);
    return (LENS_DRV(protocol->drv)->get_lens_focus_settings(protocol->ctx, channel, num, values));
}

/*
 * ctrl_protocol_set_lens_focus_settings
 *****************************************************************************/
int ctrl_protocol_set_lens_focus_settings(ctrl_protocol_handle_t protocol,
                                          ctrl_channel_handle_t channel, int num, int32_t *values)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), set_lens_focus_settings);
    return (LENS_DRV(protocol->drv)->set_lens_focus_settings(protocol->ctx, channel, num, values));
}

/*
 * ctrl_protocol_get_lens_fine_focus
 *****************************************************************************/
int ctrl_protocol_get_lens_fine_focus(ctrl_protocol_handle_t protocol,
                                      ctrl_channel_handle_t channel, uint8_t *enable)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), get_lens_fine_focus);
    CHECK_NOT_NULL(enable);
    return (LENS_DRV(protocol->drv)->get_lens_fine_focus(protocol->ctx, channel, enable));
}

/*
 * ctrl_protocol_set_lens_fine_focus
 *****************************************************************************/
int ctrl_protocol_set_lens_fine_focus(ctrl_protocol_handle_t protocol,
                                      ctrl_channel_handle_t channel, uint8_t enable)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), set_lens_fine_focus);
    return (LENS_DRV(protocol->drv)->set_lens_fine_focus(protocol->ctx, channel, enable));
}

/*
 * ctrl_protocol_get_lens_zoom_position
 *****************************************************************************/
int ctrl_protocol_get_lens_zoom_position(ctrl_protocol_handle_t protocol,
                                         ctrl_channel_handle_t channel, int32_t *pos)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), get_lens_zoom_position);
    CHECK_NOT_NULL(pos);
    return (LENS_DRV(protocol->drv)->get_lens_zoom_position(protocol->ctx, channel, pos));
}

/*
 * ctrl_protocol_set_lens_zoom_position
 *****************************************************************************/
int ctrl_protocol_set_lens_zoom_position(ctrl_protocol_handle_t protocol,
                                         ctrl_channel_handle_t channel, int32_t pos)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), set_lens_zoom_position);
    return (LENS_DRV(protocol->drv)->set_lens_zoom_position(protocol->ctx, channel, pos));
}

/*
 * ctrl_protocol_get_lens_zoom_direction
 *****************************************************************************/
int ctrl_protocol_get_lens_zoom_direction(ctrl_protocol_handle_t protocol,
                                          ctrl_channel_handle_t channel, int32_t *dir)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), get_lens_zoom_direction);
    CHECK_NOT_NULL(dir);
    return (LENS_DRV(protocol->drv)->get_lens_zoom_direction(protocol->ctx, channel, dir));
}

/*
 * ctrl_protocol_set_lens_zoom_direction
 *****************************************************************************/
int ctrl_protocol_set_lens_zoom_direction(ctrl_protocol_handle_t protocol,
                                          ctrl_channel_handle_t channel, int32_t dir)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), set_lens_zoom_direction);
    return (LENS_DRV(protocol->drv)->set_lens_zoom_direction(protocol->ctx, channel, dir));
}

/*
 * ctrl_protocol_get_lens_zoom_settings
 *****************************************************************************/
int ctrl_protocol_get_lens_zoom_settings(ctrl_protocol_handle_t protocol,
                                         ctrl_channel_handle_t channel, int num, int32_t *values)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), get_lens_zoom_settings);
    CHECK_NOT_NULL(values);
    return (LENS_DRV(protocol->drv)->get_lens_zoom_settings(protocol->ctx, channel, num, values));
}

/*
 * ctrl_protocol_set_lens_zoom_settings
 *****************************************************************************/
int ctrl_protocol_set_lens_zoom_settings(ctrl_protocol_handle_t protocol,
                                         ctrl_channel_handle_t channel, int num, int32_t *values)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), set_lens_zoom_settings);
    return (LENS_DRV(protocol->drv)->set_lens_zoom_settings(protocol->ctx, channel, num, values));
}

/*
 * ctrl_protocol_get_lens_fine_zoom
 *****************************************************************************/
int ctrl_protocol_get_lens_fine_zoom(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                     uint8_t *enable)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), get_lens_fine_zoom);
    CHECK_NOT_NULL(enable);
    return (LENS_DRV(protocol->drv)->get_lens_fine_zoom(protocol->ctx, channel, enable));
}

/*
 * ctrl_protocol_set_lens_fine_zoom
 *****************************************************************************/
int ctrl_protocol_set_lens_fine_zoom(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                     uint8_t enable)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), set_lens_fine_zoom);
    return (LENS_DRV(protocol->drv)->set_lens_fine_zoom(protocol->ctx, channel, enable));
}

/*
 * ctrl_protocol_get_lens_iris_position
 *****************************************************************************/
int ctrl_protocol_get_lens_iris_position(ctrl_protocol_handle_t protocol,
                                         ctrl_channel_handle_t channel, int32_t *pos)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), get_lens_iris_position);
    CHECK_NOT_NULL(pos);
    return (LENS_DRV(protocol->drv)->get_lens_iris_position(protocol->ctx, channel, pos));
}

/*
 * ctrl_protocol_set_lens_iris_position
 *****************************************************************************/
int ctrl_protocol_set_lens_iris_position(ctrl_protocol_handle_t protocol,
                                         ctrl_channel_handle_t channel, int32_t pos)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), set_lens_iris_position);
    return (LENS_DRV(protocol->drv)->set_lens_iris_position(protocol->ctx, channel, pos));
}

/*
 * ctrl_protocol_get_lens_fine_iris
 *****************************************************************************/
int ctrl_protocol_get_lens_fine_iris(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                     uint8_t *enable)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), get_lens_fine_iris);
    CHECK_NOT_NULL(enable);
    return (LENS_DRV(protocol->drv)->get_lens_fine_iris(protocol->ctx, channel, enable));
}

/*
 * ctrl_protocol_set_lens_fine_iris
 *****************************************************************************/
int ctrl_protocol_set_lens_fine_iris(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                     uint8_t enable)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), set_lens_fine_iris);
    return (LENS_DRV(protocol->drv)->set_lens_fine_iris(protocol->ctx, channel, enable));
}

/*
 * ctrl_protocol_get_lens_iris_settings
 *****************************************************************************/
int ctrl_protocol_get_lens_iris_settings(ctrl_protocol_handle_t protocol,
                                         ctrl_channel_handle_t channel, int num, int32_t *values)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), get_lens_iris_settings);
    CHECK_NOT_NULL(values);
    return (LENS_DRV(protocol->drv)->get_lens_iris_settings(protocol->ctx, channel, num, values));
}

/*
 * ctrl_protocol_set_lens_iris_settings
 *****************************************************************************/
int ctrl_protocol_set_lens_iris_settings(ctrl_protocol_handle_t protocol,
                                         ctrl_channel_handle_t channel, int num, int32_t *values)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), set_lens_iris_settings);
    return (LENS_DRV(protocol->drv)->set_lens_iris_settings(protocol->ctx, channel, num, values));
}

/*
 * ctrl_protocol_get_lens_iris_aperture
 *****************************************************************************/
int ctrl_protocol_get_lens_iris_aperture(ctrl_protocol_handle_t protocol,
                                         ctrl_channel_handle_t channel, int32_t *apt)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), get_lens_iris_aperture);
    CHECK_NOT_NULL(apt);
    return (LENS_DRV(protocol->drv)->get_lens_iris_aperture(protocol->ctx, channel, apt));
}

/*
 * ctrl_protocol_set_lens_iris_aperture
 *****************************************************************************/
int ctrl_protocol_set_lens_iris_aperture(ctrl_protocol_handle_t protocol,
                                         ctrl_channel_handle_t channel, int32_t apt)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), set_lens_iris_aperture);
    return (LENS_DRV(protocol->drv)->set_lens_iris_aperture(protocol->ctx, channel, apt));
}

/*
 * ctrl_protocol_get_lens_iris_table
 *****************************************************************************/
int ctrl_protocol_get_lens_iris_table(ctrl_protocol_handle_t protocol,
                                      ctrl_channel_handle_t channel, int num, int32_t *values)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), get_lens_iris_table);
    CHECK_NOT_NULL(values);
    return (LENS_DRV(protocol->drv)->get_lens_iris_table(protocol->ctx, channel, num, values));
}

/*
 * ctrl_protocol_set_lens_iris_table
 *****************************************************************************/
int ctrl_protocol_set_lens_iris_table(ctrl_protocol_handle_t protocol,
                                      ctrl_channel_handle_t channel, int num, int32_t *values)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), set_lens_iris_table);
    return (LENS_DRV(protocol->drv)->set_lens_iris_table(protocol->ctx, channel, num, values));
}

/*
 * ctrl_protocol_get_lens_filter_position
 *****************************************************************************/
int ctrl_protocol_get_lens_filter_position(ctrl_protocol_handle_t protocol,
                                           ctrl_channel_handle_t channel, int32_t *pos)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), get_lens_filter_position);
    CHECK_NOT_NULL(pos);
    return (LENS_DRV(protocol->drv)->get_lens_filter_position(protocol->ctx, channel, pos));
}

/*
 * ctrl_protocol_set_lens_filter_position
 *****************************************************************************/
int ctrl_protocol_set_lens_filter_position(ctrl_protocol_handle_t protocol,
                                           ctrl_channel_handle_t channel, int32_t pos)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), set_lens_filter_position);
    return (LENS_DRV(protocol->drv)->set_lens_filter_position(protocol->ctx, channel, pos));
}

/*
 * ctrl_protocol_get_lens_filter_settings
 *****************************************************************************/
int ctrl_protocol_get_lens_filter_settings(ctrl_protocol_handle_t protocol,
                                           ctrl_channel_handle_t channel, int num, int32_t *values)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), get_lens_filter_settings);
    CHECK_NOT_NULL(values);
    return (LENS_DRV(protocol->drv)->get_lens_filter_settings(protocol->ctx, channel, num, values));
}

/*
 * ctrl_protocol_set_lens_filter_settings
 *****************************************************************************/
int ctrl_protocol_set_lens_filter_settings(ctrl_protocol_handle_t protocol,
                                           ctrl_channel_handle_t channel, int num, int32_t *values)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), set_lens_filter_settings);
    return (LENS_DRV(protocol->drv)->set_lens_filter_settings(protocol->ctx, channel, num, values));
}

/*
 * ctrl_protocol_get_lens_mode
 *****************************************************************************/
int ctrl_protocol_get_lens_mode(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                uint16_t *mode)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), get_lens_mode);
    CHECK_NOT_NULL(mode);
    return (LENS_DRV(protocol->drv)->get_lens_mode(protocol->ctx, channel, mode));
}

/*
 * ctrl_protocol_set_lens_mode
 *****************************************************************************/
int ctrl_protocol_set_lens_mode(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                uint16_t mode)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), set_lens_mode);
    return (LENS_DRV(protocol->drv)->set_lens_mode(protocol->ctx, channel, mode));
}

/*
 * ctrl_protocol_get_motor_offset
 *****************************************************************************/
int ctrl_protocol_get_motor_offset(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                   uint16_t *offset)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), get_motor_offset);
    CHECK_NOT_NULL(offset);
    return (LENS_DRV(protocol->drv)->get_motor_offset(protocol->ctx, channel, offset));
}

/*
 * ctrl_protocol_set_motor_offset
 *****************************************************************************/
int ctrl_protocol_set_motor_offset(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                   uint16_t offset)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), set_motor_offset);
    return (LENS_DRV(protocol->drv)->set_motor_offset(protocol->ctx, channel, offset));
}

/*
 * ctrl_protocol_get_focus_offset
 *****************************************************************************/
int ctrl_protocol_get_focus_offset(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                   uint16_t *offset)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), get_focus_offset);
    CHECK_NOT_NULL(offset);
    return (LENS_DRV(protocol->drv)->get_focus_offset(protocol->ctx, channel, offset));
}

/*
 * ctrl_protocol_set_focus_offset
 *****************************************************************************/
int ctrl_protocol_set_focus_offset(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                   uint16_t offset)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), set_focus_offset);
    return (LENS_DRV(protocol->drv)->set_focus_offset(protocol->ctx, channel, offset));
}

/*
 * ctrl_protocol_get_lens_b4_info
 *****************************************************************************/
int ctrl_protocol_get_lens_b4_info(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                   int info_type, uint8_t *info)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), get_lens_b4_info);
    return (LENS_DRV(protocol->drv)->get_lens_b4_info(protocol->ctx, channel, info_type, info));
}

/*
 * ctrl_protocol_get_motor_rotating
 *****************************************************************************/
int ctrl_protocol_get_motor_rotating(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                     uint8_t *rotating)
{
    CHECK_HANDLE(protocol);
    CHECK_DRV_FUNC(LENS_DRV(protocol->drv), get_motor_rotating);
    CHECK_NOT_NULL(rotating);
    return (LENS_DRV(protocol->drv)->get_motor_rotating(protocol->ctx, channel, rotating));
}

/*
 * ctrl_protocol_lens_register
 *****************************************************************************/
int ctrl_protocol_lens_register(ctrl_protocol_handle_t handle, void *ctx,
                                const ctrl_protocol_lens_drv_t *drv)
{
    CHECK_HANDLE(handle);

    handle->drv = (void *)drv;
    handle->ctx = ctx;

    return (0);
}

/*
 * ctrl_protocol_lens_unregister
 *****************************************************************************/
int ctrl_protocol_lens_unregister(ctrl_protocol_handle_t handle)
{
    CHECK_HANDLE(handle);

    handle->drv = NULL;
    handle->ctx = NULL;

    return (0);
}
