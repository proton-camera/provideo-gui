/*
 * Copyright (C) 2017 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    test_cubic.cpp
 * @brief   Implementation of unit tests for cubic spline interpolation
 *****************************************************************************/
#include <simple_math/cubic.h>

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

using namespace testing;

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

TEST(Cubic, Init)
{
    // declare interpolation context
    spline_interpolation_ctx_t *ctx = NULL;

    // init interpolation context
    int res = sm_cubic_spline_interpolation_init(&ctx);
    ASSERT_EQ(0, res);
    ASSERT_TRUE(ctx != NULL);
    ASSERT_EQ(CSIP_STATE_INITIALIZED, ctx->state);
    ASSERT_EQ(0, ctx->n);
}

#if defined(CFG_HOLD_SAMPLES)
TEST(Cubic, AddSamples)
{
    spline_interpolation_sample_type_t x_0[3] = { CSIP_SAMPLE_TYPE(1), CSIP_SAMPLE_TYPE(5),
                                                  CSIP_SAMPLE_TYPE(10) };
    spline_interpolation_sample_type_t y_0[3] = { CSIP_SAMPLE_TYPE(1), CSIP_SAMPLE_TYPE(5),
                                                  CSIP_SAMPLE_TYPE(10) };

    spline_interpolation_sample_type_t x_1[5] = { CSIP_SAMPLE_TYPE(4), CSIP_SAMPLE_TYPE(8),
                                                  CSIP_SAMPLE_TYPE(12), CSIP_SAMPLE_TYPE(14),
                                                  CSIP_SAMPLE_TYPE(16) };
    spline_interpolation_sample_type_t y_1[5] = { CSIP_SAMPLE_TYPE(4), CSIP_SAMPLE_TYPE(8),
                                                  CSIP_SAMPLE_TYPE(12), CSIP_SAMPLE_TYPE(14),
                                                  CSIP_SAMPLE_TYPE(16) };

    spline_interpolation_sample_type_t x_2[3] = { CSIP_SAMPLE_TYPE(5), CSIP_SAMPLE_TYPE(10),
                                                  CSIP_SAMPLE_TYPE(11) };
    spline_interpolation_sample_type_t y_2[3] = { CSIP_SAMPLE_TYPE(5), CSIP_SAMPLE_TYPE(10),
                                                  CSIP_SAMPLE_TYPE(11) };

    // declare interpolation context
    spline_interpolation_ctx_t *ctx = NULL;

    // init interpolation context
    int res = sm_cubic_spline_interpolation_init(&ctx);
    ASSERT_EQ(0, res);
    ASSERT_TRUE(ctx != NULL);
    ASSERT_EQ(CSIP_STATE_INITIALIZED, ctx->state);

    /////////////////////////////////////////////////////////////////
    // TEST-CASE 1: Add 2 different series
    /////////////////////////////////////////////////////////////////

    // add samples x_0, y_0 to interpolation context
    ASSERT_TRUE(ARRAY_SIZE(x_0) == ARRAY_SIZE(y_0));
    res = sm_cubic_spline_interpolation_add_samples(ctx, x_0, y_0, ARRAY_SIZE(x_0));
    ASSERT_EQ(0, res);
    ASSERT_EQ(CSIP_STATE_GOT_SAMPLES, ctx->state);
    ASSERT_EQ(ARRAY_SIZE(x_0), ctx->n);

    // add samples x_1, y_1 to interpolation context
    ASSERT_TRUE(ARRAY_SIZE(x_1) == ARRAY_SIZE(y_1));
    res = sm_cubic_spline_interpolation_add_samples(ctx, x_1, y_1, ARRAY_SIZE(x_1));
    ASSERT_EQ(0, res);
    ASSERT_EQ(CSIP_STATE_GOT_SAMPLES, ctx->state);
    ASSERT_EQ(ARRAY_SIZE(x_1) + ARRAY_SIZE(x_0), ctx->n);

    // check the values
    ASSERT_TRUE(x_0[0] == ctx->x[0]); // 1, 1
    ASSERT_TRUE(x_1[0] == ctx->x[1]); // 4, 4
    ASSERT_TRUE(x_0[1] == ctx->x[2]); // 5, 5
    ASSERT_TRUE(x_1[1] == ctx->x[3]); // 8, 8
    ASSERT_TRUE(x_0[2] == ctx->x[4]); // 10, 10
    ASSERT_TRUE(x_1[2] == ctx->x[5]); // 12, 12
    ASSERT_TRUE(x_1[3] == ctx->x[6]); // 14, 14
    ASSERT_TRUE(x_1[4] == ctx->x[7]); // 16, 16

    ASSERT_TRUE(y_0[0] == ctx->y[0]); // 1, 1
    ASSERT_TRUE(y_1[0] == ctx->y[1]); // 4, 4
    ASSERT_TRUE(y_0[1] == ctx->y[2]); // 5, 5
    ASSERT_TRUE(y_1[1] == ctx->y[3]); // 8, 8
    ASSERT_TRUE(y_0[2] == ctx->y[4]); // 10, 10
    ASSERT_TRUE(y_1[2] == ctx->y[5]); // 12, 12
    ASSERT_TRUE(y_1[3] == ctx->y[6]); // 14, 14
    ASSERT_TRUE(y_1[4] == ctx->y[7]); // 16, 16

    /////////////////////////////////////////////////////////////////
    // TEST-CASE 2: Add a serie twice
    /////////////////////////////////////////////////////////////////

    // reset interpolation context
    res = sm_cubic_spline_interpolation_reset(ctx);
    ASSERT_EQ(0, res);
    ASSERT_TRUE(ctx != NULL);
    ASSERT_EQ(CSIP_STATE_INITIALIZED, ctx->state);
    ASSERT_EQ(0, ctx->n);

    // add samples x_0, y_0 to interpolation context
    ASSERT_TRUE(ARRAY_SIZE(x_0) == ARRAY_SIZE(y_0));
    res = sm_cubic_spline_interpolation_add_samples(ctx, x_0, y_0, ARRAY_SIZE(x_0));
    ASSERT_EQ(0, res);
    ASSERT_EQ(CSIP_STATE_GOT_SAMPLES, ctx->state);
    ASSERT_EQ(ARRAY_SIZE(x_0), ctx->n);

    // add samples x_0, y_0 to interpolation context
    ASSERT_TRUE(ARRAY_SIZE(x_0) == ARRAY_SIZE(y_0));
    res = sm_cubic_spline_interpolation_add_samples(ctx, x_0, y_0, ARRAY_SIZE(x_0));
    ASSERT_EQ(0, res);
    ASSERT_EQ(CSIP_STATE_GOT_SAMPLES, ctx->state);
    ASSERT_EQ(ARRAY_SIZE(x_0), ctx->n);

    // check the values
    ASSERT_TRUE(x_0[0] == ctx->x[0]); // 1, 1
    ASSERT_TRUE(x_0[1] == ctx->x[1]); // 5, 5
    ASSERT_TRUE(x_0[2] == ctx->x[2]); // 10, 10

    ASSERT_TRUE(y_0[0] == ctx->y[0]); // 1, 1
    ASSERT_TRUE(y_0[1] == ctx->y[1]); // 5, 5
    ASSERT_TRUE(y_0[2] == ctx->y[2]); // 10, 10

    /////////////////////////////////////////////////////////////////
    // TEST-CASE 3: Add 2 series with 2 equal x values
    /////////////////////////////////////////////////////////////////

    // reset interpolation context
    res = sm_cubic_spline_interpolation_reset(ctx);
    ASSERT_EQ(0, res);
    ASSERT_TRUE(ctx != NULL);
    ASSERT_EQ(CSIP_STATE_INITIALIZED, ctx->state);
    ASSERT_EQ(0, ctx->n);

    // add samples x_0, y_0 to interpolation context
    ASSERT_TRUE(ARRAY_SIZE(x_0) == ARRAY_SIZE(y_0));
    res = sm_cubic_spline_interpolation_add_samples(ctx, x_0, y_0, ARRAY_SIZE(x_0));
    ASSERT_EQ(0, res);
    ASSERT_EQ(CSIP_STATE_GOT_SAMPLES, ctx->state);
    ASSERT_EQ(ARRAY_SIZE(x_0), ctx->n);

    // add samples x_2, y_2 to interpolation context
    ASSERT_TRUE(ARRAY_SIZE(x_2) == ARRAY_SIZE(y_2));
    res = sm_cubic_spline_interpolation_add_samples(ctx, x_2, y_2, ARRAY_SIZE(x_2));
    ASSERT_EQ(0, res);
    ASSERT_EQ(CSIP_STATE_GOT_SAMPLES, ctx->state);
    ASSERT_EQ(ARRAY_SIZE(x_0) + ARRAY_SIZE(x_2) - 2, ctx->n);

    // check the values
    ASSERT_TRUE(x_0[0] == ctx->x[0]); // 1, 1
    ASSERT_TRUE(x_0[1] == ctx->x[1]); // 5, 5
    ASSERT_TRUE(x_0[2] == ctx->x[2]); // 10, 10
    ASSERT_TRUE(x_2[2] == ctx->x[3]); // 11, 11

    ASSERT_TRUE(y_0[0] == ctx->y[0]); // 1, 1
    ASSERT_TRUE(y_0[1] == ctx->y[1]); // 5, 5
    ASSERT_TRUE(y_0[2] == ctx->y[2]); // 10, 10
    ASSERT_TRUE(y_2[2] == ctx->y[3]); // 11, 11
}
#endif // defined(CFG_HOLD_SAMPLES)

TEST(Cubic, SetSamples)
{
    spline_interpolation_sample_type_t x_0[3] = { CSIP_SAMPLE_TYPE(1), CSIP_SAMPLE_TYPE(5),
                                                  CSIP_SAMPLE_TYPE(10) };
    spline_interpolation_sample_type_t y_0[3] = { CSIP_SAMPLE_TYPE(1), CSIP_SAMPLE_TYPE(5),
                                                  CSIP_SAMPLE_TYPE(10) };

    spline_interpolation_sample_type_t x_1[5] = { CSIP_SAMPLE_TYPE(4), CSIP_SAMPLE_TYPE(8),
                                                  CSIP_SAMPLE_TYPE(12), CSIP_SAMPLE_TYPE(14),
                                                  CSIP_SAMPLE_TYPE(16) };
    spline_interpolation_sample_type_t y_1[5] = { CSIP_SAMPLE_TYPE(4), CSIP_SAMPLE_TYPE(8),
                                                  CSIP_SAMPLE_TYPE(12), CSIP_SAMPLE_TYPE(14),
                                                  CSIP_SAMPLE_TYPE(16) };

    int res;

    // declare interpolation context
    spline_interpolation_ctx_t *ctx = NULL;

    // init interpolation context
    res = sm_cubic_spline_interpolation_init(&ctx);
    ASSERT_EQ(0, res);
    ASSERT_TRUE(ctx != NULL);
    ASSERT_EQ(CSIP_STATE_INITIALIZED, ctx->state);
    ASSERT_EQ(0, ctx->n);

    /////////////////////////////////////////////////////////////////
    // TEST-CASE 1: Set a series of sample values
    /////////////////////////////////////////////////////////////////

    // set samples x_0, y_0 to interpolation context
    ASSERT_TRUE(ARRAY_SIZE(x_0) == ARRAY_SIZE(y_0));
    res = sm_cubic_spline_interpolation_set_samples(ctx, x_0, y_0, ARRAY_SIZE(x_0));
    ASSERT_EQ(0, res);
    ASSERT_EQ(CSIP_STATE_GOT_SAMPLES, ctx->state);
    ASSERT_EQ(ARRAY_SIZE(x_0), ctx->n);

    // check the values
    ASSERT_TRUE(x_0[0] == ctx->x[0]); // 1, 1
    ASSERT_TRUE(x_0[1] == ctx->x[1]); // 5, 5
    ASSERT_TRUE(x_0[2] == ctx->x[2]); // 10, 10

    ASSERT_TRUE(y_0[0] == ctx->y[0]); // 1, 1
    ASSERT_TRUE(y_0[1] == ctx->y[1]); // 5, 5
    ASSERT_TRUE(y_0[2] == ctx->y[2]); // 10, 10

    /////////////////////////////////////////////////////////////////
    // TEST-CASE 2: Overwrite samples with a second series
    /////////////////////////////////////////////////////////////////

    // reset interpolation context
    res = sm_cubic_spline_interpolation_reset(ctx);
    ASSERT_EQ(0, res);
    ASSERT_TRUE(ctx != NULL);
    ASSERT_EQ(CSIP_STATE_INITIALIZED, ctx->state);
    ASSERT_EQ(0, ctx->n);

    // set samples x_1, y_1 to interpolation context
    ASSERT_TRUE(ARRAY_SIZE(x_1) == ARRAY_SIZE(y_1));
    res = sm_cubic_spline_interpolation_set_samples(ctx, x_1, y_1, ARRAY_SIZE(x_1));
    ASSERT_EQ(0, res);
    ASSERT_EQ(CSIP_STATE_GOT_SAMPLES, ctx->state);
    ASSERT_EQ(ARRAY_SIZE(x_1), ctx->n);

    // set samples x_0, y_0 to interpolation context
    ASSERT_TRUE(ARRAY_SIZE(x_0) == ARRAY_SIZE(y_0));
    res = sm_cubic_spline_interpolation_set_samples(ctx, x_0, y_0, ARRAY_SIZE(x_0));
    ASSERT_EQ(0, res);
    ASSERT_EQ(CSIP_STATE_GOT_SAMPLES, ctx->state);
    ASSERT_EQ(ARRAY_SIZE(x_0), ctx->n);

    // check the values
    ASSERT_TRUE(x_0[0] == ctx->x[0]); // 1, 1
    ASSERT_TRUE(x_0[1] == ctx->x[1]); // 5, 5
    ASSERT_TRUE(x_0[2] == ctx->x[2]); // 10, 10

    ASSERT_TRUE(y_0[0] == ctx->y[0]); // 1, 1
    ASSERT_TRUE(y_0[1] == ctx->y[1]); // 5, 5
    ASSERT_TRUE(y_0[2] == ctx->y[2]); // 10, 10
}

TEST(Cubic, LinearInterpolation)
{
    // linear curve
    spline_interpolation_sample_type_t x_0[5] = { CSIP_SAMPLE_TYPE(0), CSIP_SAMPLE_TYPE(5),
                                                  CSIP_SAMPLE_TYPE(10), CSIP_SAMPLE_TYPE(15),
                                                  CSIP_SAMPLE_TYPE(20) };
    spline_interpolation_sample_type_t y_0[5] = { CSIP_SAMPLE_TYPE(0), CSIP_SAMPLE_TYPE(5),
                                                  CSIP_SAMPLE_TYPE(10), CSIP_SAMPLE_TYPE(15),
                                                  CSIP_SAMPLE_TYPE(20) };

    spline_interpolation_sample_type_t y;

    int res;

    int32_t i;

    // declare interpolation context
    spline_interpolation_ctx_t *ctx = NULL;

    // init interpolation context
    res = sm_cubic_spline_interpolation_init(&ctx);
    ASSERT_EQ(0, res);
    ASSERT_TRUE(ctx != NULL);
    ASSERT_EQ(CSIP_STATE_INITIALIZED, ctx->state);
    ASSERT_EQ(0, ctx->n);

    // set samples x_0, y_0 to interpolation context
    ASSERT_TRUE(ARRAY_SIZE(x_0) == ARRAY_SIZE(y_0));
    res = sm_cubic_spline_interpolation_set_samples(ctx, x_0, y_0, ARRAY_SIZE(x_0));
    ASSERT_EQ(0, res);
    ASSERT_EQ(CSIP_STATE_GOT_SAMPLES, ctx->state);
    ASSERT_EQ(ARRAY_SIZE(x_0), ctx->n);

    // calculation int
    res = sm_cubic_spline_interpolation_calc_init(ctx);
    ASSERT_EQ(0, res);
    ASSERT_EQ(CSIP_STATE_RUNNING, ctx->state);
    ASSERT_EQ(ARRAY_SIZE(x_0), ctx->n);

    // check the values
    ASSERT_EQ(x_0[0], ctx->x[0]); // 0, 0
    ASSERT_EQ(x_0[1], ctx->x[1]); // 5, 5
    ASSERT_EQ(x_0[2], ctx->x[2]); // 10, 10
    ASSERT_EQ(x_0[3], ctx->x[3]); // 15, 15
    ASSERT_EQ(x_0[4], ctx->x[4]); // 20, 20

    ASSERT_EQ(y_0[0], ctx->y[0]); // 0, 0
    ASSERT_EQ(y_0[1], ctx->y[1]); // 5, 5
    ASSERT_EQ(y_0[2], ctx->y[2]); // 10, 10
    ASSERT_EQ(y_0[3], ctx->y[3]); // 15, 15
    ASSERT_EQ(y_0[4], ctx->y[4]); // 20, 20

    // run over sample points
    ASSERT_TRUE(ARRAY_SIZE(x_0) == ARRAY_SIZE(y_0));
    for (i = 0u; i < (int32_t)ARRAY_SIZE(x_0); i++) {
        res = sm_cubic_spline_interpolation_calc(ctx, x_0[i], &y);
        ASSERT_EQ(0, res);
        ASSERT_TRUE(ctx->y[i] == y);
    }

    // run from x_0 to x_n by incrementing
    ASSERT_TRUE(ARRAY_SIZE(x_0) == ARRAY_SIZE(y_0));
    for (i = x_0[0u]; i <= x_0[ARRAY_SIZE(x_0) - 1]; i++) {
        res = sm_cubic_spline_interpolation_calc(ctx, CSIP_SAMPLE_TYPE(i), &y);
        ASSERT_EQ(0, res);
        ASSERT_TRUE(CSIP_SAMPLE_TYPE(i) == y);
    }
}

TEST(Cubic, SquareInterpolation)
{
    // linear curve
    spline_interpolation_sample_type_t x_0[5] = { CSIP_SAMPLE_TYPE(5), CSIP_SAMPLE_TYPE(10),
                                                  CSIP_SAMPLE_TYPE(15), CSIP_SAMPLE_TYPE(20),
                                                  CSIP_SAMPLE_TYPE(25) };
    spline_interpolation_sample_type_t y_0[5] = { x_0[0] * x_0[0], x_0[1] * x_0[1], x_0[2] * x_0[2],
                                                  x_0[3] * x_0[3], x_0[4] * x_0[4] };

    spline_interpolation_sample_type_t y;

    int res;

    int32_t i;

    // declare interpolation context
    spline_interpolation_ctx_t *ctx = NULL;

    // init interpolation context
    res = sm_cubic_spline_interpolation_init(&ctx);
    ASSERT_EQ(0, res);
    ASSERT_TRUE(ctx != NULL);
    ASSERT_EQ(CSIP_STATE_INITIALIZED, ctx->state);
    ASSERT_EQ(0, ctx->n);

    // set samples x_0, y_0 to interpolation context
    ASSERT_TRUE(ARRAY_SIZE(x_0) == ARRAY_SIZE(y_0));
    res = sm_cubic_spline_interpolation_set_samples(ctx, x_0, y_0, ARRAY_SIZE(x_0));
    ASSERT_EQ(0, res);
    ASSERT_EQ(CSIP_STATE_GOT_SAMPLES, ctx->state);
    ASSERT_EQ(ARRAY_SIZE(x_0), ctx->n);

    // calculation int
    res = sm_cubic_spline_interpolation_calc_init(ctx);
    ASSERT_EQ(0, res);
    ASSERT_EQ(CSIP_STATE_RUNNING, ctx->state);
    ASSERT_EQ(ARRAY_SIZE(x_0), ctx->n);

    // check the values
    ASSERT_EQ(x_0[0], ctx->x[0]); // 0, 0
    ASSERT_EQ(x_0[1], ctx->x[1]); // 5, 5
    ASSERT_EQ(x_0[2], ctx->x[2]); // 10, 10
    ASSERT_EQ(x_0[3], ctx->x[3]); // 15, 15
    ASSERT_EQ(x_0[4], ctx->x[4]); // 20, 20

    ASSERT_EQ(y_0[0], ctx->y[0]); // 0, 0
    ASSERT_EQ(y_0[1], ctx->y[1]); // 5, 5
    ASSERT_EQ(y_0[2], ctx->y[2]); // 10, 10
    ASSERT_EQ(y_0[3], ctx->y[3]); // 15, 15
    ASSERT_EQ(y_0[4], ctx->y[4]); // 20, 20

    // run over sample points
    ASSERT_TRUE(ARRAY_SIZE(x_0) == ARRAY_SIZE(y_0));
    for (i = 0u; i < (int32_t)ARRAY_SIZE(x_0); i++) {
        res = sm_cubic_spline_interpolation_calc(ctx, x_0[i], &y);
        ASSERT_EQ(0, res);
        ASSERT_TRUE(ctx->y[i] == y);
    }

    // run from x_0 to x_n by incrementing
    double mse = 0.0f; // mean square error
    ASSERT_TRUE(ARRAY_SIZE(x_0) == ARRAY_SIZE(y_0));
    for (i = x_0[0u]; i <= x_0[ARRAY_SIZE(x_0) - 1]; i++) {
        res = sm_cubic_spline_interpolation_calc(ctx, CSIP_SAMPLE_TYPE(i), &y);
        ASSERT_EQ(0, res);
        mse += (i * i - y) * (i * i - y);
    }
    mse /= (double)i;
    ASSERT_TRUE(mse < 1.5f);
}
