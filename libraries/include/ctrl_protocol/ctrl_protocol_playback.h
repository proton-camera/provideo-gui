/*
 * Copyright (C) 2017 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    ctrl_protocol_playback.h
 *
 * @brief   Generic playback- and record engine control protocol functions
 *
 *****************************************************************************/
#ifndef CTRL_PROTOCOL_PLAYBACK_H
#define CTRL_PROTOCOL_PLAYBACK_H

#include <stdint.h>

#include <ctrl_channel/ctrl_channel.h>
#include <ctrl_protocol/ctrl_protocol.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @defgroup ctrl_protocol_layer Generic Control Protocol Layer implementation
 * @{
 *****************************************************************************/

/**
 * @brief Recording buffer information type
 *****************************************************************************/
typedef struct ctrl_protocol_buffer_info_s
{
    uint32_t buffer_id; /**< ID of the buffer */
    uint32_t num_frames; /**< current amount of frames that have been recorded in the buffer */
    uint32_t max_frames; /**< maximum number of frames that can be recorded in the buffer */
    char status[16]; /**< Buffer status, can be either of "free", "used", "record", "play" or
                        "pause" */
} ctrl_protocol_buffer_info_t;

/**
 * @brief SSD status type
 *****************************************************************************/
typedef struct ctrl_protocol_ssd_status_s
{
    uint8_t ssd_id; /**< ID of the SSD */
    uint8_t status_code; /**< Status code that was returned by smartctl */
    char status_string[32]; /**< Status as user readable text */
    uint8_t temp_min; /**< Minimum logged SSD temperature */
    uint8_t temp; /**< Current SSD temperature */
    uint8_t temp_max; /**< Maximum logged SSD temperature */
    uint8_t lifetime; /**< Remaining estimated lifetime in % */
} ctrl_protocol_ssd_status_t;

/**
 * @brief Returns the ID of the buffer which is currently selected for
 *        recording.
 *
 * @param[in]  channel  control channel instance
 * @param[in]  protocol control protocol instance
 * @param[out] id       ID of the current recording buffer
 *
 * @return     0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_rec(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                          uint8_t *id);

/**
 * @brief Starts recording into the buffer with the given ID.
 *
 * @param[in]  channel  control channel instance
 * @param[in]  protocol control protocol instance
 * @param[in]  id       ID of the buffer to which recording is started
 *
 * @return     0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_rec(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                          uint8_t id);

/**
 * @brief Returns the current recording mode.
 *
 * @param[in]  channel  control channel instance
 * @param[in]  protocol control protocol instance
 * @param[out] mode     current recording mode
 *
 * @return     0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_rec_mode(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                               uint8_t *mode);

/**
 * @brief Sets the recording mode.
 *
 * @param[in]  channel  control channel instance
 * @param[in]  protocol control protocol instance
 * @param[in]  mode     recording mode to set
 *
 * @return     0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_rec_mode(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                               uint8_t mode);

/**
 * @brief Stops recording.
 *
 * @param[in]  channel  control channel instance
 * @param[in]  protocol control protocol instance
 *
 * @return     0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_rec_stop(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel);

/**
 * @brief Returns the current playback buffer and the playback speed.
 *
 * @param[in]  channel  control channel instance
 * @param[in]  protocol control protocol instance
 * @param[in]  no       amount of elements in the output array, must be 2
 * @param[out] values   array of output values, first value will be set to
 *                      current playback buffer ID, second value playback speed
 *
 * @return     0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_play(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel, int no,
                           int16_t *values);

/**
 * @brief Starts playback from given buffer with given playback speed.
 *
 * @param[in]  channel  control channel instance
 * @param[in]  protocol control protocol instance
 * @param[in]  no       amount of elements in the input array, must be 2
 * @param[in]  values   array of input values, first value must be set to
 *                      ID of the buffer from which playback is started,
 *                      second value to the playback speed
 *
 * @return     0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_play(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel, int no,
                           int16_t *values);

/**
 * @brief Returns the current playback mode.
 *
 * @param[in]  channel  control channel instance
 * @param[in]  protocol control protocol instance
 * @param[out] mode     current playback mode
 *
 * @return     0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_play_mode(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                uint8_t *mode);

/**
 * @brief Sets the playback mode.
 *
 * @param[in]  channel  control channel instance
 * @param[in]  protocol control protocol instance
 * @param[in]  mode     playback mode to set
 *
 * @return     0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_play_mode(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                uint8_t mode);

/**
 * @brief Pause playback.
 *
 * @param[in]  channel  control channel instance
 * @param[in]  protocol control protocol instance
 *
 * @return     0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_pause(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel);

/**
 * @brief Stop playback.
 *
 * @param[in]  channel  control channel instance
 * @param[in]  protocol control protocol instance
 *
 * @return     0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_stop(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel);

/**
 * @brief Returns the current stop mode.
 *
 * @param[in]  channel  control channel instance
 * @param[in]  protocol control protocol instance
 * @param[out] mode     current stop mode
 *
 * @return     0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_stop_mode(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                uint8_t *mode);

/**
 * @brief Sets the stop mode.
 *
 * @param[in]  channel  control channel instance
 * @param[in]  protocol control protocol instance
 * @param[in]  mode     stop mode to set
 *
 * @return     0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_stop_mode(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                                uint8_t mode);

/**
 * @brief Performs a seek with the given mode to the given position.
 *
 * @param[in]  channel  control channel instance
 * @param[in]  protocol control protocol instance
 * @param[in]  no       amount of elements in the input array, must be 2
 * @param[in]  values   array of input values, first value is the seek mode,
 *                      second value the relative or absolute position
 *                      (depending on the mode)
 *
 * @return     0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_seek(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel, int no,
                           int32_t *values);

/**
 * @brief Returns the current position in the playback buffer.
 *
 * @param[in]  channel  control channel instance
 * @param[in]  protocol control protocol instance
 * @param[out] mode     current stop mode
 *
 * @return     0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_pos(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                          uint32_t *pos);

/**
 * @brief Set current frame as mark_in position.
 *
 * @param[in]  channel  control channel instance
 * @param[in]  protocol control protocol instance
 * @param[in]  position where mark_in is set, if set to 0 mark_in will be
 *             set at the current position
 *
 * @return     0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_mark_in(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                          uint32_t pos);

/**
 * @brief Set current frame as mark_out position.
 *
 * @param[in]  channel  control channel instance
 * @param[in]  protocol control protocol instance
 * @param[in]  position where mark_out is set, if set to 0 mark_out will be
 *             set at the current position
 *
 * @return     0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_mark_out(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                           uint32_t pos);

/**
 * @brief Get the mark_in and mark_out position of the current playback buffer
 *
 * @param[in]  channel  control channel instance
 * @param[in]  protocol control protocol instance
 * @param[in]  no       amount of elements in the input array, must be 2
 * @param[in]  values   array of output values, first value is mark_in
 *                      position, second value is mark_out position
 *
 * @return     0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_mark_pos(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                               int no, uint32_t *values);

/**
 * @brief Get the status of the buffer with the given ID.
 *
 * @param[in]  channel  control channel instance
 * @param[in]  protocol control protocol instance
 * @param[in]  no       size of the values array, must be
 *                      sizeof(ctrl_protocol_buffer_info_t)
 * @param[in]  values   array of output values, this is casted to a
 *                      ctrl_protocol_buffer_info_t struct
 *
 * @return     0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_status(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel, int no,
                             uint8_t *values);

/**
 * @brief Free buffer with given ID.
 *
 * @param[in]  channel  control channel instance
 * @param[in]  protocol control protocol instance
 * @param[in]  id       ID of the buffer which shall be freed, or 0 to free
 *                      all buffers
 *
 * @return     0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_free(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                           uint8_t id);

/**
 * @brief Returns the amount of available recording buffers.
 *
 * @param[in]  channel  control channel instance
 * @param[in]  protocol control protocol instance
 * @param[out] mode     current recording mode
 *
 * @return     0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_count(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                            uint8_t *num);

/**
 * @brief Sets the number of recording buffers (recording space partitioning).
 *
 * @param[in]  channel  control channel instance
 * @param[in]  protocol control protocol instance
 * @param[in]  mode     recording mode to set
 *
 * @return     0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_set_count(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel,
                            uint8_t num);

/**
 * @brief Get the health status of the SSDs.
 *
 * @param[in]  channel  control channel instance
 * @param[in]  protocol control protocol instance
 * @param[in]  no       size of the values array, must be
 *                      2 * sizeof(ctrl_protocol_ssd_status_t)
 * @param[in]  values   array of output values, this is casted to a
 *                      pointer of a two element wide array of
 *                      ctrl_protocol_ssd_status_t structs.
 *
 * @return     0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_get_health(ctrl_protocol_handle_t protocol, ctrl_channel_handle_t channel, int no,
                             uint8_t *values);

/**
 * @brief Playback engine protocol driver implementation
 *****************************************************************************/
typedef struct ctrl_protocol_playback_drv_s
{
    // record control
    ctrl_protocol_get_uint8_t get_rec;
    ctrl_protocol_set_uint8_t set_rec;
    ctrl_protocol_get_uint8_t get_rec_mode;
    ctrl_protocol_set_uint8_t set_rec_mode;
    ctrl_protocol_run_t rec_stop;
    // playback control
    ctrl_protocol_int16_array_t get_play;
    ctrl_protocol_int16_array_t set_play;
    ctrl_protocol_get_uint8_t get_play_mode;
    ctrl_protocol_set_uint8_t set_play_mode;
    ctrl_protocol_run_t pause;
    ctrl_protocol_run_t stop;
    ctrl_protocol_get_uint8_t get_stop_mode;
    ctrl_protocol_set_uint8_t set_stop_mode;
    ctrl_protocol_int32_array_t set_seek;
    ctrl_protocol_get_uint32_t get_pos;
    ctrl_protocol_set_uint32_t mark_in;
    ctrl_protocol_set_uint32_t mark_out;
    ctrl_protocol_uint32_array_t get_mark_pos;
    // buffer control
    ctrl_protocol_uint8_array_t get_status; // uses ctrl_protocol_buffer_info_t as argument
    ctrl_protocol_set_uint8_t set_free;
    ctrl_protocol_get_uint8_t get_count;
    ctrl_protocol_set_uint8_t set_count;
    ctrl_protocol_uint8_array_t get_health;
} ctrl_protocol_playback_drv_t;

/**
 * @brief      Register a protocol implementation at control protocol layer
 *
 * @param      handle   instance handle of control protocol layer
 * @param      ctx      private context of protocol driver implementation
 * @param      drv      driver functions of protocol implementation
 *
 * @return     0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_playback_register(ctrl_protocol_handle_t handle, void *ctx,
                                    ctrl_protocol_playback_drv_t *drv);

/**
 * @brief      Remove/unregister a protocol implementation from
 *             control protocol layer
 *
 * @param      handle   instance handle of control protocol layer
 *
 * @return     0 on success, error-code otherwise
 *****************************************************************************/
int ctrl_protocol_playback_unregister(ctrl_protocol_handle_t handle);

/* @} ctrl_protocol_layer */

#ifdef __cplusplus
}
#endif

#endif /* CTRL_PROTOCOL_PLAYBACK_H */
