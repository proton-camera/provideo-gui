#! /usr/bin/env bash

# This script builds and deploys the ProVideo GUI.
# Only for DCT internal use.

set -eE

rc=0
trap 'rc=$?; >&2 echo "$0:$LINENO: unexpected error"; exit $rc' ERR

PROG=$(basename "$0")
SCRIPT_DIR=$(cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)

usage() {
   cat <<EOF
Usage: $PROG --mxe <path to mxe> --gitlab <path to gitlab download dir>
  -g,--gitlab path to the provideo-downloads gitlab repo, default ../provideo-downloads
  -m,--mxe    path to built mxe environment, default ../mxe
  -t,--test   create a test release, do not copy to gitlab clone
  -h,--help   shows this help
EOF
   exit "$1"
}

# local variables
is_test_release=false
mxe_path="../mxe"
gitlab_path="../provideo-downloads"
export_path="/project/AtomOne/export/provideo_gui/release"

# option parsing
while [ $# != 0 ]; do
    case "$1" in
        -h|--help)
            usage 0
            ;;
        -g|--gitlab)
            if [[ -d "$2" ]]; then
                gitlab_path="$2"
                shift 2
            else
                usage 1
            fi
            ;;
        -m|--mxe)
            if [[ -d "$2" ]]; then
                mxe_path="$2"
                shift 2
            else
                usage 1
            fi
            ;;
        -t|--test)
            is_test_release=true
            shift 1
            ;;
        *)
            usage 1
            ;;
    esac
done

mxe_path=$(realpath "$mxe_path")

# checking for a clean workspace

if ! git diff-index --quiet HEAD && ! $is_test_release; then
    echo "This git clone is dirty."
    echo "Please commit everything before using this script."
    echo "giving up ..."
    exit 2
fi

# getting ProVideo GUI version
version=$(sed -nE 's/set\(VERSION ([0-9]+\.[0-9]+\.[0-9]+)\)$/\1/p' "$SCRIPT_DIR"/application/CMakeLists.txt)
if $is_test_release; then
    export_path=$export_path/v"$version"_test_$USER
else
    export_path=$export_path/v$version
fi

if [[ -d $export_path ]] && ! $is_test_release; then
    echo "The export path exists:"
    echo "'$export_path'"
    echo "giving up ..."
    exit 3
fi

# building ProVideo GUI with mxe environment
echo "$PROG: building ProVideo GUI version 'v$version' ..."
export PATH=$mxe_path/usr/bin:$PATH
build_dir="$SCRIPT_DIR"/build-mxe-release
mkdir -p "$build_dir"
rm -rf "${build_dir:?}"/*

# the version should be tagged before running this script
# git tag
# git tag v"$version"

# use CMakeLists.txt to build the release
(cd "$build_dir" && "$mxe_path"/usr/bin/i686-w64-mingw32.static-cmake -G Ninja -S ../application/ -B ./)
(cd "$build_dir" && ninja clean)
(cd "$build_dir" && ninja -j 4)

# copy to export directory
echo "$PROG: copying files to '$export_path' ..."
mkdir -p "$export_path"
cp "$build_dir"/ProVideo.exe "$export_path"
mkdir -p "$export_path"/tools_and_configs
cp -r "$SCRIPT_DIR"/application/tools_and_configs/flashloader.exe "$export_path"/tools_and_configs
cp -r "$SCRIPT_DIR"/application/tools_and_configs/SupportedLenses.txt "$export_path"/tools_and_configs
cp "$SCRIPT_DIR"/doc/user_manual.pdf "$export_path"/ProVideo\ GUI\ Manual.pdf

# create zip archive
echo "$PROG: creating zip archive ..."
(cd "$export_path" && zip -q -r ProVideo_GUI_v"$version".zip ./)

# copying into the gitlab clone
if ! $is_test_release; then
    echo "$PROG: copy files to the gitlab clone ..."
    cp "$export_path"/ProVideo_GUI_v"$version".zip "$gitlab_path"/provideo_gui/
    cp "$export_path"/ProVideo\ GUI\ Manual.pdf "$gitlab_path"/provideo_gui/

    # patching auto update info
    echo "$PROG: patching auto update info ..."
    version_=${version//"."/"_"}
    update_info_path="$gitlab_path"/auto_update/provideo_gui/update_info.txt
    sed -i -E "s/(^version: V)[0-9]+_[0-9]+_[0-9]+$/\1$version_/" "$update_info_path"
fi
