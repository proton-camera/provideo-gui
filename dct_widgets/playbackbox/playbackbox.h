/*
 * Copyright (C) 2017 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    playbackbox.h
 *
 * @brief   Class definition of a playback control box
 *
 *****************************************************************************/
#ifndef PLAYBACK_BOX_H
#define PLAYBACK_BOX_H

#include <dct_widgets_base.h>

/*
 * PLayback Control Box Widget
 *****************************************************************************/
class PlayBackBox : public DctWidgetBox
{
    Q_OBJECT

public:
    explicit PlayBackBox(QWidget *parent = nullptr);
    ~PlayBackBox() Q_DECL_OVERRIDE;

protected:
    void showEvent(QShowEvent *event) Q_DECL_OVERRIDE;
    void hideEvent(QHideEvent *event) Q_DECL_OVERRIDE;

    void prepareMode(Mode) Q_DECL_OVERRIDE;

    void loadSettings(QSettings &) Q_DECL_OVERRIDE;
    void saveSettings(QSettings &) Q_DECL_OVERRIDE;
    void applySettings(void) Q_DECL_OVERRIDE;

signals:
    void WbUpdate();

    void UpdateBufferStatus();
    void UpdatePosition();

    void BufferCountChanged(int num);
    void FreeBuffer(int buffer_id);

    void RecordChanged(int buffer_id);
    void RecordStopped();
    void RecordModeChanged(int mode);

    void PlayChanged(int buffer_id, int speed);
    void PlayPaused();
    void PlayStopped();
    void PlayModeChanged(int mode);
    void StopModeChanged(int mode);

    void MarkIn(int pos);
    void MarkOut(int pos);
    void SeekChanged(int mode, int pos);

public slots:
    void onBufferCountChange(int num);
    void onBufferStatusChange(int buffer_id, int num_frames, int max_frames, QString status);

    void onRecordChange(int buffer_id);
    void onRecordModeChange(int mode);

    void onPlayChange(int buffer_id, int speed);
    void onPlayModeChange(int mode);
    void onStopModeChange(int mode);

    void onPositionChange(int pos);
    void onMarkPositionChange(int mark_in, int mark_out);

private slots:
    void onUpdateTimerExpired();

    void onBufferSelectionChanged();

    void onCbxRecordModeChanged(int mode);
    void onCbxPlayModeChanged(int mode);
    void onCbxStopModeChanged(int mode);

    void onSbxBufferCountChanged(int num);
    void onApplyBufferCountClicked();

    void onFreeSelectedBufferClicked();
    void onFreeAllBuffersClicked();

    void onStartRecordingClicked();
    void onStopRecordingClicked();
    void onStartPlaybackClicked();
    void onStopPlaybackClicked();

    void onMarkInClicked();
    void onMarkOutClicked();
    void onResetMarksClicked();

    void onPositionSliderMoved(int pos);
    void onMarkInPositionSliderMoved(int mark_in);
    void onMarkOutPositionSliderMoved(int mark_out);

    void onPlayClicked();
    void onPlaybackSpeedSliderMoved(int speed);

private:
    class PrivateData;
    PrivateData *d_data;

    void beforeCommand();
    void afterCommand();

    int getRecordMode();
    void setRecordMode(int mode);
    int getPlayMode();
    void setPlayMode(int mode);
    int getStopMode();
    void setStopMode(int mode);

    void updatePlayButton(bool playing);

    void doRelativeSeek(int frames);
    void changePlaybackSpeed(int speed);
};

#endif // PLAYBACK_BOX_H
