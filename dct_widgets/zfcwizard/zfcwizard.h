/*
 * Copyright (C) 2022 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    zfcwizard.h
 * @brief   Zoom Focus Calibration Wizard
 *****************************************************************************/
#ifndef ZFCWIZARD_H
#define ZFCWIZARD_H

#include <QWizard>

namespace Ui {
class ZfcWizard;
}

class LensItf;
class LensDriverBox;
class ProVideoSystemItf;

class ZfcWizard : public QWizard
{
    Q_OBJECT

public:
    explicit ZfcWizard(QWidget *parent, LensItf *lensIf, ProVideoSystemItf *sysIf);
    ~ZfcWizard() override;

    enum { PageIntro, PageFocusPosition, PageMotorOffset, PageOutro };

    void accept() override;
    void reject() override;

protected:
    void initializePage(int id) override;
    void cleanupPage(int id) override;

private slots:
    void next();

private: // NOLINT(readability-redundant-access-specifiers)
    void _resync_focus_motor_pos();

    Ui::ZfcWizard *ui;
    LensItf *m_lensIf;
    ProVideoSystemItf *m_sysIf;
    LensDriverBox *m_ldb;
    bool m_origFineFocus{};
    bool m_origFineZoom{};
    int m_origZoomPos{};
    QTimer *m_timer;
};

#endif // ZFCWIZARD_H
