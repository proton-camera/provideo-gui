/*
 * Copyright (C) 2022 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    zfcwizard.cpp
 * @brief   Zoom Focus Calibration Wizard
 *****************************************************************************/
#include "zfcwizard.h"
#include "ui_zfcwizard.h"
#include "ui_zfcmotoroffsetpage.h"

#include "LensItf.h"
#include "ProVideoSystemItf.h"
#include "lensdriverbox.h"

#include <QAbstractButton>
#include <QDebug>
#include <QThread>

ZfcWizard::ZfcWizard(QWidget *parent, LensItf *lensIf, ProVideoSystemItf *sysIf)
    : QWizard(parent),
      ui(new Ui::ZfcWizard),
      m_lensIf(lensIf),
      m_sysIf(sysIf),
      m_ldb(dynamic_cast<LensDriverBox *>(parent)),
      m_timer(new QTimer(this))
{
    ui->setupUi(this);

    // remove help and back button
    QList<QWizard::WizardButton> button_layout;
    button_layout << QWizard::Stretch << QWizard::NextButton << QWizard::FinishButton
                  << QWizard::CancelButton;
    this->setButtonLayout(button_layout);

    // need to implement own slot for next button
    QAbstractButton *b = this->button(QWizard::NextButton);
    connect(b, SIGNAL(clicked()), this, SLOT(next()), Qt::UniqueConnection);

    // Focus Position of ZfcWizard
    connect(m_lensIf, QOverload<int>::of(&LensItf::LensFocusMotorPositionChanged),
            ui->ZfcFocusPosPageObj,
            QOverload<int>::of(&ZfcFocusPosPage::onLensFocusMotorPositionChange),
            Qt::UniqueConnection);
    connect(ui->ZfcFocusPosPageObj,
            QOverload<int>::of(&ZfcFocusPosPage::LensFocusMotorPositionChanged), m_lensIf,
            QOverload<int>::of(&LensItf::onLensFocusMotorPositionChange), Qt::UniqueConnection);

    // MotorOffset of ZfcWizard
    connect(m_lensIf, QOverload<int>::of(&LensItf::LensMotorOffsetChanged),
            ui->ZfcMotorOffsetPageObj,
            QOverload<int>::of(&ZfcMotorOffsetPage::onLensMotorOffsetChange), Qt::UniqueConnection);
    connect(ui->ZfcMotorOffsetPageObj,
            QOverload<int>::of(&ZfcMotorOffsetPage::LensMotorOffsetChanged), m_lensIf,
            QOverload<int>::of(&LensItf::onLensMotorOffsetChange), Qt::UniqueConnection);
}

ZfcWizard::~ZfcWizard()
{
    delete ui;
}

void ZfcWizard::accept()
{
    qDebug() << __PRETTY_FUNCTION__ << "..."; // NOLINT(hicpp-no-array-decay)
    const bool zoomsharpsilent = field("zoomsharpsilent").toBool();
    const bool zoomsharp = field("zoomsharp").toBool();

    // set lensmode
    m_lensIf->onLensModeChange(zoomsharpsilent ? 3 : (zoomsharp ? 2 : 1));
    m_lensIf->GetLensMode();
    // save settings
    m_sysIf->onSaveSettings();

    QDialog::accept();
}

void ZfcWizard::reject()
{
    qDebug() << __PRETTY_FUNCTION__ << "..."; // NOLINT(hicpp-no-array-decay)

    // restore original settings
    m_lensIf->onLensModeChange(0);
    m_lensIf->GetLensMode();
    m_lensIf->onLensFocusFineChange(m_origFineFocus);
    m_lensIf->GetLensFocusFine();
    m_lensIf->onLensZoomFineChange(m_origFineZoom);
    m_lensIf->GetLensZoomFine();
    m_lensIf->onLensZoomPositionChange(m_origZoomPos);
    m_lensIf->GetLensZoomPosition();

    QDialog::reject();
}

void ZfcWizard::initializePage(int id)
{
    qDebug() << __PRETTY_FUNCTION__ << id; // NOLINT(hicpp-no-array-decay)

    page(id)->initializePage(); // the default implementation

    switch (id) {
    case PageIntro:
        QGuiApplication::setOverrideCursor(Qt::WaitCursor);
        m_origFineFocus = m_ldb->getLensFineFocus();
        m_origFineZoom = m_ldb->getLensFineZoom();
        m_origZoomPos = m_ldb->getLensZoomPosition();
        m_lensIf->onLensFocusFineChange(true);
        m_lensIf->GetLensFocusFine();
        emit m_lensIf->LensFocusMotorPositionChanged(0);
        m_lensIf->onLensZoomPositionChange(0);
        m_lensIf->GetLensZoomPosition();
        QGuiApplication::setOverrideCursor(Qt::ArrowCursor);
        break;
    case PageFocusPosition:
        QGuiApplication::setOverrideCursor(Qt::WaitCursor);
        m_lensIf->onLensModeChange(1);
        m_lensIf->GetLensMode();
        m_lensIf->onLensZoomPositionChange(m_origFineZoom ? 1000 : 100);
        QThread::msleep(1000);
        m_lensIf->GetLensZoomPosition();
        _resync_focus_motor_pos();
        QGuiApplication::setOverrideCursor(Qt::ArrowCursor);
        break;
    case PageMotorOffset:
        QGuiApplication::setOverrideCursor(Qt::WaitCursor);
        m_lensIf->onLensZoomPositionChange(0);
        QThread::msleep(1000);
        m_lensIf->GetLensZoomPosition();
        _resync_focus_motor_pos();
        QGuiApplication::setOverrideCursor(Qt::ArrowCursor);
        break;
    case PageOutro:
        QGuiApplication::setOverrideCursor(Qt::WaitCursor);
        m_lensIf->onLensZoomPositionChange(m_origZoomPos);
        QThread::msleep(1000);
        m_lensIf->GetLensZoomPosition();
        _resync_focus_motor_pos();
        QGuiApplication::setOverrideCursor(Qt::ArrowCursor);
        break;
    default:
        break;
    }
}

void ZfcWizard::cleanupPage(int id)
{
    qDebug() << __PRETTY_FUNCTION__ << id; // NOLINT(hicpp-no-array-decay)
    page(id)->cleanupPage(); // the default implementation

    if (id == PageIntro) {
        ui->ZfcMotorOffsetPageObj->ui->cbxFinish->setChecked(false);
        ui->ZfcMotorOffsetPageObj->ui->cbxFinish->setEnabled(false);
    }
}

void ZfcWizard::next()
{
    QGuiApplication::setOverrideCursor(Qt::WaitCursor);
    qDebug() << __PRETTY_FUNCTION__; // NOLINT(hicpp-no-array-decay)
    const bool finish = field("finish").toBool();
    if (currentId() == PageOutro && !finish) {
        back();
        back();
        m_lensIf->onLensZoomPositionChange(m_origFineZoom ? 1000 : 100);
        QThread::msleep(1000);
        m_lensIf->GetLensZoomPosition();
        _resync_focus_motor_pos();
    }
    QGuiApplication::setOverrideCursor(Qt::ArrowCursor);
}

void ZfcWizard::_resync_focus_motor_pos()
{
    qDebug() << __PRETTY_FUNCTION__; // NOLINT(hicpp-no-array-decay)
    static constexpr int LAST_POS_CNT = 8;
    static constexpr int SLEEP_MS = 200;
    static constexpr int TIMEOUT_MS = 20000;

    int idx = 0;
    std::array<int, LAST_POS_CNT> last_pos = { -1 };
    m_lensIf->ResyncFocusMotor();
    last_pos.at(LAST_POS_CNT - 1) = ui->ZfcFocusPosPageObj->getFocusMotorPos();
    m_timer->setSingleShot(true);
    m_timer->setInterval(TIMEOUT_MS);
    m_timer->start();
    while (m_timer->isActive()) {
        if (std::all_of(last_pos.begin(), last_pos.end(),
                        [&last_pos](int x) { return x == last_pos.at(0); })) {
            break;
        }
        m_lensIf->ResyncFocusMotor();
        last_pos.at(idx) = ui->ZfcFocusPosPageObj->getFocusMotorPos();
        // NOLINTNEXTLINE(hicpp-no-array-decay)
        qDebug() << __FUNCTION__ << "(): last_pos.at(" << idx << ") = " << last_pos.at(idx);
        idx += 1;
        idx %= LAST_POS_CNT;
        QThread::msleep(SLEEP_MS);
    }
    m_timer->stop();
}
