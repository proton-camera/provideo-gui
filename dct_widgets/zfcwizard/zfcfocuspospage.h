/*
 * Copyright (C) 2022 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    zfcfocuspospage.h
 * @brief   Zoom Focus Calibration (Wizard) Focus Position Page
 *****************************************************************************/
#ifndef ZFCFOCUSPOSPAGE_H
#define ZFCFOCUSPOSPAGE_H

#include <QWizardPage>

namespace Ui {
class ZfcFocusPosPage;
}

class ZfcFocusPosPage : public QWizardPage
{
    Q_OBJECT

public:
    explicit ZfcFocusPosPage(QWidget *parent = nullptr);
    ~ZfcFocusPosPage() override;

    int getFocusMotorPos() const;

signals:
    void LensFocusMotorPositionChanged(int);

public slots:
    void onLensFocusMotorPositionChange(int value);

public: // NOLINT(readability-redundant-access-specifiers)
    Ui::ZfcFocusPosPage *ui; // NOLINT(misc-non-private-member-variables-in-classes)
};

#endif // FOCUSPOSITIONPAGE_H
