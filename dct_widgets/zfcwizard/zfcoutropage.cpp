/*
 * Copyright (C) 2022 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    zfcoutropage.cpp
 * @brief   Zoom Focus Calibration (Wizard) Outro Page
 *****************************************************************************/
#include "zfcoutropage.h"
#include "ui_zfcoutropage.h"

ZfcOutroPage::ZfcOutroPage(QWidget *parent) : QWizardPage(parent), ui(new Ui::ZfcOutroPage)
{
    ui->setupUi(this);

    // register fields, to be used in the accept() method of the wizard
    registerField("zoomsharpsilent", ui->rbtnZoomSharpSilent);
    registerField("zoomsharp", ui->rbtnZoomSharp);
}

ZfcOutroPage::~ZfcOutroPage()
{
    delete ui;
}
