/*
 * Copyright (C) 2019 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    lensdriverbox.h
 *
 * @brief   Class definition of lens driver box
 *
 *****************************************************************************/
#ifndef LENS_DRIVER_BOX_H
#define LENS_DRIVER_BOX_H

#include <dct_widgets_base.h>
#include <QItemSelection>
#include <QSpinBox>
#include <QTimer>
#include "defines.h"

#include <array>

using lens_settings_t = struct
{
    int version;
    int chipID;
    int controllerFeatures;
    int focusMotorNr;
    int zoomMotorNr;
    int irisMotorNr;
    int filterMotorNr;
    int focusMotorFeatures;
    int zoomMotorFeatures;
    int irisMotorFeatures;
    int filterMotorFeatures;
};

using lens_iris_position_t = struct
{
    QVector<int> fStops;
    QVector<int> fStopsPos;
};

using lens_iris_position_template_t = struct
{
    QVector<int> fStops;
    QVector<int> fStopPos;
    bool invertIris;
    QString lensName;
    QVector<int> compatibleID;
};

/*
 * Special spin box for the torque setting with custom validator
 *
 * The torque can internally only be set in steps of 10. To make the GUI less
 * confusing, use a validator that checks this and adjusts the values
 * accordingly.
 ******************************************************************************/
class TorqueSpinBox : public QSpinBox
{
public:
    explicit TorqueSpinBox(QWidget *parent = nullptr) : QSpinBox(parent)
    {
        // Correct intermediate values to nearest value using fixup()
        // function when editing ends.
        setCorrectionMode(CorrectToNearestValue);
    }

    void fixup(QString &input) const override
    {
        // Round torque to nearest multiple of 10
        int torque = input.toInt();
        if (torque % TORQE_DIVIDER != 0) {
            torque = (int)qRound((float)torque / TORQUE_DIVIDER_FLOAT) * TORQE_DIVIDER;
            input = QString::number(torque);
        }
    }

    QValidator::State validate(QString &input, int &pos) const override
    {
        (void)pos;
        auto torque = input.toInt();

        // Check value range
        if (torque < 0 || torque > MAX_TORQE) {
            return QValidator::Invalid;
        }
        // Check if multiple of TORQUE_DIVIDER
        if (torque % TORQE_DIVIDER == 0) {
            return QValidator::Acceptable;
        }

        // All other cases are valid intermediates, so that they get
        // rounded using fixup() when editing ends.
        return QValidator::Intermediate;
    }

private:
    const int MAX_TORQE = 100;
    const int TORQE_DIVIDER = 10;
    const float TORQUE_DIVIDER_FLOAT = 10.0F;
};

/*
 * Lens Driver Box Widget
 *****************************************************************************/
class LensDriverBox : public DctWidgetBox
{
    Q_OBJECT

public:
    explicit LensDriverBox(QWidget *parent = nullptr);
    ~LensDriverBox() Q_DECL_OVERRIDE;

    QList<int> LensSettings() const;
    int LensMode() const;
    QString LensInvert() const;
    bool LensAutoTorque() const;
    int LensFocusPosition() const;
    QList<int> LensFocusSettings() const;
    int LensZoomPosition() const;
    QList<int> LensZoomSettings() const;
    int LensIrisPosition() const;
    QList<int> LensIrisSettings() const;
    QList<int> LensIrisTable();
    int LensFilterPosition() const;
    QList<int> LensFilterSettings() const;

    void setLensSettings(const QList<int> &settings);
    void setLensMode(int mode);
    void setLensInvert(const QString &mode);
    void setLensAutoTorque(bool enable);
    int getLensFocusPosition();
    void setLensFocusPosition(int);
    void setLensFocusSettings(const QList<int> &settings);
    int getLensZoomPosition();
    void setLensZoomPosition(int);
    void setLensZoomSettings(const QList<int> &settings);
    void setLensIrisPosition(int);
    void setLensIrisSettings(const QList<int> &settings);
    void setLensIrisTable(const QList<int> &table);
    void setLensFilterPosition(int);
    void setLensFilterSettings(const QList<int> &settings);
    bool getLensFineZoom();
    bool getLensFineFocus();
    bool getLensFineIris();

    // Show or hide UI elements
    void setFeaturesVisible(bool fullFeatureSet, bool hasZoomFine, bool hasIrisFine,
                            const QString &lensB4Info);

protected:
    void prepareMode(Mode mode) override;

    void loadSettings(QSettings &s) override;
    void saveSettings(QSettings &s) override;
    void applySettings() override;

    void addLensIrisAperture(const QString &name, int id);
    void addLensIrisTemplate(const QString &name, int id);

private:
    void showLensFeatureGroupBoxes();
    void enableLensMotorSettings(enum LensFeatues features, int motorSettings);
    void processMotorFunctionChange(int indexFront, int indexRear);
    void configureUIforAECSetup();

signals:
    void LensSettingsChanged(QVector<int>);
    void LensActiveChanged(int);
    void LensModeChanged(int);
    void LensInvertChanged(QVector<int>);
    void LensAutoTorqueChanged(bool);

    void irisAvailableChanged(bool);

    void ResyncRequestIris();
    void ResyncFocus();
    void ResyncZoom();
    void ResyncFocusDone(int);

    void LensFocusPositionChanged(int pos);
    void LensFocusFineChanged(bool enable);
    void LensZoomPositionChanged(int pos);
    void LensZoomDirectionChanged(int dir);
    void LensZoomFineChanged(bool enable);
    void LensIrisPositionChanged(int pos);
    void LensFilterPositionChanged(int pos);
    void LensFocusSettingsChanged(QVector<int>);
    void LensZoomSettingsChanged(QVector<int>);
    void LensIrisSettingsChanged(QVector<int>);
    void LensIrisApertureChanged(int apt);
    void LensIrisAperturePosChanged(int pos);
    void LensIrisTableChanged(QVector<int>);
    void LensIrisFineChanged(bool enable);
    void LensFilterSettingsChanged(QVector<int>);

    void StartCalibration();

public slots:
    void onLensSettingsChange(QVector<int> values);
    void onLensActiveChange(int active);
    void onLensModeChange(int mode);
    void onLensInvertChange(QVector<int>);
    void onLensAutoTorqueChange(bool enable);
    void onLensFocusPositionChange(int pos);
    void onLensFocusFineChange(bool enable);
    void onLensZoomPositionChange(int pos);
    void onLensZoomDirectionChange(int dir);
    void onLensZoomFineChange(bool enable);
    void onLensIrisPositionChange(int pos);
    void onLensFilterPositionChange(int pos);
    void onLensFocusSettingsChange(QVector<int>);
    void onLensZoomSettingsChange(QVector<int>);
    void onLensIrisSettingsChange(QVector<int>);
    void onLensIrisTableChange(QVector<int> values);
    void onLensIrisAperturePosChange(int pos);
    void onLensIrisFineChange(bool enable);
    void onLensFilterSettingsChange(QVector<int>);
    void onAecSettingsChange(QVector<int> values);
    void onAecEnableChange(int enable);

private slots:
    void onCbxFrontMotorFunctionChange(int indexFront);
    void onCbxRearMotorFunctionChange(int indexRear);
    void onBtnLensActiveChange();
    void onCbxAutoTorqueChange(int state);

    void onCbxLensModeChanged(int mode);

    void onSbxLensFocusPositionChanged(int pos);
    void onSbxLensFocusSpeedChanged(int speed);
    void onSbxLensFocusStepModeChanged(int stepMode);
    void onSbxLensFocusTorqueChanged(int torque);
    void onCbxLensFocusFineChanged(int enable);
    void onCbxLensFocusInvertChanged(int enable);

    void onSbxLensZoomPositionChanged(int pos);
    void onSbxLensZoomSpeedChanged(int speed);
    void onSbxLensZoomStepModeChanged(int stepMode);
    void onSbxLensZoomTorqueChanged(int torque);
    void onCbxLensZoomFineChanged(int enable);
    void onCbxLensZoomInvertChanged(int enable);

    void onSbxLensIrisPositionChanged(int pos);
    void onSbxLensIrisSpeedChanged(int speed);
    void onSbxLensIrisStepModeChanged(int stepMode);
    void onSbxLensIrisTorqueChanged(int torque);
    void onCbxLensIrisInvertChanged(int enable);
    void onCbxLensIrisApertureChanged(int index);
    void onBtnLensIrisAperturePlusChanged();
    void onBtnLensIrisApertureMinusChanged();
    void onCbxLensIrisFineChanged(int enable);
    void onBtnLensIrisTableTransmitChanged();
    void onBtnLensIrisTableRemoveColumnChanged();
    void onCbxLensIrisTemplateChanged(int index);

    void onSbxLensFilterPositionChanged(int pos);
    void onSbxLensFilterSpeedChanged(int speed);
    void onSbxLensFilterStepModeChanged(int stepMode);
    void onSbxLensFilterTorqueChanged(int torque);
    void onCbxLensFilterInvertChanged(int enable);

private: // NOLINT(readability-redundant-access-specifiers)
    static constexpr int DEFAULT_RANGE = 100;
    static constexpr int EXTENDED_RANGE = 1000;
    static constexpr int RANGE_FACTOR = (EXTENDED_RANGE / DEFAULT_RANGE);
    static constexpr float RANGE_FACTOR_F = static_cast<float>(RANGE_FACTOR);
    QString m_lensB4Info;
    class PrivateData;
    PrivateData *d_data;
};

#endif // LENS_DRIVER_BOX_H
