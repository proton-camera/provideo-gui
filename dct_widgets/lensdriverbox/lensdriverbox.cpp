/*
 * Copyright (C) 2017 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    LensDriverBox.cpp
 *
 * @brief   Implementation of lens driver box
 *
 *****************************************************************************/
#include <QtDebug>
#include <QProxyStyle>

#include <QModelIndex>
#include <QStandardItemModel>
#include <QLineEdit>
#include <QItemDelegate>
#include <QStyledItemDelegate>
#include <QTextEdit>
#include <QFileInfo>
#include <QDir>
#include <QMessageBox>

#include "lensdriverbox.h"
#include "ui_lensdriverbox.h"
#include "defines.h"
#include "inoutbox.h"

/******************************************************************************
 * namespaces
 *****************************************************************************/
namespace Ui {
class UI_LensDriverBox;
}

/******************************************************************************
 * local definitions
 *****************************************************************************/
#define LENSDRIVER_SETTINGS_SECTION_NAME ("LENSDRIVER")

#define LENSDRIVER_SETTINGS_SETTINGS ("settings")
#define LENSDRIVER_SETTINGS_LENS_MODE ("lens_mode")
#define LENSDRIVER_SETTINGS_INVERT ("invert")
#define LENSDRIVER_SETTINGS_AUTO_TORQUE ("auto_torque")

#define LENSDRIVER_SETTINGS_FOCUS_POSITION ("focus_position")
#define LENSDRIVER_SETTINGS_ZOOM_POSITION ("zoom_position")
#define LENSDRIVER_SETTINGS_IRIS_POSITION ("iris_position")
#define LENSDRIVER_SETTINGS_FILTER_POSITION ("filter_position")

#define LENSDRIVER_SETTINGS_FOCUS_SETTINGS ("focus_settings")
#define LENSDRIVER_SETTINGS_ZOOM_SETTINGS ("zoom_settings")
#define LENSDRIVER_SETTINGS_IRIS_SETTINGS ("iris_settings")
#define LENSDRIVER_SETTINGS_IRIS_TABLE ("iris_table")
#define LENSDRIVER_SETTINGS_FILTER_SETTINGS ("filter_settings")

// Defines for the Lens Drive Iris Table Templates
#define LENSDRIVER_LENS_TEMPLATES_SECTION_NAME ("LENS_DRIVER_IRIS_TEMPLATES")
#define LENSDRIVER_LENS_TEMPLATES_NR_OF_TEMPLATES ("number_of_templates")
#define LENSDRIVER_LENS_TEMPLATE ("lens_template_")
#define LENSDRIVER_LENS_FSTOP ("fStops")
#define LENSDRIVER_LENS_FSTOP_POS ("fStop_pos")
#define LENSDRIVER_LENS_INVERT_IRIS ("invert_iris")
#define LENSDRIVER_LENS_COMPATIBLE ("compatible_drive_device")
#define LENSDRIVER_LENS_NAME ("lens_name")

#define IRIS_APT_TABLE_MAX_NO_ROWS (2)
#define IRIS_APT_TABLE_MAX_NO_COLUMNS (9)

#define AUTO_REPEAT_THRESHOLD (5000)

#define FRONT_MOTOR_NR (2)
#define REAR_MOTOR_NR (1)

/*
 * Overrule unconfigureable timer implementation in qt, there's  no property
 * in spinbox class to do this => use proxy-style (Arrggghhh!!!)
 *
 * Problem: Due to a long processing time of some provideo commands the
 *          internal repeat-timer of spin-box gets expired. This leads to an
 *          repetation of setValue event in the spin-box instance by button
 *          releases and finally the spinbox-value is decreased or increased
 *          twice by clicking up- or down-button once.
 *
 * Hope my qt-patch will be accepted to make it configurable.
 *****************************************************************************/
class SpinBoxStyle : public QProxyStyle
{
public:
    explicit SpinBoxStyle(QStyle *style = nullptr) : QProxyStyle(style) { }

    int styleHint(StyleHint hint, const QStyleOption *option = nullptr,
                  const QWidget *widget = nullptr,
                  QStyleHintReturn *returnData = nullptr) const Q_DECL_OVERRIDE
    {
        if (hint == QStyle::SH_SpinBox_ClickAutoRepeatThreshold) {
            return (AUTO_REPEAT_THRESHOLD);
        }

        return (QProxyStyle::styleHint(hint, option, widget, returnData));
    }
};

/*
 * @brief Sorts and checks an F-Stop-To-Position Table given as two vectors.
 *        Both vectors must have the same size.
 * @param a Vector of double values that describe the F-Stops
 * @param b Vector of integer values ranging from 0 to 1000 that describe the
 *          motor position that is associated with the according Fstop value.
 * @returns False if an error occured (e.g. length missmatch), true otherwise
 *****************************************************************************/
namespace {
template<class T1, class T2>
bool checkFstopTable(QVector<T1> &fStops, QVector<T2> &positions)
{
    if (fStops.length() != positions.length() || fStops.length() > IRIS_APT_TABLE_MAX_NO_COLUMNS) {
        return false;
    }

    QList<QPair<T1, T2>> pairList;
    for (int i = 0; i < fStops.length(); i++) {
        // Do not add duplicates to the List
        const QPair<T1, T2> newPair(fStops.at(i), positions.at(i));
        if (!pairList.contains(newPair)) {
            pairList.append(newPair);
        }
    }

    // Sort it (will sort first for x, than for y, see QPair
    std::sort(pairList.begin(), pairList.end());

    // Write the results back into the table
    fStops.clear();
    positions.clear();
    for (int i = (pairList.length() - 1); i >= 0; i--) {
        fStops.append(pairList.at(i).first);
        positions.append(pairList.at(i).second);
    }

    // Check range of the positions
    // While at it check position range
    static const int MAX_POSITION = 1000;
    for (int i = 0; i < fStops.length(); i++) {
        if ((positions[i] < 0) || (positions[i] > MAX_POSITION)) {
            return false;
        }
    }

    // Check for correct order of the positions, they must be in descending order.
    // While at it also check for duplicate f-stops or position values.
    for (int i = 0; i < fStops.length() - 1; i++) {
        if (fStops[i] == fStops[i + 1]
            || (positions[i] <= positions[i + 1] && positions[i + 1] != 0)) {
            return false;
        }
    }

    // Fill table with trailing 0s if needed
    while (fStops.length() < IRIS_APT_TABLE_MAX_NO_COLUMNS) {
        fStops.append(0);
        positions.append(0);
    }

    return true;
}

/*
 * Template functions to convert QVariant to QList (and vice versa)
 * so that we can store lists in QSettings.
 *****************************************************************************/
template<class T>
QVariant toVariant(const QList<T> &list)
{
    QVariantList variantList;
    variantList.reserve(list.size());
    for (const auto &v : list) {
        variantList.append(v);
    }
    return variantList;
}

template<class T>
QList<T> toList(const QVariant &qv)
{
    QList<T> dataList;
    foreach (const QVariant v, qv.value<QVariantList>()) {
        dataList << v.value<T>();
    }
    return dataList;
}
} // end anonymous namespace
/*
 * Delegate
 *****************************************************************************/
class LensDriverIrisTabStyledDelegate : public QStyledItemDelegate
{

signals:
    void modelDataChanged();

public:
    // create a single editable table-cell
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &item,
                          const QModelIndex &index) const override
    {
        (void)item;
        auto *edit = new QLineEdit(parent);

        // set validator
        QValidator *valid = nullptr;

        if (index.row() == 1) {
            valid = new QIntValidator(0, DEFAULT_INT_VAL_RANGE, edit);
        } else {
            valid = new QDoubleValidator(0.0, DEFAULT_DBL_VAL_RANGE, 1, edit);
        }
        edit->setValidator(valid);

        return (edit);
    }

    // transfer value from data-model into line-edit
    void setEditorData(QWidget *editor, const QModelIndex &idx) const Q_DECL_OVERRIDE
    {
        if (idx.model()->data(idx, Qt::EditRole).userType() == QVariant::Int) {
            auto value = idx.model()->data(idx, Qt::EditRole).toInt();
            auto *edt = dynamic_cast<QLineEdit *>(editor);
            edt->setText(QString().setNum(value));
        } else {
            auto value = idx.model()->data(idx, Qt::EditRole).toDouble();
            auto *edt = dynamic_cast<QLineEdit *>(editor);
            edt->setText(edt->locale().toString(value));
        }
    }

    // transfer value from line_edit into data-model
    void setModelData(QWidget *editor, QAbstractItemModel *model,
                      const QModelIndex &idx) const Q_DECL_OVERRIDE
    {
        auto *edt = dynamic_cast<QLineEdit *>(editor);
        auto value = edt->text();
        if (idx.row() == 1) {
            model->setData(idx, value.toInt(), Qt::EditRole);
        } else {
            model->setData(idx, edt->locale().toDouble(value), Qt::EditRole);
        }
    }

    // set geometry of line-edit
    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option,
                              const QModelIndex &idx) const Q_DECL_OVERRIDE
    {
        (void)idx;
        editor->setGeometry(option.rect);
    }

    QString displayText(const QVariant &value, const QLocale &locale) const override
    {
        QString text;
        switch (value.userType()) {
        case QMetaType::Float:
        case QVariant::Double:
            text = locale.toString(value.toDouble());
            break;
        case QVariant::Int:
        case QVariant::LongLong:
        default:
            text = locale.toString(value.toLongLong());
            break;
        }

        return text;
    }

private:
    static constexpr int DEFAULT_INT_VAL_RANGE = 1000;
    static constexpr int DEFAULT_DBL_VAL_RANGE = 1000.0;
};

/*
 * LensDriverBox::PrivateData
 *****************************************************************************/
class LensDriverBox::PrivateData
{
public:
    explicit PrivateData(QWidget *parent)
        : m_ui(new Ui::UI_LensDriverBox),
          m_sbxStyle(new SpinBoxStyle()),
          m_delegate(new LensDriverIrisTabStyledDelegate()),
          m_model(new QStandardItemModel(0, IRIS_APT_TABLE_MAX_NO_COLUMNS, nullptr)),
          m_LensSettings{},
          m_LensIrisTable{},
          m_AecSetup{}
    {
        // initialize UI
        m_ui->setupUi(parent);
        initDataModel();
        m_ui->tblIrisAptPosition->setItemDelegate(m_delegate);
        m_ui->tblIrisAptPosition->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
        m_ui->tblIrisAptPosition->setSelectionBehavior(QAbstractItemView::SelectColumns);
        m_ui->tblIrisAptPosition->clearFocus();
        setDataModel();
    }

    ~PrivateData()
    {
        delete m_ui;
        delete m_sbxStyle;
        delete m_model;
    }

    /*
     * initDataModel()
     *****************************************************************************/
    // initialize data model
    void initDataModel() const
    {
        m_model->setVerticalHeaderItem(0, new QStandardItem(QString("F-Stop")));
        m_model->setVerticalHeaderItem(1, new QStandardItem(QString("Position")));
    }

    /*
     * setDataModel()
     *****************************************************************************/
    // replace data model of the table view widget with our custom model
    void setDataModel() const
    {
        // see http://doc.qt.io/qt-5.7/qabstractitemview.html#setModel
        // get pointer to old selection model
        QItemSelectionModel *oldm = m_ui->tblIrisAptPosition->selectionModel();
        // set new data model
        m_ui->tblIrisAptPosition->setModel(m_model);
        // delete old selection model
        delete oldm;
    }

    /*
     * fillTable( QVector<double> &Fstop, QVector<int> &FstopPos )
     *****************************************************************************/
    // set positions in tableview widget
    void fillTable(QVector<double> &Fstop, QVector<int> &FstopPos) const
    {
        Q_ASSERT(Fstop.count() == FstopPos.count());
        Q_ASSERT(m_model != nullptr);

        // update data model (add or remove rows)
        auto numColModel = m_model->columnCount();
        auto numColInput = Fstop.count();
        if (numColModel < numColInput) {
            m_model->insertColumns(0, numColInput - numColModel);
        } else {
            m_model->removeColumns(0, numColInput - numColModel);
        }

        // set values
        for (int i = 0; i < Fstop.count(); ++i) {
            auto Fstop_i = m_model->index(0, i, QModelIndex());
            auto FstopPos_i = m_model->index(1, i, QModelIndex());

            // set alignment
            m_model->setData(Fstop_i, QVariant(Qt::AlignVCenter | Qt::AlignRight),
                             Qt::TextAlignmentRole);
            m_model->setData(FstopPos_i, QVariant(Qt::AlignVCenter | Qt::AlignRight),
                             Qt::TextAlignmentRole);

            // set used or clear unsed sample values
            m_model->setData(Fstop_i, Fstop[i]);
            m_model->setData(FstopPos_i, FstopPos[i]);
        }
    }

    /*
     * getDataFromModel( QVector<double> &Fstop, QVector<int> &FstopPos  )
     *****************************************************************************/
    void getDataFromModel(QVector<double> &Fstop, QVector<int> &FstopPos) const
    {
        Fstop.clear();
        FstopPos.clear();

        for (int i = 0; i < m_model->columnCount(); i++) {
            Fstop.append(m_model->data(m_model->index(0, i), Qt::DisplayRole).toDouble());
            FstopPos.append(m_model->data(m_model->index(1, i), Qt::DisplayRole).toInt());
        }
    }

    /*
     * getLensesFromFile( lens_iris_position_template_t )
     *****************************************************************************/
    static bool getLensesFromFile(QVector<lens_iris_position_template_t> *table)
    {
        auto m_filename = QDir::fromNativeSeparators(QDir::currentPath() + QDir::separator()
                                                     + "tools_and_configs" + QDir::separator()
                                                     + "SupportedLenses.txt");

        const QFileInfo check_file(m_filename);

        lens_iris_position_template_t temp_template;
        QString temp_compatIDs;
        QString temp_fstops;
        QString temp_fstop_pos;

        if (check_file.exists() && check_file.isFile()) {
            // Open settings
            QSettings settings(m_filename, QSettings::IniFormat);

            // Load the device name and platform from the settings file
            settings.beginGroup(LENSDRIVER_LENS_TEMPLATES_SECTION_NAME);
            auto templates_nr = settings.value(LENSDRIVER_LENS_TEMPLATES_NR_OF_TEMPLATES).toInt();
            settings.endGroup();

            for (int i = 0; i < templates_nr; i++) {
                settings.beginGroup(QString("%1%2").arg(LENSDRIVER_LENS_TEMPLATE).arg(i + 1));

                temp_template.lensName = settings.value(LENSDRIVER_LENS_NAME).toString();
                temp_template.invertIris = settings.value(LENSDRIVER_LENS_INVERT_IRIS).toBool();

                temp_compatIDs = settings.value(LENSDRIVER_LENS_COMPATIBLE).toString();
                temp_fstops = settings.value(LENSDRIVER_LENS_FSTOP).toString();
                temp_fstop_pos = settings.value(LENSDRIVER_LENS_FSTOP_POS).toString();

                settings.endGroup();

                static const QRegularExpression regex("\\s+");
                const QStringList temp_id_list = temp_compatIDs.split(regex);
                const QStringList temp_fstop_list = temp_fstops.split(regex);
                const QStringList temp_fstop_pos_list = temp_fstop_pos.split(regex);

                for (int k = 0; k < temp_id_list.length(); k++) {
                    temp_template.compatibleID.append(temp_id_list.value(k).toInt());
                }
                for (int k = 0; k < temp_fstop_list.length(); k++) {
                    temp_template.fStops.append(temp_fstop_list.value(k).toInt());
                }
                for (int k = 0; k < temp_fstop_pos_list.length(); k++) {
                    temp_template.fStopPos.append(temp_fstop_pos_list.value(k).toInt());
                }

                if (checkFstopTable(temp_template.fStops, temp_template.fStopPos)) {
                    // Table is ok, add to templates
                    table->append(temp_template);
                } else {
                    QMessageBox::warning(nullptr, "Invalid F-Stop table in template file",
                                         QString("The profile '%1' from the template file is "
                                                 "invalid. It contains out of range values "
                                                 "or the values are in the wrong order.\n\n"
                                                 "Make sure that both F-Stop values and positions "
                                                 "are in descending order and the "
                                                 "positions are in the range 0 to 100. If "
                                                 "descending order of the positions is not "
                                                 "possible due to the lens mechanics, use the "
                                                 "invert motor function.")
                                                 .arg(temp_template.lensName));
                }

                temp_template.fStops.clear();
                temp_template.fStopPos.clear();
                temp_template.compatibleID.clear();
                temp_template.lensName.clear();
            }

            return true;
        }

        return false;
    }

    /*
     * setLensesToFile( lens_iris_position_template_t )
     *****************************************************************************/
    static bool setLensesToFile(const QVector<lens_iris_position_template_t> &table)
    {
        const QString m_filename = "SupportedLenses.txt";

        QString temp_fstops;
        QString temp_fstopPos;

        // Open settings file and make sure it is clean
        QSettings settings(m_filename, QSettings::IniFormat);
        settings.clear();

        // Write the device name and platform into the settings file
        settings.beginGroup(LENSDRIVER_LENS_TEMPLATES_SECTION_NAME);
        settings.setValue(LENSDRIVER_LENS_TEMPLATES_NR_OF_TEMPLATES, table.length());
        settings.endGroup();

        for (int i = 0; i < table.length(); i++) {
            settings.beginGroup(QString("%1%2").arg(LENSDRIVER_LENS_TEMPLATE).arg(i + 1));

            settings.setValue(LENSDRIVER_LENS_NAME, table.value(i).lensName);
            settings.setValue(LENSDRIVER_LENS_COMPATIBLE, table.value(i).compatibleID.first());

            for (int k = 0; k < table.value(i).fStops.length(); k++) {
                temp_fstops.append(QString("%1 ").arg(table.value(i).fStops.value(k)));
                temp_fstopPos.append(QString("%1 ").arg(table.value(i).fStopPos.value(k)));
            }

            settings.setValue(LENSDRIVER_LENS_FSTOP, temp_fstops);
            settings.setValue(LENSDRIVER_LENS_FSTOP_POS, temp_fstopPos);

            temp_fstops.clear();
            temp_fstopPos.clear();

            settings.endGroup();
        }

        return true;
    }

    // NOLINTBEGIN(misc-non-private-member-variables-in-classes)
    Ui::UI_LensDriverBox *m_ui;
    SpinBoxStyle *m_sbxStyle;

    LensDriverIrisTabStyledDelegate *m_delegate;
    QStandardItemModel *m_model;
    lens_settings_t m_LensSettings;
    lens_iris_position_t m_LensIrisTable;
    QVector<lens_iris_position_template_t> m_LensIrisTemplates;
    aec_setup_t m_AecSetup;

    bool m_FullFeatureSet = true;
    // NOLINTEND(misc-non-private-member-variables-in-classes)
};

/*
 * LensDriverBox::LensDriverBox
 *****************************************************************************/
LensDriverBox::LensDriverBox(QWidget *parent) : DctWidgetBox(parent), d_data(new PrivateData(this))
{
    // overrule auto-repeat threshold
    d_data->m_ui->sbxFocusPosition->setStyle(d_data->m_sbxStyle);
    d_data->m_ui->sbxFocusSpeed->setStyle(d_data->m_sbxStyle);
    d_data->m_ui->sbxFocusStepMode->setStyle(d_data->m_sbxStyle);
    d_data->m_ui->sbxFocusTorque->setStyle(d_data->m_sbxStyle);

    d_data->m_ui->sbxZoomPosition->setStyle(d_data->m_sbxStyle);
    d_data->m_ui->sbxZoomSpeed->setStyle(d_data->m_sbxStyle);
    d_data->m_ui->sbxZoomStepMode->setStyle(d_data->m_sbxStyle);
    d_data->m_ui->sbxZoomTorque->setStyle(d_data->m_sbxStyle);

    d_data->m_ui->sbxIrisPosition->setStyle(d_data->m_sbxStyle);
    d_data->m_ui->sbxIrisSpeed->setStyle(d_data->m_sbxStyle);
    d_data->m_ui->sbxIrisStepMode->setStyle(d_data->m_sbxStyle);
    d_data->m_ui->sbxIrisTorque->setStyle(d_data->m_sbxStyle);

    d_data->m_ui->sbxFilterPosition->setStyle(d_data->m_sbxStyle);
    d_data->m_ui->sbxFilterSpeed->setStyle(d_data->m_sbxStyle);
    d_data->m_ui->sbxFilterStepMode->setStyle(d_data->m_sbxStyle);
    d_data->m_ui->sbxFilterTorque->setStyle(d_data->m_sbxStyle);

    d_data->m_ui->sbxFocusPosition->setRange(0, DEFAULT_RANGE);
    d_data->m_ui->sbxFocusSpeed->setRange(0, DEFAULT_RANGE);
    d_data->m_ui->sbxFocusStepMode->setRange(0, DEFAULT_RANGE);
    d_data->m_ui->sbxFocusTorque->setRange(0, DEFAULT_RANGE);

    d_data->m_ui->sbxZoomPosition->setRange(0, DEFAULT_RANGE);
    d_data->m_ui->sbxZoomSpeed->setRange(0, DEFAULT_RANGE);
    d_data->m_ui->sbxZoomStepMode->setRange(0, DEFAULT_RANGE);
    d_data->m_ui->sbxZoomTorque->setRange(0, DEFAULT_RANGE);

    d_data->m_ui->sbxIrisPosition->setRange(0, DEFAULT_RANGE);
    d_data->m_ui->sbxIrisSpeed->setRange(0, DEFAULT_RANGE);
    d_data->m_ui->sbxIrisStepMode->setRange(0, DEFAULT_RANGE);
    d_data->m_ui->sbxIrisTorque->setRange(0, DEFAULT_RANGE);

    d_data->m_ui->sbxFilterPosition->setRange(0, DEFAULT_RANGE);
    d_data->m_ui->sbxFilterSpeed->setRange(0, DEFAULT_RANGE);
    d_data->m_ui->sbxFilterStepMode->setRange(0, DEFAULT_RANGE);
    d_data->m_ui->sbxFilterTorque->setRange(0, DEFAULT_RANGE);

    d_data->m_ui->sldFocusPosition->setRange(0, DEFAULT_RANGE);
    d_data->m_ui->sldZoomPosition->setRange(0, DEFAULT_RANGE);
    d_data->m_ui->sldIrisPosition->setRange(0, DEFAULT_RANGE);
    d_data->m_ui->sldFilterPosition->setRange(0, DEFAULT_RANGE);

    d_data->m_ui->btnActivateLens->clearFocus();

    d_data->m_ui->lblFocusSpeed->setVisible(false);
    d_data->m_ui->lblFocusStepMode->setVisible(false);
    d_data->m_ui->lblFocusTorque->setVisible(false);
    d_data->m_ui->lblFocusTorqueInfo->setVisible(false);
    d_data->m_ui->lblFocusTorqueInfo->setStyleSheet("QLabel { color : yellow; }");

    d_data->m_ui->sbxFocusSpeed->setVisible(false);
    d_data->m_ui->sbxFocusStepMode->setVisible(false);
    d_data->m_ui->sbxFocusTorque->setVisible(false);

    d_data->m_ui->lblZoomSpeed->setVisible(false);
    d_data->m_ui->lblZoomStepMode->setVisible(false);
    d_data->m_ui->lblZoomTorque->setVisible(false);
    d_data->m_ui->lblZoomTorqueInfo->setVisible(false);
    d_data->m_ui->lblZoomTorqueInfo->setStyleSheet("QLabel { color : yellow; }");

    d_data->m_ui->sbxZoomSpeed->setVisible(false);
    d_data->m_ui->sbxZoomStepMode->setVisible(false);
    d_data->m_ui->sbxZoomTorque->setVisible(false);

    d_data->m_ui->lblIrisSpeed->setVisible(false);
    d_data->m_ui->lblIrisStepMode->setVisible(false);
    d_data->m_ui->lblIrisTorque->setVisible(false);
    d_data->m_ui->lblIrisTorqueInfo->setVisible(false);
    d_data->m_ui->lblIrisTorqueInfo->setStyleSheet("QLabel { color : yellow; }");

    d_data->m_ui->sbxIrisSpeed->setVisible(false);
    d_data->m_ui->sbxIrisStepMode->setVisible(false);
    d_data->m_ui->sbxIrisTorque->setVisible(false);

    d_data->m_ui->lblFilterSpeed->setVisible(false);
    d_data->m_ui->lblFilterStepMode->setVisible(false);
    d_data->m_ui->lblFilterTorque->setVisible(false);
    d_data->m_ui->lblFilterTorqueInfo->setVisible(false);
    d_data->m_ui->lblFilterTorqueInfo->setStyleSheet("QLabel { color : yellow; }");

    d_data->m_ui->sbxFilterSpeed->setVisible(false);
    d_data->m_ui->sbxFilterStepMode->setVisible(false);
    d_data->m_ui->sbxFilterTorque->setVisible(false);

    d_data->m_ui->cbxFocusInvertEnable->setVisible(false);
    d_data->m_ui->cbxZoomInvertEnable->setVisible(false);
    d_data->m_ui->cbxIrisInvertEnable->setVisible(false);
    d_data->m_ui->cbxFilterInvertEnable->setVisible(false);

    d_data->m_ui->cbxIrisTableTemplate->setVisible(false);
    d_data->m_ui->lblIrisTableUseTemplate->setVisible(false);
    d_data->m_ui->btnIrisTransmitTable->setVisible(false);
    d_data->m_ui->btnDeleteColumn->setVisible(false);
    d_data->m_ui->lblAutoIrisStatus->clear();

    if (!PrivateData::getLensesFromFile(&d_data->m_LensIrisTemplates)) {
        QMessageBox::warning(this, "Lens driver template file not found",
                             "The template file for the supported lenses was not found. "
                             "Please make sure that the file 'SupportedLenses.txt' is "
                             "placed in a folder named 'tools_and_configs' which is placed "
                             "in the same folder that 'ProVideo.exe' is placed.\n\n"
                             "Please restart the GUI after the file was restored to use "
                             "the available templates.");
    }

    addLensIrisAperture("Select", 0);

    // connect internal signals
    connect(d_data->m_ui->cbxFrontMotorFunction, SIGNAL(currentIndexChanged(int)), this,
            SLOT(onCbxFrontMotorFunctionChange(int)));
    connect(d_data->m_ui->cbxRearMotorFunction, SIGNAL(currentIndexChanged(int)), this,
            SLOT(onCbxRearMotorFunctionChange(int)));
    connect(d_data->m_ui->btnActivateLens, SIGNAL(clicked()), this, SLOT(onBtnLensActiveChange()));
    connect(d_data->m_ui->cbxAutoTorqueCalib, SIGNAL(stateChanged(int)), this,
            SLOT(onCbxAutoTorqueChange(int)));

    // Lens Mode
    connect(d_data->m_ui->cbxLensMode, SIGNAL(currentIndexChanged(int)), this,
            SLOT(onCbxLensModeChanged(int)));

    // Zoom Focus Calibration Start
    connect(d_data->m_ui->btnStartCalibration, SIGNAL(clicked()), this, SIGNAL(StartCalibration()));

    // Focus Elements
    connect(d_data->m_ui->sbxFocusPosition, SIGNAL(valueChanged(int)), this,
            SLOT(onSbxLensFocusPositionChanged(int)));
    connect(d_data->m_ui->sldFocusPosition, SIGNAL(valueChanged(int)),
            d_data->m_ui->sbxFocusPosition, SLOT(setValue(int)));
    connect(d_data->m_ui->sbxFocusPosition, SIGNAL(valueChanged(int)),
            d_data->m_ui->sldFocusPosition, SLOT(setValue(int)));
    connect(d_data->m_ui->sbxFocusSpeed, SIGNAL(valueChanged(int)), this,
            SLOT(onSbxLensFocusSpeedChanged(int)));
    connect(d_data->m_ui->sbxFocusStepMode, SIGNAL(valueChanged(int)), this,
            SLOT(onSbxLensFocusStepModeChanged(int)));
    connect(d_data->m_ui->sbxFocusTorque, SIGNAL(valueChanged(int)), this,
            SLOT(onSbxLensFocusTorqueChanged(int)));
    connect(d_data->m_ui->cbxFocusFineEnable, SIGNAL(stateChanged(int)), this,
            SLOT(onCbxLensFocusFineChanged(int)));
    connect(d_data->m_ui->cbxFocusInvertEnable, SIGNAL(stateChanged(int)), this,
            SLOT(onCbxLensFocusInvertChanged(int)));

    // Zoom Elements
    connect(d_data->m_ui->sbxZoomPosition, SIGNAL(valueChanged(int)), this,
            SLOT(onSbxLensZoomPositionChanged(int)));
    connect(d_data->m_ui->sldZoomPosition, SIGNAL(valueChanged(int)), d_data->m_ui->sbxZoomPosition,
            SLOT(setValue(int)));
    connect(d_data->m_ui->sbxZoomPosition, SIGNAL(valueChanged(int)), d_data->m_ui->sldZoomPosition,
            SLOT(setValue(int)));
    connect(d_data->m_ui->sbxZoomSpeed, SIGNAL(valueChanged(int)), this,
            SLOT(onSbxLensZoomSpeedChanged(int)));
    connect(d_data->m_ui->sbxZoomStepMode, SIGNAL(valueChanged(int)), this,
            SLOT(onSbxLensZoomStepModeChanged(int)));
    connect(d_data->m_ui->sbxZoomTorque, SIGNAL(valueChanged(int)), this,
            SLOT(onSbxLensZoomTorqueChanged(int)));
    connect(d_data->m_ui->cbxZoomFineEnable, SIGNAL(stateChanged(int)), this,
            SLOT(onCbxLensZoomFineChanged(int)));
    connect(d_data->m_ui->cbxZoomInvertEnable, SIGNAL(stateChanged(int)), this,
            SLOT(onCbxLensZoomInvertChanged(int)));
    connect(d_data->m_ui->sldZoomDirection, QOverload<int>::of(&QSlider::valueChanged), this,
            [=](int dir) { emit LensZoomDirectionChanged(dir); });
    connect(d_data->m_ui->sldZoomDirection, &QSlider::sliderReleased, this, [=]() {
        d_data->m_ui->sldZoomDirection->setValue(0);
        emit ResyncZoom();
    });

    // Iris Elements
    connect(d_data->m_ui->sbxIrisPosition, SIGNAL(valueChanged(int)), this,
            SLOT(onSbxLensIrisPositionChanged(int)));
    connect(d_data->m_ui->sldIrisPosition, SIGNAL(valueChanged(int)), d_data->m_ui->sbxIrisPosition,
            SLOT(setValue(int)));
    connect(d_data->m_ui->sbxIrisPosition, SIGNAL(valueChanged(int)), d_data->m_ui->sldIrisPosition,
            SLOT(setValue(int)));
    connect(d_data->m_ui->sbxIrisSpeed, SIGNAL(valueChanged(int)), this,
            SLOT(onSbxLensIrisSpeedChanged(int)));
    connect(d_data->m_ui->sbxIrisStepMode, SIGNAL(valueChanged(int)), this,
            SLOT(onSbxLensIrisStepModeChanged(int)));
    connect(d_data->m_ui->sbxIrisTorque, SIGNAL(valueChanged(int)), this,
            SLOT(onSbxLensIrisTorqueChanged(int)));
    connect(d_data->m_ui->cbxIrisInvertEnable, SIGNAL(stateChanged(int)), this,
            SLOT(onCbxLensIrisInvertChanged(int)));
    connect(d_data->m_ui->cbxIrisAperture, SIGNAL(currentIndexChanged(int)), this,
            SLOT(onCbxLensIrisApertureChanged(int)));

    connect(this, SIGNAL(LensIrisAperturePosChanged(int)), this,
            SLOT(onLensIrisPositionChange(int)));
    connect(this, SIGNAL(LensIrisAperturePosChanged(int)), this,
            SLOT(onLensIrisAperturePosChange(int)));

    connect(d_data->m_ui->cbxIrisFineEnable, QOverload<int>::of(&QCheckBox::stateChanged), this,
            [=](int enable) { onCbxLensIrisFineChanged(enable); });

    connect(d_data->m_ui->btnIrisAptPlus, SIGNAL(clicked()), this,
            SLOT(onBtnLensIrisAperturePlusChanged()));
    connect(d_data->m_ui->btnIrisAptMinus, SIGNAL(clicked()), this,
            SLOT(onBtnLensIrisApertureMinusChanged()));
    connect(d_data->m_ui->btnIrisTransmitTable, SIGNAL(clicked()), this,
            SLOT(onBtnLensIrisTableTransmitChanged()));
    connect(this, SIGNAL(LensIrisTableChanged(QVector<int>)), this,
            SLOT(onLensIrisTableChange(QVector<int>)));
    connect(d_data->m_ui->cbxIrisTableTemplate, SIGNAL(currentIndexChanged(int)), this,
            SLOT(onCbxLensIrisTemplateChanged(int)));
    connect(d_data->m_ui->btnDeleteColumn, SIGNAL(clicked()), this,
            SLOT(onBtnLensIrisTableRemoveColumnChanged()));

    // Filter Elements
    connect(d_data->m_ui->sbxFilterPosition, SIGNAL(valueChanged(int)), this,
            SLOT(onSbxLensFilterPositionChanged(int)));
    connect(d_data->m_ui->sldFilterPosition, SIGNAL(valueChanged(int)),
            d_data->m_ui->sbxFilterPosition, SLOT(setValue(int)));
    connect(d_data->m_ui->sbxFilterPosition, SIGNAL(valueChanged(int)),
            d_data->m_ui->sldFilterPosition, SLOT(setValue(int)));
    connect(d_data->m_ui->sbxFilterSpeed, SIGNAL(valueChanged(int)), this,
            SLOT(onSbxLensFilterSpeedChanged(int)));
    connect(d_data->m_ui->sbxFilterStepMode, SIGNAL(valueChanged(int)), this,
            SLOT(onSbxLensFilterStepModeChanged(int)));
    connect(d_data->m_ui->sbxFilterTorque, SIGNAL(valueChanged(int)), this,
            SLOT(onSbxLensFilterTorqueChanged(int)));
    connect(d_data->m_ui->cbxFilterInvertEnable, SIGNAL(stateChanged(int)), this,
            SLOT(onCbxLensFilterInvertChanged(int)));

    ////////////////////
    // operation mode
    ////////////////////
    prepareMode(mode());
}

/*
 * LensDriverBox::~LensDriverBox
 *****************************************************************************/
LensDriverBox::~LensDriverBox()
{
    delete d_data;
}

/*
 * LensDriverBox::prepareMode
 *****************************************************************************/
void LensDriverBox::prepareMode(const Mode mode)
{
    (void)mode;
}

/*
 * LensDriverBox::loadSettings
 *****************************************************************************/
void LensDriverBox::loadSettings(QSettings &s)
{
    s.beginGroup(LENSDRIVER_SETTINGS_SECTION_NAME);
    setLensSettings(toList<int>(s.value(LENSDRIVER_SETTINGS_SETTINGS)));
    setLensMode(s.value(LENSDRIVER_SETTINGS_LENS_MODE).toInt());
    setLensInvert(s.value(LENSDRIVER_SETTINGS_INVERT).toString());
    setLensAutoTorque(s.value(LENSDRIVER_SETTINGS_AUTO_TORQUE).toBool());
    setLensFocusPosition(s.value(LENSDRIVER_SETTINGS_FOCUS_POSITION).toInt());
    setLensFocusSettings(toList<int>(s.value(LENSDRIVER_SETTINGS_FOCUS_SETTINGS)));
    setLensZoomPosition(s.value(LENSDRIVER_SETTINGS_ZOOM_POSITION).toInt());
    setLensZoomSettings(toList<int>(s.value(LENSDRIVER_SETTINGS_ZOOM_SETTINGS)));
    setLensIrisPosition(s.value(LENSDRIVER_SETTINGS_IRIS_POSITION).toInt());
    setLensIrisSettings(toList<int>(s.value(LENSDRIVER_SETTINGS_IRIS_SETTINGS)));
    setLensIrisTable(toList<int>(s.value(LENSDRIVER_SETTINGS_IRIS_TABLE)));
    setLensFilterPosition(s.value(LENSDRIVER_SETTINGS_FILTER_POSITION).toInt());
    setLensFilterSettings(toList<int>(s.value(LENSDRIVER_SETTINGS_FILTER_SETTINGS)));
    s.endGroup();
}

/*
 * LensDriverBox::saveSettings
 *****************************************************************************/
void LensDriverBox::saveSettings(QSettings &s)
{
    s.beginGroup(LENSDRIVER_SETTINGS_SECTION_NAME);
    s.setValue(LENSDRIVER_SETTINGS_SETTINGS, toVariant(LensSettings()));
    s.setValue(LENSDRIVER_SETTINGS_LENS_MODE, LensMode());
    s.setValue(LENSDRIVER_SETTINGS_INVERT, LensInvert());
    s.setValue(LENSDRIVER_SETTINGS_AUTO_TORQUE, LensAutoTorque());
    s.setValue(LENSDRIVER_SETTINGS_FOCUS_POSITION, LensFocusPosition());
    s.setValue(LENSDRIVER_SETTINGS_FOCUS_SETTINGS, toVariant(LensFocusSettings()));
    s.setValue(LENSDRIVER_SETTINGS_ZOOM_POSITION, LensZoomPosition());
    s.setValue(LENSDRIVER_SETTINGS_ZOOM_SETTINGS, toVariant(LensZoomSettings()));
    s.setValue(LENSDRIVER_SETTINGS_IRIS_POSITION, LensIrisPosition());
    s.setValue(LENSDRIVER_SETTINGS_IRIS_SETTINGS, toVariant(LensIrisSettings()));
    s.setValue(LENSDRIVER_SETTINGS_IRIS_TABLE, toVariant(LensIrisTable()));
    s.setValue(LENSDRIVER_SETTINGS_FILTER_POSITION, LensFilterPosition());
    s.setValue(LENSDRIVER_SETTINGS_FILTER_SETTINGS, toVariant(LensFilterSettings()));
    s.endGroup();
}

/*
 * LensDriverBox::applySettings
 *****************************************************************************/
void LensDriverBox::applySettings() { }

/*
 * LensDriverBox::addLensIrisAperture
 *****************************************************************************/
void LensDriverBox::addLensIrisAperture(const QString &name, int id)
{
    d_data->m_ui->cbxIrisAperture->blockSignals(true);
    d_data->m_ui->cbxIrisAperture->addItem(name, id);
    d_data->m_ui->cbxIrisAperture->blockSignals(false);
}

/*
 * LensDriverBox::addLensIrisTemplate
 *****************************************************************************/
void LensDriverBox::addLensIrisTemplate(const QString &name, int id)
{
    d_data->m_ui->cbxIrisTableTemplate->blockSignals(true);
    d_data->m_ui->cbxIrisTableTemplate->addItem(name, id);
    d_data->m_ui->cbxIrisTableTemplate->blockSignals(false);
}
/*
 * LensDriverBox::LensSettings
 *****************************************************************************/
QList<int> LensDriverBox::LensSettings() const
{
    QList<int> values;

    values.append(d_data->m_LensSettings.version);
    values.append(d_data->m_LensSettings.chipID);
    values.append(d_data->m_LensSettings.controllerFeatures);
    values.append(d_data->m_LensSettings.focusMotorNr);
    values.append(d_data->m_LensSettings.zoomMotorNr);
    values.append(d_data->m_LensSettings.irisMotorNr);
    values.append(d_data->m_LensSettings.filterMotorNr);
    values.append(d_data->m_LensSettings.focusMotorFeatures);
    values.append(d_data->m_LensSettings.zoomMotorFeatures);
    values.append(d_data->m_LensSettings.irisMotorFeatures);
    values.append(d_data->m_LensSettings.filterMotorFeatures);

    return (values);
}

int LensDriverBox::LensMode() const
{
    return d_data->m_ui->cbxLensMode->currentIndex();
}

/*
 * LensDriverBox::LensProfile
 *****************************************************************************/
QString LensDriverBox::LensInvert() const
{
    QString settings;

    int val = static_cast<int>(d_data->m_ui->cbxFocusInvertEnable->isChecked());
    settings.append(QString("%1").arg(val));
    settings.append(" ");
    val = static_cast<int>(d_data->m_ui->cbxZoomInvertEnable->isChecked());
    settings.append(QString("%1").arg(val));
    settings.append(" ");
    val = static_cast<int>(d_data->m_ui->cbxIrisInvertEnable->isChecked());
    settings.append(QString("%1").arg(val));
    settings.append(" ");
    val = static_cast<int>(d_data->m_ui->cbxFilterInvertEnable->isChecked());
    settings.append(QString("%1").arg(val));

    return (settings);
}

/*
 * LensDriverBox::LensAutoTorque
 *****************************************************************************/
bool LensDriverBox::LensAutoTorque() const
{
    return (d_data->m_ui->cbxAutoTorqueCalib->isChecked());
}

/*
 * LensDriverBox::LensFocusPosition
 *****************************************************************************/
int LensDriverBox::LensFocusPosition() const
{
    return (d_data->m_ui->sbxFocusPosition->value());
}

/*
 * LensDriverBox::LensFocusSettings
 *****************************************************************************/
QList<int> LensDriverBox::LensFocusSettings() const
{
    QList<int> settings;
    settings.append(d_data->m_ui->sbxFocusSpeed->value());
    settings.append(d_data->m_ui->sbxFocusStepMode->value());
    settings.append(d_data->m_ui->sbxFocusTorque->value());
    settings.append((int)d_data->m_ui->cbxFocusFineEnable->isChecked());

    return (settings);
}

/*
 * LensDriverBox::LensZoomPosition
 *****************************************************************************/
int LensDriverBox::LensZoomPosition() const
{
    return (d_data->m_ui->sbxZoomPosition->value());
}

/*
 * LensDriverBox::LensZoomSettings
 *****************************************************************************/
QList<int> LensDriverBox::LensZoomSettings() const
{
    QList<int> settings;
    settings.append(d_data->m_ui->sbxZoomSpeed->value());
    settings.append(d_data->m_ui->sbxZoomStepMode->value());
    settings.append(d_data->m_ui->sbxZoomTorque->value());
    settings.append((int)d_data->m_ui->cbxZoomFineEnable->isChecked());

    return (settings);
}

/*
 * LensDriverBox::LensIrisPosition
 *****************************************************************************/
int LensDriverBox::LensIrisPosition() const
{
    return (d_data->m_ui->sbxIrisPosition->value());
}

/*
 * LensDriverBox::LensIrisSettings
 *****************************************************************************/
QList<int> LensDriverBox::LensIrisSettings() const
{
    QList<int> settings;
    settings.append(d_data->m_ui->sbxIrisSpeed->value());
    settings.append(d_data->m_ui->sbxIrisStepMode->value());
    settings.append(d_data->m_ui->sbxIrisTorque->value());

    return (settings);
}

/*
 * LensDriverBox::LensIrisTable
 *****************************************************************************/
QList<int> LensDriverBox::LensIrisTable()
{
    QList<int> table;
    QVector<double> tempFstop;
    QVector<int> tempFstopPos;

    d_data->getDataFromModel(tempFstop, tempFstopPos);
    if (!checkFstopTable(tempFstop, tempFstopPos)) {
        QMessageBox::warning(
                this, "Trying to save invalid F-Stop table",
                "The currently configured F-Stop table is invalid. The values are out of range "
                "or in the wrong order.\n\n"
                "Make sure that both F-Stop values and positions are in descending order and the "
                "positions are in the range 0 to 100. If descending order of the positions is not "
                "possible due to the lens mechanics, use the invert motor function.");
        return table;
    }

    for (int i = 0; i < tempFstop.length(); i++) {
        table.append((int)(tempFstop[i] * RANGE_FACTOR_F));
        table.append(tempFstopPos[i]);
    }

    return table;
}

/*
 * LensDriverBox::LensFilterPosition
 *****************************************************************************/
int LensDriverBox::LensFilterPosition() const
{
    return (d_data->m_ui->sbxFilterPosition->value());
}

/*
 * LensDriverBox::LensFilterSettings
 *****************************************************************************/
QList<int> LensDriverBox::LensFilterSettings() const
{
    QList<int> settings;
    settings.append(d_data->m_ui->sbxFilterSpeed->value());
    settings.append(d_data->m_ui->sbxFilterStepMode->value());
    settings.append(d_data->m_ui->sbxFilterTorque->value());

    return (settings);
}

/*
 * LensDriverBox::setLensProfile
 *****************************************************************************/
void LensDriverBox::setLensSettings(const QList<int> &settings)
{
    // Store settings in class member
    d_data->m_LensSettings.version = settings[0];
    d_data->m_LensSettings.chipID = settings[1];
    d_data->m_LensSettings.controllerFeatures = settings[2];
    d_data->m_LensSettings.focusMotorNr = settings[3];
    d_data->m_LensSettings.zoomMotorNr = settings[4];
    d_data->m_LensSettings.irisMotorNr = settings[5]; // NOLINT(readability-magic-numbers)
    d_data->m_LensSettings.filterMotorNr = settings[6]; // NOLINT(readability-magic-numbers)
    d_data->m_LensSettings.focusMotorFeatures = settings[7]; // NOLINT(readability-magic-numbers)
    d_data->m_LensSettings.zoomMotorFeatures = settings[8]; // NOLINT(readability-magic-numbers)
    d_data->m_LensSettings.irisMotorFeatures = settings[9]; // NOLINT(readability-magic-numbers)
    d_data->m_LensSettings.filterMotorFeatures = settings[10]; // NOLINT(readability-magic-numbers)

    // Send settings to device
    emit LensSettingsChanged(settings.toVector());
}

/*
 * LensDriverBox::setLensMode
 *****************************************************************************/
void LensDriverBox::setLensMode(int mode)
{
    emit LensModeChanged(mode);
}

/*
 * LensDriverBox::setLensInvert
 *****************************************************************************/
void LensDriverBox::setLensInvert(const QString &mode)
{
    QStringList values;
    static const QRegularExpression regex("\\s+");
    values = mode.split(regex);

    auto focusInvert = values.first().toInt();
    values.removeFirst();
    auto zoomInvert = values.first().toInt();
    values.removeFirst();
    auto irisInvert = values.first().toInt();
    values.removeFirst();
    auto filterInvert = values.first().toInt();

    QVector<int> invert;

    invert.append(filterInvert);
    invert.append(irisInvert);
    invert.append(zoomInvert);
    invert.append(focusInvert);

    d_data->m_ui->cbxFocusInvertEnable->blockSignals(true);
    d_data->m_ui->cbxFocusInvertEnable->setChecked(bool(focusInvert));
    d_data->m_ui->cbxFocusInvertEnable->blockSignals(false);

    d_data->m_ui->cbxZoomInvertEnable->blockSignals(true);
    d_data->m_ui->cbxZoomInvertEnable->setChecked(bool(zoomInvert));
    d_data->m_ui->cbxZoomInvertEnable->blockSignals(false);

    d_data->m_ui->cbxIrisInvertEnable->blockSignals(true);
    d_data->m_ui->cbxIrisInvertEnable->setChecked(bool(irisInvert));
    d_data->m_ui->cbxIrisInvertEnable->blockSignals(false);

    d_data->m_ui->cbxFilterInvertEnable->blockSignals(true);
    d_data->m_ui->cbxFilterInvertEnable->setChecked(bool(filterInvert));
    d_data->m_ui->cbxFilterInvertEnable->blockSignals(false);

    emit LensInvertChanged(invert);
}

/*
 * LensDriverBox::setLensAutoTorque
 *****************************************************************************/
void LensDriverBox::setLensAutoTorque(bool enable)
{
    d_data->m_ui->cbxAutoTorqueCalib->blockSignals(true);
    d_data->m_ui->cbxAutoTorqueCalib->setChecked(enable);
    d_data->m_ui->cbxAutoTorqueCalib->blockSignals(false);

    emit LensAutoTorqueChanged(enable);
}

/**
 * @brief LensDriverBox::getLensFocusPosition
 * @return current focus position
 */
int LensDriverBox::getLensFocusPosition()
{
    return d_data->m_ui->sldFocusPosition->value();
}

/*
 * LensDriverBox::setLensFocusPosition
 *****************************************************************************/
void LensDriverBox::setLensFocusPosition(int focus)
{
    d_data->m_ui->sbxFocusPosition->blockSignals(true);
    d_data->m_ui->sbxFocusPosition->setValue(focus);
    d_data->m_ui->sbxFocusPosition->blockSignals(false);

    emit LensFocusPositionChanged(focus);
}

/**
 * @brief LensDriverBox::getLensZoomPosition
 * @return current zoom position
 */
int LensDriverBox::getLensZoomPosition()
{
    return d_data->m_ui->sldZoomPosition->value();
}

/*
 * LensDriverBox::setLensZoomPosition
 *****************************************************************************/
void LensDriverBox::setLensZoomPosition(int zoom)
{
    d_data->m_ui->sbxZoomPosition->blockSignals(true);
    d_data->m_ui->sbxZoomPosition->setValue(zoom);
    d_data->m_ui->sbxZoomPosition->blockSignals(false);

    emit LensZoomPositionChanged(zoom);
}

/*
 * LensDriverBox::setLensIrisPosition
 *****************************************************************************/
void LensDriverBox::setLensIrisPosition(int iris)
{
    d_data->m_ui->sbxIrisPosition->blockSignals(true);
    d_data->m_ui->sbxIrisPosition->setValue(iris);
    d_data->m_ui->sbxIrisPosition->blockSignals(false);

    d_data->m_ui->sldIrisPosition->blockSignals(true);
    d_data->m_ui->sldIrisPosition->setValue(iris);
    d_data->m_ui->sldIrisPosition->blockSignals(false);

    emit LensIrisPositionChanged(iris);
}

/*
 * LensDriverBox::setLensFilterPosition
 *****************************************************************************/
void LensDriverBox::setLensFilterPosition(int filter)
{
    d_data->m_ui->sbxFilterPosition->blockSignals(true);
    d_data->m_ui->sbxFilterPosition->setValue(filter);
    d_data->m_ui->sbxFilterPosition->blockSignals(false);

    emit LensFilterPositionChanged(filter);
}

/*
 * LensDriverBox::setLensFocusSettings
 *****************************************************************************/
void LensDriverBox::setLensFocusSettings(const QList<int> &settings)
{
    d_data->m_ui->sbxFocusSpeed->blockSignals(true);
    d_data->m_ui->sbxFocusSpeed->setValue(settings[0]);
    d_data->m_ui->sbxFocusSpeed->blockSignals(false);

    d_data->m_ui->sbxFocusStepMode->blockSignals(true);
    d_data->m_ui->sbxFocusStepMode->setValue(settings[1]);
    d_data->m_ui->sbxFocusStepMode->blockSignals(false);

    d_data->m_ui->sbxFocusTorque->blockSignals(true);
    d_data->m_ui->sbxFocusTorque->setValue(settings[2]);
    d_data->m_ui->sbxFocusTorque->blockSignals(false);

    emit LensFocusSettingsChanged(settings.toVector());

    d_data->m_ui->cbxFocusFineEnable->setChecked(bool(settings[3]));

    emit LensFocusFineChanged(bool(settings[3]));
}

/*
 * LensDriverBox::setLensZoomSettings
 *****************************************************************************/
void LensDriverBox::setLensZoomSettings(const QList<int> &settings)
{
    d_data->m_ui->sbxZoomSpeed->blockSignals(true);
    d_data->m_ui->sbxZoomSpeed->setValue(settings[0]);
    d_data->m_ui->sbxZoomSpeed->blockSignals(false);

    d_data->m_ui->sbxZoomStepMode->blockSignals(true);
    d_data->m_ui->sbxZoomStepMode->setValue(settings[1]);
    d_data->m_ui->sbxZoomStepMode->blockSignals(false);

    d_data->m_ui->sbxZoomTorque->blockSignals(true);
    d_data->m_ui->sbxZoomTorque->setValue(settings[2]);
    d_data->m_ui->sbxZoomTorque->blockSignals(false);

    emit LensZoomSettingsChanged(settings.toVector());

    d_data->m_ui->cbxZoomFineEnable->setChecked(bool(settings[3]));

    emit LensZoomFineChanged(bool(settings[3]));
}

/*
 * LensDriverBox::setLensIrisSettings
 *****************************************************************************/
void LensDriverBox::setLensIrisSettings(const QList<int> &settings)
{
    d_data->m_ui->sbxIrisSpeed->blockSignals(true);
    d_data->m_ui->sbxIrisSpeed->setValue(settings[0]);
    d_data->m_ui->sbxIrisSpeed->blockSignals(false);

    d_data->m_ui->sbxIrisStepMode->blockSignals(true);
    d_data->m_ui->sbxIrisStepMode->setValue(settings[1]);
    d_data->m_ui->sbxIrisStepMode->blockSignals(false);

    d_data->m_ui->sbxIrisTorque->blockSignals(true);
    d_data->m_ui->sbxIrisTorque->setValue(settings[2]);
    d_data->m_ui->sbxIrisTorque->blockSignals(false);

    emit LensIrisSettingsChanged(settings.toVector());
}

/*
 * LensDriverBox::setLensIrisTable
 *****************************************************************************/
void LensDriverBox::setLensIrisTable(const QList<int> &table)
{
    QVector<double> fStops;
    QVector<int> fStopPos;

    for (int i = 0; i < table.length(); i = i + 2) {
        fStops.append((double)table[i] / RANGE_FACTOR_F);
        fStopPos.append(table[i + 1]);
    }

    // Check table
    if (!checkFstopTable(fStops, fStopPos)) {
        QMessageBox::warning(
                this, "Loaded invalid F-Stop table from settings file",
                "The F-Stop table in the settings file is invalid. The values are out of range "
                "or in the wrong order.\n\n"
                "Make sure that both F-Stop values and positions are in descending order and the "
                "positions are in the range 0 to 100. If descending order of the positions is not "
                "possible due to the lens mechanics, use the invert motor function.\n\n"
                "The table will be reset to the table that is currently stored in the device.");
    } else {
        d_data->fillTable(fStops, fStopPos);
        emit LensIrisTableChanged(table.toVector());
    }
}

/*
 * LensDriverBox::setLensFilterSettings
 *****************************************************************************/
void LensDriverBox::setLensFilterSettings(const QList<int> &settings)
{
    d_data->m_ui->sbxFilterSpeed->blockSignals(true);
    d_data->m_ui->sbxFilterSpeed->setValue(settings[0]);
    d_data->m_ui->sbxFilterSpeed->blockSignals(false);

    d_data->m_ui->sbxFilterStepMode->blockSignals(true);
    d_data->m_ui->sbxFilterStepMode->setValue(settings[1]);
    d_data->m_ui->sbxFilterStepMode->blockSignals(false);

    d_data->m_ui->sbxFilterTorque->blockSignals(true);
    d_data->m_ui->sbxFilterTorque->setValue(settings[2]);
    d_data->m_ui->sbxFilterTorque->blockSignals(false);

    emit LensFilterSettingsChanged(settings.toVector());
}

/**
 * @brief LensDriverBox::getLensFineFocus
 * @return true, if fine focus is enabled
 */
bool LensDriverBox::getLensFineFocus()
{
    return d_data->m_ui->cbxFocusFineEnable->isChecked();
}

/**
 * @brief LensDriverBox::getLensFineZoom
 * @return true, if fine zoom is enabled
 */
bool LensDriverBox::getLensFineZoom()
{
    return d_data->m_ui->cbxZoomFineEnable->isChecked();
}

/**
 * @brief LensDriverBox::getLensFineIris
 * @return true, if fine iris is enabled
 */
bool LensDriverBox::getLensFineIris()
{
    return d_data->m_ui->cbxIrisFineEnable->isChecked();
}

/*
 * LensDriverBox::setFeaturesVisible
 *****************************************************************************/
void LensDriverBox::setFeaturesVisible(const bool fullFeatureSet, const bool hasZoomFine,
                                       const bool hasIrisFine, const QString &lensB4Info)
{
    d_data->m_FullFeatureSet = fullFeatureSet;

    d_data->m_ui->lblFrontMotor->setVisible(fullFeatureSet);
    d_data->m_ui->cbxFrontMotorFunction->setVisible(fullFeatureSet);
    d_data->m_ui->lblRearMotor->setVisible(fullFeatureSet);
    d_data->m_ui->cbxRearMotorFunction->setVisible(fullFeatureSet);
    d_data->m_ui->btnActivateLens->setVisible(fullFeatureSet);
    d_data->m_ui->lineLensSelect->setVisible(fullFeatureSet);
    d_data->m_ui->cbxAutoTorqueCalib->setVisible(fullFeatureSet);

    // For limitied feature set, limit filter range from 0 to 1
    d_data->m_ui->sldFilterPosition->setRange(0, fullFeatureSet ? DEFAULT_RANGE : 1);
    d_data->m_ui->sbxFilterPosition->setRange(0, fullFeatureSet ? DEFAULT_RANGE : 1);

    // Only show fine zoom checkbox if supported. If not supported, uncheck checkbox.
    d_data->m_ui->cbxZoomFineEnable->setVisible(hasZoomFine);
    if (!hasZoomFine) {
        d_data->m_ui->cbxZoomFineEnable->setChecked(false);
        // only show 'Start Zoom Focus Calibration' when lens_mode == Normal Mode
        d_data->m_ui->btnStartCalibration->setVisible(d_data->m_ui->cbxLensMode->currentIndex()
                                                      == 0);
    }
    // Only show fine iris checkbox if supported
    d_data->m_ui->cbxIrisFineEnable->setVisible(hasIrisFine);

    // Show or hide UI elements based on motor features
    m_lensB4Info = lensB4Info;
    showLensFeatureGroupBoxes();
}

/*
 * LensDriverBox::showLensFeatureGroupBoxes
 *****************************************************************************/
void LensDriverBox::showLensFeatureGroupBoxes()
{
    bool focusAvailable = false;
    bool zoomAvailable = false;
    bool irisAvailable = false;
    bool filterAvailable = false;

    focusAvailable = d_data->m_LensSettings.focusMotorNr > 0
            ? ((d_data->m_LensSettings.controllerFeatures
                & (1 << (d_data->m_LensSettings.focusMotorNr - 1)))
               > 0)
            : false;
    zoomAvailable = d_data->m_LensSettings.zoomMotorNr > 0
            ? ((d_data->m_LensSettings.controllerFeatures
                & (1 << (d_data->m_LensSettings.zoomMotorNr - 1)))
               > 0)
            : false;
    irisAvailable = d_data->m_LensSettings.irisMotorNr > 0
            ? ((d_data->m_LensSettings.controllerFeatures
                & (1 << (d_data->m_LensSettings.irisMotorNr - 1)))
               > 0)
            : false;
    filterAvailable = d_data->m_LensSettings.filterMotorNr > 0
            ? ((d_data->m_LensSettings.controllerFeatures
                & (1 << (d_data->m_LensSettings.filterMotorNr - 1)))
               > 0)
            : false;

    d_data->m_ui->gbxFocusControl->setVisible(focusAvailable);
    d_data->m_ui->gbxZoomControl->setVisible(zoomAvailable);
    d_data->m_ui->gbxIrisControl->setVisible(irisAvailable);
    d_data->m_ui->gbxFilterControl->setVisible(filterAvailable);

    enableLensMotorSettings(LensFeatuesFocus, d_data->m_LensSettings.focusMotorFeatures);
    enableLensMotorSettings(LensFeatuesZoom, d_data->m_LensSettings.zoomMotorFeatures);
    enableLensMotorSettings(LensFeatuesIris, d_data->m_LensSettings.irisMotorFeatures);
    enableLensMotorSettings(LensFeatuesFilter, d_data->m_LensSettings.filterMotorFeatures);

    // Elements that are only visible if the device supports the full feature set
    if (d_data->m_FullFeatureSet) {
        d_data->m_ui->cbxFocusInvertEnable->setVisible(true);
        d_data->m_ui->cbxZoomInvertEnable->setVisible(true);
        d_data->m_ui->cbxIrisInvertEnable->setVisible(true);
        d_data->m_ui->cbxFilterInvertEnable->setVisible(true);

        d_data->m_ui->cbxIrisTableTemplate->setVisible(true);
        d_data->m_ui->lblIrisTableUseTemplate->setVisible(true);
        d_data->m_ui->btnIrisTransmitTable->setVisible(true);
        d_data->m_ui->btnDeleteColumn->setVisible(true);

        // TODO: remove, when zoom by direction and speed implemented for motor-drive too
        d_data->m_ui->lblZoomDirection->setVisible(false);
        d_data->m_ui->sldZoomDirection->setVisible(false);
    } else {
        d_data->m_ui->cbxFocusInvertEnable->setVisible(false);
        d_data->m_ui->cbxZoomInvertEnable->setVisible(false);
        d_data->m_ui->cbxIrisInvertEnable->setVisible(false);
        d_data->m_ui->cbxFilterInvertEnable->setVisible(false);

        d_data->m_ui->cbxIrisTableTemplate->setVisible(false);
        d_data->m_ui->lblIrisTableUseTemplate->setVisible(false);
        d_data->m_ui->btnIrisTransmitTable->setVisible(false);
        d_data->m_ui->btnDeleteColumn->setVisible(false);

        d_data->m_ui->gbxFilterControl->setVisible(false);
        d_data->m_ui->tblIrisAptPosition->setEditTriggers(QAbstractItemView::NoEditTriggers);
    }

    if (m_lensB4Info.isEmpty()) {
        d_data->m_ui->lblLensMode->setVisible(zoomAvailable);
        d_data->m_ui->cbxLensMode->setVisible(zoomAvailable);
        d_data->m_ui->lblLensB4Info->setText("");
    } else {
        d_data->m_ui->lblLensMode->setVisible(false);
        d_data->m_ui->cbxLensMode->setVisible(false);
        d_data->m_ui->btnStartCalibration->setVisible(false);
        d_data->m_ui->cbxIrisInvertEnable->setChecked(false);
        d_data->m_ui->lblLensB4Info->setText(m_lensB4Info);
    }
}

/*
 * LensDriverBox::enableLensMotorSettings
 *****************************************************************************/
void LensDriverBox::enableLensMotorSettings(enum LensFeatues features, int motorSettings)
{
    bool speedEnable = false;
    bool stepModeEnable = false;
    bool torqueEnable = false;

    speedEnable = ((motorSettings & (1 << 1)) > 0);
    stepModeEnable = ((motorSettings & (1 << 2)) > 0);
    torqueEnable = ((motorSettings & (1 << 3)) > 0);

    switch (features) {
    case LensFeatuesFocus: {
        d_data->m_ui->sbxFocusSpeed->setVisible(speedEnable);
        d_data->m_ui->sbxFocusStepMode->setVisible(stepModeEnable);
        d_data->m_ui->sbxFocusTorque->setVisible(torqueEnable);

        d_data->m_ui->lblFocusSpeed->setVisible(speedEnable);
        d_data->m_ui->lblFocusStepMode->setVisible(stepModeEnable);
        d_data->m_ui->lblFocusTorque->setVisible(torqueEnable);

        break;
    }
    case LensFeatuesZoom: {
        d_data->m_ui->sbxZoomSpeed->setVisible(speedEnable);
        d_data->m_ui->sbxZoomStepMode->setVisible(stepModeEnable);
        d_data->m_ui->sbxZoomTorque->setVisible(torqueEnable);

        d_data->m_ui->lblZoomSpeed->setVisible(speedEnable);
        d_data->m_ui->lblZoomStepMode->setVisible(stepModeEnable);
        d_data->m_ui->lblZoomTorque->setVisible(torqueEnable);
        break;
    }
    case LensFeatuesIris: {
        d_data->m_ui->sbxIrisSpeed->setVisible(speedEnable);
        d_data->m_ui->sbxIrisStepMode->setVisible(stepModeEnable);
        d_data->m_ui->sbxIrisTorque->setVisible(torqueEnable);

        d_data->m_ui->lblIrisSpeed->setVisible(speedEnable);
        d_data->m_ui->lblIrisStepMode->setVisible(stepModeEnable);
        d_data->m_ui->lblIrisTorque->setVisible(torqueEnable);
        break;
    }
    case LensFeatuesFilter: {
        d_data->m_ui->sbxFilterSpeed->setVisible(speedEnable);
        d_data->m_ui->sbxFilterStepMode->setVisible(stepModeEnable);
        d_data->m_ui->sbxFilterTorque->setVisible(torqueEnable);

        d_data->m_ui->lblFilterSpeed->setVisible(speedEnable);
        d_data->m_ui->lblFilterStepMode->setVisible(stepModeEnable);
        d_data->m_ui->lblFilterTorque->setVisible(torqueEnable);
        break;
    }
    case LensFeatuesMax:
        // Invalid index, should not happen
        assert(false);
    }
}

/*
 * LensDriverBox::onLensSettingsChange
 *****************************************************************************/
void LensDriverBox::onLensSettingsChange(QVector<int> values)
{
    // Convert values to settings and store them in class member
    lens_settings_t settings;
    settings.version = values[0];
    settings.chipID = values[1];
    settings.controllerFeatures = values[2];
    settings.focusMotorNr = values[3];
    settings.zoomMotorNr = values[4];
    settings.irisMotorNr = values[5]; // NOLINT(readability-magic-numbers)
    settings.filterMotorNr = values[6]; // NOLINT(readability-magic-numbers)
    settings.focusMotorFeatures = values[7]; // NOLINT(readability-magic-numbers)
    settings.zoomMotorFeatures = values[8]; // NOLINT(readability-magic-numbers)
    settings.irisMotorFeatures = values[9]; // NOLINT(readability-magic-numbers)
    settings.filterMotorFeatures = values[10]; // NOLINT(readability-magic-numbers)

    d_data->m_LensSettings = settings;

    /* Setup motor function combo boxes */
    d_data->m_ui->cbxFrontMotorFunction->blockSignals(true);
    d_data->m_ui->cbxRearMotorFunction->blockSignals(true);

    // Defaults
    d_data->m_ui->cbxFrontMotorFunction->setCurrentIndex(0);
    d_data->m_ui->cbxRearMotorFunction->setCurrentIndex(0);
    // Focus
    if (settings.focusMotorNr == FRONT_MOTOR_NR) {
        d_data->m_ui->cbxFrontMotorFunction->setCurrentIndex(1);
    } else if (settings.focusMotorNr == REAR_MOTOR_NR) {
        d_data->m_ui->cbxRearMotorFunction->setCurrentIndex(1);
    }
    // Zoom
    if (settings.zoomMotorNr == FRONT_MOTOR_NR) {
        d_data->m_ui->cbxFrontMotorFunction->setCurrentIndex(2);
    } else if (settings.zoomMotorNr == REAR_MOTOR_NR) {
        d_data->m_ui->cbxRearMotorFunction->setCurrentIndex(2);
    }
    // Iris
    if (settings.irisMotorNr == FRONT_MOTOR_NR) {
        d_data->m_ui->cbxFrontMotorFunction->setCurrentIndex(3);
    } else if (settings.irisMotorNr == REAR_MOTOR_NR) {
        d_data->m_ui->cbxRearMotorFunction->setCurrentIndex(3);
    }
    // Filter
    if (settings.filterMotorNr == FRONT_MOTOR_NR) {
        d_data->m_ui->cbxFrontMotorFunction->setCurrentIndex(4);
    } else if (settings.filterMotorNr == REAR_MOTOR_NR) {
        d_data->m_ui->cbxRearMotorFunction->setCurrentIndex(4);
    }

    d_data->m_ui->cbxFrontMotorFunction->blockSignals(false);
    d_data->m_ui->cbxRearMotorFunction->blockSignals(false);

    // Show or hide UI elements
    showLensFeatureGroupBoxes();

    // Add iris templates with matching chip ID
    d_data->m_ui->cbxIrisTableTemplate->blockSignals(true);
    d_data->m_ui->cbxIrisTableTemplate->clear();
    d_data->m_ui->cbxIrisTableTemplate->blockSignals(false);

    addLensIrisTemplate("Choose a Template", 1);
    for (int i = 0; i < d_data->m_LensIrisTemplates.length(); i++) {
        if (d_data->m_LensIrisTemplates.value(i).compatibleID.contains(settings.chipID)) {
            addLensIrisTemplate(d_data->m_LensIrisTemplates.value(i).lensName, i);
        }
    }
}

/*
 * LensDriverBox::onLensActiveChange
 *****************************************************************************/
void LensDriverBox::onLensActiveChange(int active)
{
    // Hide torque info labels (lens has just been activeted so torque was applied)
    d_data->m_ui->lblFocusTorqueInfo->setVisible(false);
    d_data->m_ui->lblZoomTorqueInfo->setVisible(false);
    d_data->m_ui->lblIrisTorqueInfo->setVisible(false);
    d_data->m_ui->lblFilterTorqueInfo->setVisible(false);

    const bool no_motor_drive =
            ((d_data->m_LensSettings.version == 0) && (d_data->m_LensSettings.chipID == 0));

    if (no_motor_drive && (active != -2)) {
        active = -1;
    }

    if (active < 0) {
        // No iris motor available
        emit irisAvailableChanged(false);

        // A value of -1 indicates that no lens driver was found, in this case all
        // group boxes are invisible and we do not have to enable/disable them
        if (active == -1) {
            if (no_motor_drive) {
                d_data->m_ui->lblLensStatus->setText("No Lens Driver connected");
                d_data->m_ui->lblLensStatus->setStyleSheet("QLabel { color : yellow; }");
                d_data->m_ui->lblVersion->setText("Unkown");
            } else {
                d_data->m_ui->lblLensStatus->setText("Lens Driver connected, but not initialised");
                d_data->m_ui->lblLensStatus->setStyleSheet("QLabel { color : yellow; }");
                // Update version label
                d_data->m_ui->lblVersion->setText(
                        QString("%1.%2.%3")
                                .arg((d_data->m_LensSettings.version & 0x0F00) >> 8)
                                .arg((d_data->m_LensSettings.version & 0x00F0) >> 4)
                                .arg(d_data->m_LensSettings.version & 0x000F));
            }
        }
        // The value of -2 is used internally to indicate that the user changed the
        // motor functions and the lens has to be activated again
        else if (active == -2) {
            d_data->m_ui->lblLensStatus->setText("Config changed, new Activation needed");
            d_data->m_ui->lblLensStatus->setStyleSheet("QLabel { color : red; }");
        }

        d_data->m_ui->lblFocusPosition->setEnabled(false);
        d_data->m_ui->sbxFocusPosition->setEnabled(false);
        d_data->m_ui->sldFocusPosition->setEnabled(false);

        d_data->m_ui->lblZoomPosition->setEnabled(false);
        d_data->m_ui->sbxZoomPosition->setEnabled(false);
        d_data->m_ui->sldZoomPosition->setEnabled(false);

        d_data->m_ui->lblIrisPosition->setEnabled(false);
        d_data->m_ui->sbxIrisPosition->setEnabled(false);
        d_data->m_ui->sldIrisPosition->setEnabled(false);
        d_data->m_ui->lblIrisAperture->setEnabled(false);
        d_data->m_ui->cbxIrisAperture->setEnabled(false);
        d_data->m_ui->btnIrisAptMinus->setEnabled(false);
        d_data->m_ui->btnIrisAptPlus->setEnabled(false);

        d_data->m_ui->lblFilterPosition->setEnabled(false);
        d_data->m_ui->sbxFilterPosition->setEnabled(false);
        d_data->m_ui->sldFilterPosition->setEnabled(false);

        d_data->m_ui->lblFocusStatus->setText("Inactive");
        d_data->m_ui->lblZoomStatus->setText("Inactive");
        d_data->m_ui->lblIrisStatus->setText("Inactive");
        d_data->m_ui->lblFilterStatus->setText("Inactive");

        d_data->m_ui->lblFocusStatus->setStyleSheet("QLabel { color : yellow; }");
        d_data->m_ui->lblZoomStatus->setStyleSheet("QLabel { color : yellow; }");
        d_data->m_ui->lblIrisStatus->setStyleSheet("QLabel { color : yellow; }");
        d_data->m_ui->lblFilterStatus->setStyleSheet("QLabel { color : yellow; }");
    }
    // Else a lens driver was found, so check which motors got successfully activated/initialized
    else {
        // Update version label
        d_data->m_ui->lblVersion->setText(
                QString("%1.%2.%3")
                        .arg((d_data->m_LensSettings.version & 0x0F00) >> 8)
                        .arg((d_data->m_LensSettings.version & 0x00F0) >> 4)
                        .arg(d_data->m_LensSettings.version & 0x000F));

        // Check which motors are active and activate the according group boxes
        auto focusActive = d_data->m_LensSettings.focusMotorNr > 0
                ? ((active & (1 << (d_data->m_LensSettings.focusMotorNr - 1))) > 0)
                : false;
        auto zoomActive = d_data->m_LensSettings.zoomMotorNr > 0
                ? ((active & (1 << (d_data->m_LensSettings.zoomMotorNr - 1))) > 0)
                : false;
        auto irisActive = d_data->m_LensSettings.irisMotorNr > 0
                ? ((active & (1 << (d_data->m_LensSettings.irisMotorNr - 1))) > 0)
                : false;
        auto filterActive = d_data->m_LensSettings.filterMotorNr > 0
                ? ((active & (1 << (d_data->m_LensSettings.filterMotorNr - 1))) > 0)
                : false;

        d_data->m_ui->lblFocusPosition->setEnabled(focusActive);
        d_data->m_ui->sbxFocusPosition->setEnabled(focusActive);
        d_data->m_ui->sldFocusPosition->setEnabled(focusActive);

        d_data->m_ui->lblZoomPosition->setEnabled(zoomActive);
        d_data->m_ui->sbxZoomPosition->setEnabled(zoomActive);
        d_data->m_ui->sldZoomPosition->setEnabled(zoomActive);

        d_data->m_ui->lblIrisPosition->setEnabled(irisActive);
        d_data->m_ui->sbxIrisPosition->setEnabled(irisActive);
        d_data->m_ui->sldIrisPosition->setEnabled(irisActive);
        d_data->m_ui->lblIrisAperture->setEnabled(irisActive);
        d_data->m_ui->cbxIrisAperture->setEnabled(irisActive);
        d_data->m_ui->btnIrisAptMinus->setEnabled(irisActive);
        d_data->m_ui->btnIrisAptPlus->setEnabled(irisActive);

        d_data->m_ui->lblFilterPosition->setEnabled(filterActive);
        d_data->m_ui->sbxFilterPosition->setEnabled(filterActive);
        d_data->m_ui->sldFilterPosition->setEnabled(filterActive);

        // Setup status labels
        if (d_data->m_LensSettings.focusMotorNr
            > 0) // Focus motor should be active, set label text according to active flag
        {
            d_data->m_ui->lblFocusStatus->setText(
                    focusActive ? "Active"
                                : "Initialization failed. Check mechanics or try different torque "
                                  "setting and activate again.");
            d_data->m_ui->lblFocusStatus->setStyleSheet(focusActive ? "QLabel { color : green; }"
                                                                    : "QLabel { color : red; }");
        }
        if (d_data->m_LensSettings.zoomMotorNr > 0) // Zoom motor should be active
        {
            d_data->m_ui->lblZoomStatus->setText(
                    zoomActive ? "Active"
                               : "Initialization failed. Check mechanics or try different torque "
                                 "setting and activate again.");
            d_data->m_ui->lblZoomStatus->setStyleSheet(zoomActive ? "QLabel { color : green; }"
                                                                  : "QLabel { color : red; }");
        }
        if (d_data->m_LensSettings.irisMotorNr > 0) // Iris motor should be active
        {
            d_data->m_ui->lblIrisStatus->setText(
                    irisActive ? "Active"
                               : "Initialization failed. Check mechanics or try different torque "
                                 "setting and activate again.");
            d_data->m_ui->lblIrisStatus->setStyleSheet(irisActive ? "QLabel { color : green; }"
                                                                  : "QLabel { color : red; }");
        }
        if (d_data->m_LensSettings.filterMotorNr > 0) // Filter motor should be active
        {
            d_data->m_ui->lblFilterStatus->setText(
                    filterActive ? "Active"
                                 : "Initialization failed. Check mechanics or try different torque "
                                   "setting and activate again.");
            d_data->m_ui->lblFilterStatus->setStyleSheet(filterActive ? "QLabel { color : green; }"
                                                                      : "QLabel { color : red; }");
        }

        // Check if init of at least one of the selected motor functions failed
        if ((d_data->m_LensSettings.focusMotorNr > 0 && !focusActive)
            || (d_data->m_LensSettings.zoomMotorNr > 0 && !zoomActive)
            || (d_data->m_LensSettings.irisMotorNr > 0 && !irisActive)
            || (d_data->m_LensSettings.filterMotorNr > 0 && !filterActive)) {
            // Lens driver was activated but some motors failed to init
            d_data->m_ui->lblLensStatus->setText("Connected but some Motors failed to Initialize");
            d_data->m_ui->lblLensStatus->setStyleSheet("QLabel { color : yellow; }");
        } else {
            // Lens driver was succesfully activated
            d_data->m_ui->lblLensStatus->setText("Connected and Initialized");
            d_data->m_ui->lblLensStatus->setStyleSheet("QLabel { color : green; }");
        }

        // Emit signal to show if iris motor is now available
        emit irisAvailableChanged(irisActive);
    }

    configureUIforAECSetup();
}

void LensDriverBox::onLensModeChange(int mode)
{
    d_data->m_ui->cbxLensMode->setCurrentIndex(mode);
    d_data->m_ui->btnStartCalibration->setVisible(mode == 0);
}

/*
 * LensDriverBox::onLensInvertChange
 *****************************************************************************/
void LensDriverBox::onLensInvertChange(
        QVector<int> values) // NOLINT(performance-unnecessary-value-param)
{
    d_data->m_ui->cbxFocusInvertEnable->blockSignals(true);
    d_data->m_ui->cbxFocusInvertEnable->setChecked(bool(values.value(0)));
    d_data->m_ui->cbxFocusInvertEnable->blockSignals(false);

    d_data->m_ui->cbxZoomInvertEnable->blockSignals(true);
    d_data->m_ui->cbxZoomInvertEnable->setChecked(bool(values.value(1)));
    d_data->m_ui->cbxZoomInvertEnable->blockSignals(false);

    d_data->m_ui->cbxIrisInvertEnable->blockSignals(true);
    d_data->m_ui->cbxIrisInvertEnable->setChecked(bool(values.value(2)));
    d_data->m_ui->cbxIrisInvertEnable->blockSignals(false);

    d_data->m_ui->cbxFilterInvertEnable->blockSignals(true);
    d_data->m_ui->cbxFilterInvertEnable->setChecked(bool(values.value(3)));
    d_data->m_ui->cbxFilterInvertEnable->blockSignals(false);
}

/*
 * LensDriverBox::onLensAutoTorqueChange
 *****************************************************************************/
void LensDriverBox::onLensAutoTorqueChange(bool enable)
{
    d_data->m_ui->cbxAutoTorqueCalib->blockSignals(true);
    d_data->m_ui->cbxAutoTorqueCalib->setChecked(enable);
    d_data->m_ui->cbxAutoTorqueCalib->blockSignals(false);

    d_data->m_ui->sbxFocusTorque->setEnabled(!enable);
    d_data->m_ui->sbxZoomTorque->setEnabled(!enable);
    d_data->m_ui->sbxIrisTorque->setEnabled(!enable);
    d_data->m_ui->sbxFilterTorque->setEnabled(!enable);
}

/*
 * LensDriverBox::onLensFocusPositionChange
 *****************************************************************************/
void LensDriverBox::onLensFocusPositionChange(int pos)
{
    d_data->m_ui->sbxFocusPosition->blockSignals(true);
    d_data->m_ui->sbxFocusPosition->setValue(pos);
    d_data->m_ui->sbxFocusPosition->blockSignals(false);

    d_data->m_ui->sldFocusPosition->blockSignals(true);
    d_data->m_ui->sldFocusPosition->setValue(pos);
    d_data->m_ui->sldFocusPosition->blockSignals(false);
}

/*
 * LensDriverBox::onLensFocusFineChange
 *****************************************************************************/
void LensDriverBox::onLensFocusFineChange(bool en)
{
    d_data->m_ui->cbxFocusFineEnable->blockSignals(true);
    d_data->m_ui->cbxFocusFineEnable->setChecked(en);
    d_data->m_ui->cbxFocusFineEnable->blockSignals(false);

    if (en) {
        d_data->m_ui->sbxFocusPosition->setRange(0, EXTENDED_RANGE);
        d_data->m_ui->sldFocusPosition->setRange(0, EXTENDED_RANGE);
    } else {
        d_data->m_ui->sbxFocusPosition->setRange(0, DEFAULT_RANGE);
        d_data->m_ui->sldFocusPosition->setRange(0, DEFAULT_RANGE);
    }
}

/*
 * LensDriverBox::onLensZoomPositionChange
 *****************************************************************************/
void LensDriverBox::onLensZoomPositionChange(int pos)
{
    d_data->m_ui->sbxZoomPosition->blockSignals(true);
    d_data->m_ui->sbxZoomPosition->setValue(pos);
    d_data->m_ui->sbxZoomPosition->blockSignals(false);

    d_data->m_ui->sldZoomPosition->blockSignals(true);
    d_data->m_ui->sldZoomPosition->setValue(pos);
    d_data->m_ui->sldZoomPosition->blockSignals(false);
}

/*
 * LensDriverBox::onLensDirectionChange
 *****************************************************************************/
void LensDriverBox::onLensZoomDirectionChange(int dir)
{
    d_data->m_ui->sldZoomDirection->blockSignals(true);
    d_data->m_ui->sldZoomDirection->setValue(dir);
    d_data->m_ui->sldZoomDirection->blockSignals(false);
}

/*
 * LensDriverBox::onLensZoomFineChange
 *****************************************************************************/
void LensDriverBox::onLensZoomFineChange(bool en)
{
    d_data->m_ui->cbxZoomFineEnable->blockSignals(true);
    d_data->m_ui->cbxZoomFineEnable->setChecked(en);
    d_data->m_ui->cbxZoomFineEnable->blockSignals(false);

    if (en) {
        d_data->m_ui->sbxZoomPosition->setRange(0, EXTENDED_RANGE);
        d_data->m_ui->sldZoomPosition->setRange(0, EXTENDED_RANGE);
    } else {
        d_data->m_ui->sbxZoomPosition->setRange(0, DEFAULT_RANGE);
        d_data->m_ui->sldZoomPosition->setRange(0, DEFAULT_RANGE);
    }
}

/*
 * LensDriverBox::onLensIrisPositionChange
 *****************************************************************************/
void LensDriverBox::onLensIrisPositionChange(int pos)
{
    d_data->m_ui->sbxIrisPosition->blockSignals(true);
    d_data->m_ui->sbxIrisPosition->setValue(pos);
    d_data->m_ui->sbxIrisPosition->blockSignals(false);

    d_data->m_ui->sldIrisPosition->blockSignals(true);
    d_data->m_ui->sldIrisPosition->setValue(pos);
    d_data->m_ui->sldIrisPosition->blockSignals(false);
}
/*
 * LensDriverBox::onLensIrisTableChange
 *****************************************************************************/
void LensDriverBox::onLensIrisTableChange(
        QVector<int> values) // NOLINT(performance-unnecessary-value-param)
{
    d_data->m_LensIrisTable.fStops.clear();
    d_data->m_LensIrisTable.fStopsPos.clear();

    for (int i = 0; i < 9; i++) {
        d_data->m_LensIrisTable.fStops.append(values.value(i * 2));
        d_data->m_LensIrisTable.fStopsPos.append(values.value((i * 2) + 1));
    }

    d_data->m_ui->cbxIrisAperture->blockSignals(true);
    d_data->m_ui->cbxIrisAperture->setInsertPolicy(d_data->m_ui->cbxIrisAperture->InsertAtTop);
    d_data->m_ui->cbxIrisAperture->clear();
    d_data->m_ui->cbxIrisAperture->blockSignals(false);

    addLensIrisAperture("Select", 0);

    QVector<double> tempFStops;
    for (int i = 0; i < d_data->m_LensIrisTable.fStops.size(); i++) {
        auto FStop = static_cast<double>(d_data->m_LensIrisTable.fStops.value(i)) / RANGE_FACTOR_F;
        if (FStop != 0.0) {
            addLensIrisAperture(locale().toString(FStop), i + 1);
        }
        tempFStops.append(FStop);
    }

    d_data->fillTable(tempFStops, d_data->m_LensIrisTable.fStopsPos);
}

/*
 * LensDriverBox::onLensIrisAperturePosChange
 *****************************************************************************/
void LensDriverBox::onLensIrisAperturePosChange(int pos)
{
    if (d_data->m_LensIrisTable.fStopsPos.contains(pos)) {
        for (int i = 0; i < d_data->m_LensIrisTable.fStopsPos.size(); i++) {
            if ((pos == d_data->m_LensIrisTable.fStopsPos.value(i))
                && (d_data->m_LensIrisTable.fStops.value(i) != 0)) {
                d_data->m_ui->cbxIrisAperture->blockSignals(true);
                d_data->m_ui->cbxIrisAperture->setCurrentIndex(i + 1);
                d_data->m_ui->cbxIrisAperture->blockSignals(false);
            }
        }
    } else {
        d_data->m_ui->cbxIrisAperture->blockSignals(true);
        d_data->m_ui->cbxIrisAperture->setCurrentIndex(0);
        d_data->m_ui->cbxIrisAperture->blockSignals(false);
    }
}

/*
 * LensDriverBox::onLensIrisFineChange
 *****************************************************************************/
void LensDriverBox::onLensIrisFineChange(bool enable)
{
    d_data->m_ui->cbxIrisFineEnable->blockSignals(true);
    d_data->m_ui->cbxIrisFineEnable->setChecked(enable);
    d_data->m_ui->cbxIrisFineEnable->blockSignals(false);

    if (enable) {
        d_data->m_ui->sbxIrisPosition->setRange(0, EXTENDED_RANGE);
        d_data->m_ui->sldIrisPosition->setRange(0, EXTENDED_RANGE);
    } else {
        d_data->m_ui->sbxIrisPosition->setRange(0, DEFAULT_RANGE);
        d_data->m_ui->sldIrisPosition->setRange(0, DEFAULT_RANGE);
    }
}

/*
 * LensDriverBox::onLensFilterPositionChange
 *****************************************************************************/
void LensDriverBox::onLensFilterPositionChange(int pos)
{
    d_data->m_ui->sbxFilterPosition->blockSignals(true);
    d_data->m_ui->sbxFilterPosition->setValue(pos);
    d_data->m_ui->sbxFilterPosition->blockSignals(false);

    d_data->m_ui->sldFilterPosition->blockSignals(true);
    d_data->m_ui->sldFilterPosition->setValue(pos);
    d_data->m_ui->sldFilterPosition->blockSignals(false);
}

/*
 * LensDriverBox::onLensFocusSettingsChange
 *****************************************************************************/
void LensDriverBox::onLensFocusSettingsChange(QVector<int> values)
{
    d_data->m_ui->sbxFocusSpeed->blockSignals(true);
    d_data->m_ui->sbxFocusSpeed->setValue(values[0]);
    d_data->m_ui->sbxFocusSpeed->blockSignals(false);

    d_data->m_ui->sbxFocusStepMode->blockSignals(true);
    d_data->m_ui->sbxFocusStepMode->setValue(values[1]);
    d_data->m_ui->sbxFocusStepMode->blockSignals(false);

    d_data->m_ui->sbxFocusTorque->blockSignals(true);
    d_data->m_ui->sbxFocusTorque->setValue(values[2]);
    d_data->m_ui->sbxFocusTorque->blockSignals(false);
}

/*
 * LensDriverBox::onLensZoomSettingsChange
 *****************************************************************************/
void LensDriverBox::onLensZoomSettingsChange(QVector<int> values)
{
    d_data->m_ui->sbxZoomSpeed->blockSignals(true);
    d_data->m_ui->sbxZoomSpeed->setValue(values[0]);
    d_data->m_ui->sbxZoomSpeed->blockSignals(false);

    d_data->m_ui->sbxZoomStepMode->blockSignals(true);
    d_data->m_ui->sbxZoomStepMode->setValue(values[1]);
    d_data->m_ui->sbxZoomStepMode->blockSignals(false);

    d_data->m_ui->sbxZoomTorque->blockSignals(true);
    d_data->m_ui->sbxZoomTorque->setValue(values[2]);
    d_data->m_ui->sbxZoomTorque->blockSignals(false);
}

/*
 * LensDriverBox::onLensIrisSettingsChange
 *****************************************************************************/
void LensDriverBox::onLensIrisSettingsChange(QVector<int> values)
{
    d_data->m_ui->sbxIrisSpeed->blockSignals(true);
    d_data->m_ui->sbxIrisSpeed->setValue(values[0]);
    d_data->m_ui->sbxIrisSpeed->blockSignals(false);

    d_data->m_ui->sbxIrisStepMode->blockSignals(true);
    d_data->m_ui->sbxIrisStepMode->setValue(values[1]);
    d_data->m_ui->sbxIrisStepMode->blockSignals(false);

    d_data->m_ui->sbxIrisTorque->blockSignals(true);
    d_data->m_ui->sbxIrisTorque->setValue(values[2]);
    d_data->m_ui->sbxIrisTorque->blockSignals(false);
}

/*
 * LensDriverBox::onLensFilterSettingsChange
 *****************************************************************************/
void LensDriverBox::onLensFilterSettingsChange(QVector<int> values)
{
    d_data->m_ui->sbxFilterSpeed->blockSignals(true);
    d_data->m_ui->sbxFilterSpeed->setValue(values[0]);
    d_data->m_ui->sbxFilterSpeed->blockSignals(false);

    d_data->m_ui->sbxFilterStepMode->blockSignals(true);
    d_data->m_ui->sbxFilterStepMode->setValue(values[1]);
    d_data->m_ui->sbxFilterStepMode->blockSignals(false);

    d_data->m_ui->sbxFilterTorque->blockSignals(true);
    d_data->m_ui->sbxFilterTorque->setValue(values[2]);
    d_data->m_ui->sbxFilterTorque->blockSignals(false);
}

/*
 * LensDriverBox::onAecSettingsChange
 *****************************************************************************/
void LensDriverBox::configureUIforAECSetup()
{
    if (d_data->m_AecSetup.run && (d_data->m_AecSetup.costAperture > 0)) {
        d_data->m_ui->gbxIrisControl->setEnabled(false);
        d_data->m_ui->lblAutoIrisStatus->setText("Iris is controlled by Auto-Exposure");
        d_data->m_ui->lblAutoIrisStatus->setStyleSheet("QLabel { color : yellow; }");
    } else {
        if (!d_data->m_ui->gbxIrisControl->isEnabled()) {
            emit ResyncRequestIris();
        }
        d_data->m_ui->gbxIrisControl->setEnabled(true);
        d_data->m_ui->lblAutoIrisStatus->clear();
    }
}

/*
 * LensDriverBox::onAecSettingsChange
 *****************************************************************************/
void LensDriverBox::onAecSettingsChange(QVector<int> values)
{
    if (!values.empty()) {
        d_data->m_AecSetup.run = static_cast<bool>(values[0]);
        d_data->m_AecSetup.setPoint = values[1];
        d_data->m_AecSetup.speed = values[2];
        d_data->m_AecSetup.ClmTolerance = values[3];
        d_data->m_AecSetup.costGain = values[4];
        d_data->m_AecSetup.costTInt = values[5]; // NOLINT(readability-magic-numbers)
        d_data->m_AecSetup.costAperture = values[6]; // NOLINT(readability-magic-numbers)
        d_data->m_AecSetup.tAf = values[7]; // NOLINT(readability-magic-numbers)
        d_data->m_AecSetup.maxGain = values[8]; // NOLINT(readability-magic-numbers)
        d_data->m_AecSetup.useCustomWeighting =
                static_cast<bool>(values[9]); // NOLINT(readability-magic-numbers)

        configureUIforAECSetup();
    }
}

/*
 * LensDriverBox::onAecEnableChange
 *****************************************************************************/
void LensDriverBox::onAecEnableChange(int enable)
{
    d_data->m_AecSetup.run = (bool)enable;

    configureUIforAECSetup();
}

/*
 * LensDriverBox::onCbxFrontMotorFunctionChange
 *****************************************************************************/
void LensDriverBox::processMotorFunctionChange(int indexFront, int indexRear)
{
    // Clear all motor numbers and feature masks in the lens settings
    d_data->m_LensSettings.focusMotorNr = 0;
    d_data->m_LensSettings.focusMotorFeatures = 0;
    d_data->m_LensSettings.zoomMotorNr = 0;
    d_data->m_LensSettings.zoomMotorFeatures = 0;
    d_data->m_LensSettings.irisMotorNr = 0;
    d_data->m_LensSettings.irisMotorFeatures = 0;
    d_data->m_LensSettings.filterMotorNr = 0;
    d_data->m_LensSettings.filterMotorFeatures = 0;

    // Set numbers and feature mask according to front and rear index
    switch (indexFront) {
    case 1:
        d_data->m_LensSettings.focusMotorNr = FRONT_MOTOR_NR;
        d_data->m_LensSettings.focusMotorFeatures = 0xB;
        break;
    case 2:
        d_data->m_LensSettings.zoomMotorNr = FRONT_MOTOR_NR;
        d_data->m_LensSettings.zoomMotorFeatures = 0xB;
        break;
    case 3:
        d_data->m_LensSettings.irisMotorNr = FRONT_MOTOR_NR;
        d_data->m_LensSettings.irisMotorFeatures = 0xB;
        break;
    case 4:
        d_data->m_LensSettings.filterMotorNr = FRONT_MOTOR_NR;
        d_data->m_LensSettings.filterMotorFeatures = 0xB;
        break;
    default:
        // Nothing to do since we already set all motor nrs to 0 above
        break;
    }

    switch (indexRear) {
    case 1:
        d_data->m_LensSettings.focusMotorNr = REAR_MOTOR_NR;
        d_data->m_LensSettings.focusMotorFeatures = 0xB;
        break;
    case 2:
        d_data->m_LensSettings.zoomMotorNr = REAR_MOTOR_NR;
        d_data->m_LensSettings.zoomMotorFeatures = 0xB;
        break;
    case 3:
        d_data->m_LensSettings.irisMotorNr = REAR_MOTOR_NR;
        d_data->m_LensSettings.irisMotorFeatures = 0xB;
        break;
    case 4:
        d_data->m_LensSettings.filterMotorNr = REAR_MOTOR_NR;
        d_data->m_LensSettings.filterMotorFeatures = 0xB;
        break;
    default:
        // Nothing to do since we already set all motor nrs to 0 above
        break;
    }

    // Build settings vector
    QVector<int> values;
    values.append(d_data->m_LensSettings.version);
    values.append(d_data->m_LensSettings.chipID);
    values.append(d_data->m_LensSettings.controllerFeatures);
    values.append(d_data->m_LensSettings.focusMotorNr);
    values.append(d_data->m_LensSettings.zoomMotorNr);
    values.append(d_data->m_LensSettings.irisMotorNr);
    values.append(d_data->m_LensSettings.filterMotorNr);
    values.append(d_data->m_LensSettings.focusMotorFeatures);
    values.append(d_data->m_LensSettings.zoomMotorFeatures);
    values.append(d_data->m_LensSettings.irisMotorFeatures);
    values.append(d_data->m_LensSettings.filterMotorFeatures);

    // Send new settings to device and update GUI
    setWaitCursor();
    emit LensSettingsChanged(values);
    showLensFeatureGroupBoxes();
    onLensActiveChange(-2);
    setNormalCursor();
}

/*
 * LensDriverBox::onCbxFrontMotorFunctionChange
 *****************************************************************************/
void LensDriverBox::onCbxFrontMotorFunctionChange(int indexFront)
{
    // Also get rear index
    int indexRear = d_data->m_ui->cbxRearMotorFunction->currentIndex();

    // If front index is not 0 and set to rear index, reset rear index to 0
    // (disable it) as it is not allowed to have the same function on both motors.
    if (indexFront != 0 && indexFront == indexRear) {
        indexRear = 0;
        d_data->m_ui->cbxRearMotorFunction->blockSignals(true);
        d_data->m_ui->cbxRearMotorFunction->setCurrentIndex(indexRear);
        d_data->m_ui->cbxRearMotorFunction->blockSignals(false);
    }

    processMotorFunctionChange(indexFront, indexRear);
}

/*
 * LensDriverBox::onCbxRearMotorFunctionChange
 *****************************************************************************/
void LensDriverBox::onCbxRearMotorFunctionChange(int indexRear)
{
    // Also get rear index
    int indexFront = d_data->m_ui->cbxFrontMotorFunction->currentIndex();

    // If rear index is not 0 and set to front index, reset front index to 0
    // (disable it) as it is not allowed to have the same function on both motors.
    if (indexRear != 0 && indexFront == indexRear) {
        indexFront = 0;
        d_data->m_ui->cbxFrontMotorFunction->blockSignals(true);
        d_data->m_ui->cbxFrontMotorFunction->setCurrentIndex(indexFront);
        d_data->m_ui->cbxFrontMotorFunction->blockSignals(false);
    }

    processMotorFunctionChange(indexFront, indexRear);
}

/*
 * LensDriverBox::onBtnLensActiveChange
 *****************************************************************************/
void LensDriverBox::onBtnLensActiveChange()
{
    d_data->m_ui->btnActivateLens->clearFocus();
    setWaitCursor();
    emit LensActiveChanged(1);
    setNormalCursor();
}

/*
 * LensDriverBox::onCbxAutoTorqueChange
 *****************************************************************************/
void LensDriverBox::onCbxAutoTorqueChange(int state)
{
    auto check = static_cast<bool>(state);

    setWaitCursor();
    emit LensAutoTorqueChanged(check);
    setNormalCursor();

    d_data->m_ui->sbxFocusTorque->setEnabled(!check);
    d_data->m_ui->sbxZoomTorque->setEnabled(!check);
    d_data->m_ui->sbxIrisTorque->setEnabled(!check);
    d_data->m_ui->sbxFilterTorque->setEnabled(!check);
}

/*
 * LensDriverBox::onCbxLensModeChanged
 *****************************************************************************/
void LensDriverBox::onCbxLensModeChanged(int mode)
{
    setWaitCursor();
    emit LensModeChanged(mode);
    d_data->m_ui->btnStartCalibration->setVisible(mode == 0);
    setNormalCursor();
}

/*
 * LensDriverBox::onSbxLensFocusPositionChanged
 *****************************************************************************/
void LensDriverBox::onSbxLensFocusPositionChanged(int pos)
{
    setWaitCursor();
    emit LensFocusPositionChanged(pos);
    setNormalCursor();
}

/*
 * LensDriverBox::onSbxLensFocusSpeedChanged
 *****************************************************************************/
void LensDriverBox::onSbxLensFocusSpeedChanged(int speed)
{
    QVector<int> values;

    values.append(speed);
    values.append(d_data->m_ui->sbxFocusStepMode->value());
    values.append(d_data->m_ui->sbxFocusTorque->value());

    setWaitCursor();
    emit LensFocusSettingsChanged(values);
    setNormalCursor();
}

/*
 * LensDriverBox::onSbxLensFocusStepModeChanged
 *****************************************************************************/
void LensDriverBox::onSbxLensFocusStepModeChanged(int stepMode)
{
    QVector<int> values;

    values.append(d_data->m_ui->sbxFocusSpeed->value());
    values.append(stepMode);
    values.append(d_data->m_ui->sbxFocusTorque->value());

    setWaitCursor();
    emit LensFocusSettingsChanged(values);
    setNormalCursor();
}

/*
 * LensDriverBox::onSbxLensFocusTorqueChanged
 *****************************************************************************/
void LensDriverBox::onSbxLensFocusTorqueChanged(int torque)
{
    QVector<int> values;

    values.append(d_data->m_ui->sbxFocusSpeed->value());
    values.append(d_data->m_ui->sbxFocusStepMode->value());
    values.append(torque);

    // Torque was changed, so show info label that new activation is needed
    d_data->m_ui->lblFocusTorqueInfo->setVisible(true);

    setWaitCursor();
    emit LensFocusSettingsChanged(values);
    setNormalCursor();
}

/*
 * LensDriverBox::onCbxLensFocusFineChanged
 *****************************************************************************/
void LensDriverBox::onCbxLensFocusFineChanged(int enable)
{
    setWaitCursor();

    emit LensFocusFineChanged(static_cast<bool>(enable));

    if (static_cast<bool>(enable)) {
        d_data->m_ui->sbxFocusPosition->setRange(0, EXTENDED_RANGE);
        d_data->m_ui->sldFocusPosition->setRange(0, EXTENDED_RANGE);
        d_data->m_ui->sbxFocusPosition->setValue(d_data->m_ui->sbxFocusPosition->value()
                                                 * RANGE_FACTOR);
    } else {
        d_data->m_ui->sbxFocusPosition->setValue(d_data->m_ui->sbxFocusPosition->value()
                                                 / RANGE_FACTOR);
        d_data->m_ui->sbxFocusPosition->setRange(0, DEFAULT_RANGE);
        d_data->m_ui->sldFocusPosition->setRange(0, DEFAULT_RANGE);
    }

    setNormalCursor();
}

/*
 * LensDriverBox::onCbxLensFocusInvertChanged
 *****************************************************************************/
void LensDriverBox::onCbxLensFocusInvertChanged(int enable)
{
    QVector<int> values;

    values.append(static_cast<int>(static_cast<bool>(enable)));
    values.append(
            static_cast<int>(static_cast<bool>(d_data->m_ui->cbxZoomInvertEnable->isChecked())));
    values.append(
            static_cast<int>(static_cast<bool>(d_data->m_ui->cbxIrisInvertEnable->isChecked())));
    values.append(
            static_cast<int>(static_cast<bool>(d_data->m_ui->cbxFilterInvertEnable->isChecked())));

    setWaitCursor();
    emit LensInvertChanged(values);
    setNormalCursor();
}

/*
 * LensDriverBox::onSbxLensZoomPositionChanged
 *****************************************************************************/
void LensDriverBox::onSbxLensZoomPositionChanged(int pos)
{
    setWaitCursor();
    emit LensZoomPositionChanged(pos);
    setNormalCursor();
}

/*
 * LensDriverBox::onSbxLensZoomSpeedChanged
 *****************************************************************************/
void LensDriverBox::onSbxLensZoomSpeedChanged(int speed)
{
    QVector<int> values;

    values.append(speed);
    values.append(d_data->m_ui->sbxZoomStepMode->value());
    values.append(d_data->m_ui->sbxZoomTorque->value());

    setWaitCursor();
    emit LensZoomSettingsChanged(values);
    setNormalCursor();
}

/*
 * LensDriverBox::onSbxLensZoomStepModeChanged
 *****************************************************************************/
void LensDriverBox::onSbxLensZoomStepModeChanged(int stepMode)
{
    QVector<int> values;

    values.append(d_data->m_ui->sbxZoomSpeed->value());
    values.append(stepMode);
    values.append(d_data->m_ui->sbxZoomTorque->value());

    setWaitCursor();
    emit LensZoomSettingsChanged(values);
    setNormalCursor();
}

/*
 * LensDriverBox::onSbxLensZoomTorqueChanged
 *****************************************************************************/
void LensDriverBox::onSbxLensZoomTorqueChanged(int torque)
{
    QVector<int> values;

    values.append(d_data->m_ui->sbxZoomSpeed->value());
    values.append(d_data->m_ui->sbxZoomStepMode->value());
    values.append(torque);

    // Torque was changed, so show info label that new activation is needed
    d_data->m_ui->lblZoomTorqueInfo->setVisible(true);

    setWaitCursor();
    emit LensZoomSettingsChanged(values);
    setNormalCursor();
}

/*
 * LensDriverBox::onCbxLensZoomFineChanged
 *****************************************************************************/
void LensDriverBox::onCbxLensZoomFineChanged(int enable)
{
    setWaitCursor();

    emit LensZoomFineChanged(static_cast<bool>(enable));

    if (static_cast<bool>(enable)) {
        d_data->m_ui->sbxZoomPosition->setRange(0, EXTENDED_RANGE);
        d_data->m_ui->sldZoomPosition->setRange(0, EXTENDED_RANGE);
        d_data->m_ui->sbxZoomPosition->setValue(d_data->m_ui->sbxZoomPosition->value()
                                                * RANGE_FACTOR);
    } else {
        d_data->m_ui->sbxZoomPosition->setValue(d_data->m_ui->sbxZoomPosition->value()
                                                / RANGE_FACTOR);
        d_data->m_ui->sbxZoomPosition->setRange(0, DEFAULT_RANGE);
        d_data->m_ui->sldZoomPosition->setRange(0, DEFAULT_RANGE);
    }

    setNormalCursor();
}

/*
 * LensDriverBox::onCbxLensZoomInvertChanged
 *****************************************************************************/
void LensDriverBox::onCbxLensZoomInvertChanged(int enable)
{
    QVector<int> values;

    values.append(
            static_cast<int>(static_cast<bool>(d_data->m_ui->cbxFocusInvertEnable->isChecked())));
    values.append(static_cast<int>(static_cast<bool>(enable)));
    values.append(
            static_cast<int>(static_cast<bool>(d_data->m_ui->cbxIrisInvertEnable->isChecked())));
    values.append(
            static_cast<int>(static_cast<bool>(d_data->m_ui->cbxFilterInvertEnable->isChecked())));

    setWaitCursor();
    emit LensInvertChanged(values);
    setNormalCursor();
}

/*
 * LensDriverBox::onSbxLensIrisPositionChanged
 *****************************************************************************/
void LensDriverBox::onSbxLensIrisPositionChanged(int pos)
{
    setWaitCursor();
    emit LensIrisPositionChanged(pos);
    emit LensIrisAperturePosChanged(pos);
    setNormalCursor();
}

/*
 * LensDriverBox::onSbxLensIrisSpeedChanged
 *****************************************************************************/
void LensDriverBox::onSbxLensIrisSpeedChanged(int speed)
{
    QVector<int> values;

    values.append(speed);
    values.append(d_data->m_ui->sbxIrisStepMode->value());
    values.append(d_data->m_ui->sbxIrisTorque->value());

    setWaitCursor();
    emit LensIrisSettingsChanged(values);
    setNormalCursor();
}

/*
 * LensDriverBox::onSbxLensIrisStepModeChanged
 *****************************************************************************/
void LensDriverBox::onSbxLensIrisStepModeChanged(int stepMode)
{
    QVector<int> values;

    values.append(d_data->m_ui->sbxIrisSpeed->value());
    values.append(stepMode);
    values.append(d_data->m_ui->sbxIrisTorque->value());

    setWaitCursor();
    emit LensIrisSettingsChanged(values);
    setNormalCursor();
}

/*
 * LensDriverBox::onSbxLensIrisTorqueChanged
 *****************************************************************************/
void LensDriverBox::onSbxLensIrisTorqueChanged(int torque)
{
    QVector<int> values;

    values.append(d_data->m_ui->sbxIrisSpeed->value());
    values.append(d_data->m_ui->sbxIrisStepMode->value());
    values.append(torque);

    // Torque was changed, so show info label that new activation is needed
    d_data->m_ui->lblIrisTorqueInfo->setVisible(true);

    setWaitCursor();
    emit LensIrisSettingsChanged(values);
    setNormalCursor();
}

/*
 * LensDriverBox::onCbxLensIrisInvertChanged
 *****************************************************************************/
void LensDriverBox::onCbxLensIrisInvertChanged(int enable)
{
    QVector<int> values;

    values.append(
            static_cast<int>(static_cast<bool>(d_data->m_ui->cbxFocusInvertEnable->isChecked())));
    values.append(
            static_cast<int>(static_cast<bool>(d_data->m_ui->cbxZoomInvertEnable->isChecked())));
    values.append(static_cast<int>(static_cast<bool>(enable)));
    values.append(
            static_cast<int>(static_cast<bool>(d_data->m_ui->cbxFilterInvertEnable->isChecked())));

    setWaitCursor();
    emit LensInvertChanged(values);
    setNormalCursor();
}

/*
 * LensDriverBox::onCbxLensIrisApertureChanged
 *****************************************************************************/
void LensDriverBox::onCbxLensIrisApertureChanged(int index)
{
    if (index != 0) {
        setWaitCursor();
        emit LensIrisApertureChanged(d_data->m_LensIrisTable.fStops.value((index - 1)));
        emit LensIrisAperturePosChanged(d_data->m_LensIrisTable.fStopsPos.value((index - 1)));
        setNormalCursor();
    }
}

/*
 * LensDriverBox::onBtnLensIrisAperturePlusChanged
 *****************************************************************************/
void LensDriverBox::onBtnLensIrisAperturePlusChanged()
{
    d_data->m_ui->btnIrisAptPlus->clearFocus();

    // Get the current position which might be between two fstop values
    auto acPos = d_data->m_ui->sbxIrisPosition->value();

    // Find the current fstop value or the values above and below the current position, new index
    // is current index + 1
    int index = d_data->m_LensIrisTable.fStopsPos.size() - 1;
    for (int i = 0; i < d_data->m_LensIrisTable.fStopsPos.size() - 1; i++) {
        // Catch corner cases where current position is outside the position
        // range of the table. In those cases fall back to the nearest value
        // in the table.
        if (acPos > d_data->m_LensIrisTable.fStopsPos.value(i)
            || d_data->m_LensIrisTable.fStops.value(i + 1) == 0) {
            index = i;
            break;
        }
        if ((d_data->m_LensIrisTable.fStopsPos[i] == acPos)
            || (d_data->m_LensIrisTable.fStopsPos[i] < acPos
                && d_data->m_LensIrisTable.fStopsPos[i + 1] > acPos)
            || (d_data->m_LensIrisTable.fStopsPos[i + 1] < acPos
                && d_data->m_LensIrisTable.fStopsPos[i] > acPos)) {
            index = i + 1;
            break;
        }
    }

    if (d_data->m_LensIrisTable.fStops.value(index) != 0) {
        setWaitCursor();
        emit LensIrisApertureChanged(d_data->m_LensIrisTable.fStops.value(index));
        emit LensIrisAperturePosChanged(d_data->m_LensIrisTable.fStopsPos.value(index));
        setNormalCursor();
    }
}

/*
 * LensDriverBox::onBtnLensIrisApertureMinusChanged
 *****************************************************************************/
void LensDriverBox::onBtnLensIrisApertureMinusChanged()
{
    d_data->m_ui->btnIrisAptPlus->clearFocus();

    // Get the current position which might be between two fstop values
    auto acPos = d_data->m_ui->sbxIrisPosition->value();

    // Find the current fstop value or the values above and below the current position, new index
    // is current index - 1
    int index = 0;
    for (int i = 1; i < d_data->m_LensIrisTable.fStopsPos.size(); i++) {
        if ((d_data->m_LensIrisTable.fStopsPos[i] == acPos)
            || (d_data->m_LensIrisTable.fStopsPos[i] < acPos
                && d_data->m_LensIrisTable.fStopsPos[i - 1] > acPos)
            || (d_data->m_LensIrisTable.fStopsPos[i - 1] < acPos
                && d_data->m_LensIrisTable.fStopsPos[i] > acPos)) {
            index = i - 1;
            break;
        }
    }

    if (d_data->m_LensIrisTable.fStops.value(index) != 0) {
        setWaitCursor();
        emit LensIrisApertureChanged(d_data->m_LensIrisTable.fStops.value(index));
        emit LensIrisAperturePosChanged(d_data->m_LensIrisTable.fStopsPos.value(index));
        setNormalCursor();
    }
}

/*
 * LensDriverBox::onCbxLensIrisFineChanged
 *****************************************************************************/
void LensDriverBox::onCbxLensIrisFineChanged(int enable)
{
    setWaitCursor();

    emit LensIrisFineChanged(static_cast<bool>(enable));

    if (enable != 0) {
        d_data->m_ui->sbxIrisPosition->setRange(0, EXTENDED_RANGE);
        d_data->m_ui->sldIrisPosition->setRange(0, EXTENDED_RANGE);
        d_data->m_ui->sbxIrisPosition->setValue(d_data->m_ui->sbxIrisPosition->value()
                                                * RANGE_FACTOR);
    } else {
        d_data->m_ui->sbxIrisPosition->setValue(d_data->m_ui->sbxIrisPosition->value()
                                                / RANGE_FACTOR);
        d_data->m_ui->sbxIrisPosition->setRange(0, DEFAULT_RANGE);
        d_data->m_ui->sldIrisPosition->setRange(0, DEFAULT_RANGE);
    }

    setNormalCursor();
}

/*
 * LensDriverBox::onBtnLensIrisTableTransmitChanged
 *****************************************************************************/
void LensDriverBox::onBtnLensIrisTableTransmitChanged()
{
    d_data->m_ui->btnIrisTransmitTable->clearFocus();

    QVector<double> tempFstop;
    QVector<int> tempFstopPos;
    QVector<int> tempIrisSettings;

    d_data->getDataFromModel(tempFstop, tempFstopPos);

    // Sort and check table
    if (!checkFstopTable(tempFstop, tempFstopPos)) {
        QMessageBox::warning(
                this, "Trying to transmit invalid F-Stop table",
                "The currently configured F-Stop table is invalid. The values are out of range "
                "or in the wrong order.\n\n"
                "Make sure that both F-Stop values and positions are in descending order and the "
                "positions are in the range 0 to 100. If descending order of the positions is not "
                "possible due to the lens mechanics, use the invert motor function.");
        return;
    }

    for (int i = 0; i < tempFstop.size(); i++) {
        tempIrisSettings.append((int)qRound(tempFstop.value(i) * RANGE_FACTOR_F));
        tempIrisSettings.append(tempFstopPos.value(i));
    }

    setWaitCursor();
    emit LensIrisTableChanged(tempIrisSettings);
    setNormalCursor();
}

/*
 * LensDriverBox::onBtnLensIrisTableRemoveColumnChanged
 *****************************************************************************/
void LensDriverBox::onBtnLensIrisTableRemoveColumnChanged()
{
    QItemSelectionModel *select = d_data->m_ui->tblIrisAptPosition->selectionModel();

    d_data->m_ui->btnDeleteColumn->clearFocus();

    if (select->hasSelection()) {
        // Get list of selected columns
        const QModelIndexList list = select->selectedColumns();
        QModelIndexList::ConstIterator listIterator = list.constEnd();

        // Remove all columns in the list starting with the last columns
        while (listIterator != list.constBegin()) {
            --listIterator; // Decrement first because constEnd() points to the element behind the
                            // last list entry
            d_data->m_ui->tblIrisAptPosition->model()->removeColumn((*listIterator).column());
            d_data->m_ui->tblIrisAptPosition->model()->insertColumn(
                    d_data->m_ui->tblIrisAptPosition->model()->columnCount());
        }
    }
}

/*
 * LensDriverBox::onCbxLensIrisTemplateChanged
 *****************************************************************************/
void LensDriverBox::onCbxLensIrisTemplateChanged(int index)
{
    int i = 0;
    QVector<double> tempFStops;
    QVector<int> tempFStopPos;

    while (d_data->m_LensIrisTemplates.value(i).lensName.compare(
                   d_data->m_ui->cbxIrisTableTemplate->itemText(index))
                   != 0
           && i < d_data->m_LensIrisTemplates.length()) {
        i++;
    }

    if (i < d_data->m_LensIrisTemplates.length()) {
        // Get template
        const lens_iris_position_template_t &irisTemplate = d_data->m_LensIrisTemplates[i];

        // Load F-Stop table from template
        for (int a = 0; a < irisTemplate.fStops.length(); a++) {
            tempFStops.append(double(irisTemplate.fStops.value(a)) / RANGE_FACTOR);
            tempFStopPos.append(irisTemplate.fStopPos.value(a));
        }
        d_data->fillTable(tempFStops, tempFStopPos);

        // Set iris motor inversion according to template
        d_data->m_ui->cbxIrisInvertEnable->setChecked(irisTemplate.invertIris);
    }
}

/*
 * LensDriverBox::onSbxLensFilterPositionChanged
 *****************************************************************************/
void LensDriverBox::onSbxLensFilterPositionChanged(int pos)
{
    setWaitCursor();
    emit LensFilterPositionChanged(pos);
    setNormalCursor();
}

/*
 * LensDriverBox::onSbxLensFilterSpeedChanged
 *****************************************************************************/
void LensDriverBox::onSbxLensFilterSpeedChanged(int speed)
{
    QVector<int> values;

    values.append(speed);
    values.append(d_data->m_ui->sbxFilterStepMode->value());
    values.append(d_data->m_ui->sbxFilterTorque->value());

    setWaitCursor();
    emit LensFilterSettingsChanged(values);
    setNormalCursor();
}

/*
 * LensDriverBox::onSbxLensFilterStepModeChanged
 *****************************************************************************/
void LensDriverBox::onSbxLensFilterStepModeChanged(int stepMode)
{
    QVector<int> values;

    values.append(d_data->m_ui->sbxFilterSpeed->value());
    values.append(stepMode);
    values.append(d_data->m_ui->sbxFilterTorque->value());

    setWaitCursor();
    emit LensFilterSettingsChanged(values);
    setNormalCursor();
}

/*
 * LensDriverBox::onSbxLensFilterTorqueChanged
 *****************************************************************************/
void LensDriverBox::onSbxLensFilterTorqueChanged(int torque)
{
    QVector<int> values;

    values.append(d_data->m_ui->sbxFilterSpeed->value());
    values.append(d_data->m_ui->sbxFilterStepMode->value());
    values.append(torque);

    // Torque was changed, so show info label that new activation is needed
    d_data->m_ui->lblFilterTorqueInfo->setVisible(true);

    setWaitCursor();
    emit LensFilterSettingsChanged(values);
    setNormalCursor();
}

/*
 * LensDriverBox::onCbxLensFilterInvertChanged
 *****************************************************************************/
void LensDriverBox::onCbxLensFilterInvertChanged(int enable)
{
    QVector<int> values;

    values.append(
            static_cast<int>(static_cast<bool>(d_data->m_ui->cbxFocusInvertEnable->isChecked())));
    values.append(
            static_cast<int>(static_cast<bool>(d_data->m_ui->cbxZoomInvertEnable->isChecked())));
    values.append(
            static_cast<int>(static_cast<bool>(d_data->m_ui->cbxIrisInvertEnable->isChecked())));
    values.append(static_cast<int>(static_cast<bool>(enable)));

    setWaitCursor();
    emit LensInvertChanged(values);
    setNormalCursor();
}
