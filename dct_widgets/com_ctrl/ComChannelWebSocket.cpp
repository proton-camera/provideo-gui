/*
 * Copyright (C) 2021 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    ComChannelWebSocket.cpp
 *
 * @brief
 *
 *****************************************************************************/

#include <QDebug>
#include <QGuiApplication>
#include <QThread>

#include "common.h"
#include "provideo_protocol/provideo_protocol_common.h"
#include "ComChannelWebSocket.h"
#include "qeventloop.h"

#include <array>

// NOLINTBEGIN(cppcoreguidelines-pro-type-reinterpret-cast)

ComChannelWebSocket::ComChannelWebSocket(QString address, uint16_t port, uint8_t dev_addr)
    : m_address(std::move(address)),
      m_port(port),
      m_dev_addr(dev_addr),
      m_timer(new QTimer(this)),
      m_timed_out(false),
      m_emit_data(true)
{
    connect(m_timer, &QTimer::timeout, this, &ComChannelWebSocket::onTimeout);

    // register ctrl channel functions
    auto res = ctrl_channel_register(GetInstance(), this, ctrl_channel_websocket_get_no_ports,
                                     ctrl_channel_websocket_get_port_name,
                                     ctrl_channel_websocket_open, ctrl_channel_websocket_close,
                                     nullptr, nullptr, ctrl_channel_websocket_send_request,
                                     ctrl_channel_websocket_get_response);
    if (res != 0) {
        showError(res, __FILE__, __FUNCTION__, __LINE__); // NOLINT(hicpp-no-array-decay)
    }
}

ComChannelWebSocket::~ComChannelWebSocket()
{
    if (m_webSocket.isValid()) {
        ctrl_channel_websocket_close(this);
    }
}

void ComChannelWebSocket::ReOpen() { }

/*
 * ComChannelWebSocket::emitDataRecieved
 *****************************************************************************/
void ComChannelWebSocket::emitDataReceived(char const *data)
{
    if (receivers(SIGNAL(dataReceived(QString))) > 0) {
        emit dataReceived(QString(data));
    }
}

/*
 * ctrl_channel_websocket_get_no_ports
 *****************************************************************************/
int ComChannelWebSocket::ctrl_channel_websocket_get_no_ports(void *const handle)
{
    (void)handle;

    return 1;
}

/*
 * ctrl_channel_websocket_get_port_name
 *****************************************************************************/
int ComChannelWebSocket::ctrl_channel_websocket_get_port_name(void *const handle, int const idx,
                                                              ctrl_channel_name_t name)
{
    (void)handle;
    (void)idx;

    strncpy(name, "ws", sizeof(ctrl_channel_name_t));

    return 0;
}

/*
 * ctrl_channel_websocket_open
 *****************************************************************************/
int ComChannelWebSocket::ctrl_channel_websocket_open(void *const handle, void *const param,
                                                     int const size)
{
    (void)size;

    auto *self = reinterpret_cast<ComChannelWebSocket *>(handle);

    // type cast open configuration
    auto *conf = static_cast<ctrl_channel_websocket_open_config_t *>(param);
    self->setAddress(conf->ws_addr);
    self->setPort(conf->port);
    self->setDeviceAddress(conf->dev_addr);

    const QString url =
            QString("ws://" + QString(self->getAddress()) + ":" + QString::number(self->getPort()));
    qDebug() << "websocket_open: connecting to " << url << " ....";

    connect(&self->m_webSocket, &QWebSocket::connected, self, &ComChannelWebSocket::onConnected);
    connect(&self->m_webSocket, &QWebSocket::disconnected, self,
            &ComChannelWebSocket::onDisConnected);
    self->m_webSocket.open(QUrl(url));
    qDebug() << "websocket_open: waiting for connection to " << url << " ....";
    self->m_timer->start(10000);
    while (self->m_webSocket.state() != QAbstractSocket::ConnectedState) {
        QGuiApplication::processEvents(QEventLoop::ExcludeUserInputEvents);
        if ((self->m_timed_out) || (!(self->m_timer->isActive()))) {
            break;
        }
    }

    qDebug() << "websocket_open: waiting for connection to " << url << " done";
    return (self->m_webSocket.state() == QAbstractSocket::ConnectedState) ? 0 : -1;
}

/*
 * ctrl_channel_websocket_close
 *****************************************************************************/
int ComChannelWebSocket::ctrl_channel_websocket_close(void *const handle)
{
    auto *self = reinterpret_cast<ComChannelWebSocket *>(handle);

    qDebug() << "websocket_close(): closing websocket ....";
    self->m_webSocket.close();

    return 0;
}

/*
 * ctrl_channel_websocket_send_request
 *****************************************************************************/
int ComChannelWebSocket::ctrl_channel_websocket_send_request(void *const handle,
                                                             uint8_t *const data, int const len)
{
    auto *self = reinterpret_cast<ComChannelWebSocket *>(handle);

    const QString url = QString("ws://" + self->m_address + ":" + QString::number(self->m_port));
    QString msg = "";
    if (self->m_dev_addr != 0) {
        msg += QString::number(self->m_dev_addr) + " ";
    }
    msg += QString(QByteArray(reinterpret_cast<const char *>(data), len));

    // ignore the "flush" command
    if (msg.compare("\n\n\n\n\n") != 0) {
        qDebug() << "websocket_send_request(): " << msg << "to" << url;

        if (self->m_emit_data) {
            self->emitDataReceived(msg.toLocal8Bit().data());
        }

        self->m_webSocket.sendTextMessage(msg);

        QGuiApplication::processEvents(QEventLoop::ExcludeUserInputEvents);
    }

    return self->m_timed_out ? -1 : 0;
}

/*
 * ctrl_channel_websocket_receive_response
 *****************************************************************************/
int ComChannelWebSocket::ctrl_channel_websocket_get_response(void *const handle,
                                                             uint8_t *const data, int const len)
{
    int ret = 0;
    auto *self = reinterpret_cast<ComChannelWebSocket *>(handle);
    QGuiApplication::processEvents(QEventLoop::ExcludeUserInputEvents);
    if (!self->m_lastMessage.isEmpty()) {
        qDebug() << "websocket_get_response(" << len << ":" << self->m_lastMessage.size()
                 << "): " << self->m_lastMessage;
        memcpy(data, self->m_lastMessage.toLocal8Bit(), self->m_lastMessage.size());
        ret = self->m_lastMessage.size();
        self->m_lastMessage.clear();
        self->emitDataReceived(reinterpret_cast<char *>(data));
    }

    return ret;
}

void ComChannelWebSocket::onConnected()
{
    qDebug() << __PRETTY_FUNCTION__ << ": connected to websocket"; // NOLINT(hicpp-no-array-decay)

    connect(&m_webSocket, &QWebSocket::binaryMessageReceived, this,
            &ComChannelWebSocket::onBinaryMessageReceived);
    connect(&m_webSocket, &QWebSocket::textMessageReceived, this,
            &ComChannelWebSocket::onTextMessageReceived);
    m_timed_out = false;
    m_timer->stop();
}

void ComChannelWebSocket::onBinaryMessageReceived(const QByteArray &message)
{
    m_lastMessage = QString(message);
}

void ComChannelWebSocket::onTextMessageReceived(const QString &message)
{
    m_lastMessage = message;
}

void ComChannelWebSocket::onDisConnected()
{
    qDebug() << __PRETTY_FUNCTION__ // NOLINT(hicpp-no-array-decay)
             << ": disconnected from websocket with state: " << m_webSocket.state();
    disconnect(&m_webSocket, &QWebSocket::binaryMessageReceived, this,
               &ComChannelWebSocket::onBinaryMessageReceived);
    disconnect(&m_webSocket, &QWebSocket::textMessageReceived, this,
               &ComChannelWebSocket::onTextMessageReceived);
    m_timed_out = false;
    m_lastMessage.clear();
    m_timer->stop();
}

void ComChannelWebSocket::onTimeout()
{
    qDebug() << __PRETTY_FUNCTION__; // NOLINT(hicpp-no-array-decay)
    m_timed_out = true;
    m_timer->stop();
}

void ComChannelWebSocket::onSendData(QString data, int responseWaitTime)
{
    // we don't respect the response timeout, because we are on a network device,
    // that may have longer response time => we are waiting 10s max
    (void)responseWaitTime;

    if (isOpen() && !data.isEmpty() && (data != "\n")) {
        // send the request without sending it to the debug-console => no doubled data
        m_emit_data = false;
        ctrl_channel_websocket_send_request(this, reinterpret_cast<uint8_t *>(data.toUtf8().data()),
                                            data.size());
        m_emit_data = true;

        // Get reply from device
        static std::array<uint8_t, CMD_SINGLE_LINE_RESPONSE_SIZE> response = { 0 };

        // try to get a response within 10s
        size_t loops = 0;
        int rlen = 0;
        do {
            QGuiApplication::processEvents(QEventLoop::ExcludeUserInputEvents);
            rlen = ctrl_channel_websocket_get_response(this, response.data(), sizeof(response));
            loops += 1;
            QThread::msleep(100);
        } while ((rlen == 0) && (loops <= 100));
    }
    QGuiApplication::processEvents(QEventLoop::ExcludeUserInputEvents);
}

// NOLINTEND(cppcoreguidelines-pro-type-reinterpret-cast)
