/*
 * Copyright (C) 2023 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    ComChannelTCP.cpp
 *
 * @brief   TCP implementation of the communication channel
 *****************************************************************************/

#include "common.h"
#include "provideo_protocol/provideo_protocol_common.h"
#include "ComChannelTCP.h"

// Qt includes
#include <QDebug>
#include <QGuiApplication>
#include <QIcon>
#include <QMessageBox>
#include <QThread>

// std includes
#include <array>
#include <utility>

ComChannelTCP::ComChannelTCP(QString address, uint16_t port, uint8_t dev_addr)
    : m_address(std::move(address)),
      m_port(port),
      m_dev_addr(dev_addr),
      m_tcp_socket(new QTcpSocket(this)),
      m_emit_data(true),
      m_show_disconnect_msgbox(true)
{
    connect(m_tcp_socket, &QTcpSocket::connected, this, [=]() { qDebug() << "TCP connected ..."; });
    connect(m_tcp_socket, &QTcpSocket::disconnected, this, [=]() {
        qDebug() << "TCP disconnected ...";
        if (m_show_disconnect_msgbox) {
            QMessageBox msgBox;
            msgBox.setWindowTitle("TCP Connection Interrupted");
            msgBox.setWindowIcon(QIcon(":/icons/disconnect_64x64.png"));
            msgBox.setText("The TCP connection was interrupted.\n\n"
                           "Please check the connection and try to connect again.");
            msgBox.exec();
        }
        m_show_disconnect_msgbox = true;
    });
    connect(m_tcp_socket, &QTcpSocket::readyRead, this,
            [=]() { qDebug() << "TCP ready to read ..."; });
    connect(m_tcp_socket, &QTcpSocket::bytesWritten, this,
            [=](qint64 bytes) { qDebug() << "TCP bytes written:" << bytes; });

    // register ctrl channel functions
    auto res = ctrl_channel_register(GetInstance(), this, ctrl_channel_tcp_get_no_ports,
                                     ctrl_channel_tcp_get_port_name, ctrl_channel_tcp_open,
                                     ctrl_channel_tcp_close, nullptr, nullptr,
                                     ctrl_channel_tcp_send_request, ctrl_channel_tcp_get_response);
    if (res != 0) {
        showError(res, __FILE__, __FUNCTION__, __LINE__); // NOLINT(hicpp-no-array-decay)
    }
}

ComChannelTCP::~ComChannelTCP()
{
    if (m_tcp_socket->isOpen()) {
        m_show_disconnect_msgbox = false;
        ctrl_channel_tcp_close(this);
    }
}

void ComChannelTCP::ReOpen()
{
    if (m_tcp_socket->isOpen()) {
        m_show_disconnect_msgbox = false;
        ctrl_channel_tcp_close(this);
    }
    if (!m_tcp_socket->isOpen()) {
        ctrl_channel_tcp_open_config_t conf;
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
        conf.addr = reinterpret_cast<char *>(m_address.data());
        conf.port = getPort();
        conf.dev_addr = getDeviceAddress();
        ctrl_channel_tcp_open(this, &conf, sizeof(conf));
    }
}

/*
 * ComChannelTCP::emitDataRecieved
 *****************************************************************************/
void ComChannelTCP::emitDataReceived(char const *data)
{
    if (receivers(SIGNAL(dataReceived(QString))) > 0) {
        emit dataReceived(QString(data));
    }
}

/*
 * ctrl_channel_tcp_get_no_ports
 *****************************************************************************/
int ComChannelTCP::ctrl_channel_tcp_get_no_ports(void *const handle)
{
    (void)handle;

    return 1;
}

/*
 * ctrl_channel_tcp_get_port_name
 *****************************************************************************/
int ComChannelTCP::ctrl_channel_tcp_get_port_name(void *const handle, int const idx,
                                                  ctrl_channel_name_t name)
{
    (void)handle;
    (void)idx;

    strncpy(name, "tcp", sizeof(ctrl_channel_name_t));

    return 0;
}

/*
 * ctrl_channel_tcp_open
 *****************************************************************************/
int ComChannelTCP::ctrl_channel_tcp_open(void *const handle, void *const param, int const size)
{
    (void)size;

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    auto *self = reinterpret_cast<ComChannelTCP *>(handle);
    auto *conf = static_cast<ctrl_channel_tcp_open_config_t *>(param);
    self->setAddress(conf->addr);
    self->setPort(conf->port);
    self->setDeviceAddress(conf->dev_addr);

    qDebug().noquote().nospace() << "tcp_open: connecting to " << self->getAddress() << ":"
                                 << self->getPort() << " dev: " << self->getDeviceAddress();
    self->m_tcp_socket->connectToHost(self->getAddress(), self->getPort());

    if (!(self->m_tcp_socket->waitForConnected(10000))) {
        qWarning() << "tcp_open: could not connect";
        return -1;
    }

    return 0;
}

/*
 * ctrl_channel_tcp_close
 *****************************************************************************/
int ComChannelTCP::ctrl_channel_tcp_close(void *const handle)
{
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    auto *self = reinterpret_cast<ComChannelTCP *>(handle);

    if (self->m_tcp_socket->isOpen()) {
        qDebug() << "tcp_close: ...";
        self->m_show_disconnect_msgbox = false;
        self->m_tcp_socket->close();
    }

    return 0;
}

/*
 * ctrl_channel_tcp_send_request
 *****************************************************************************/
int ComChannelTCP::ctrl_channel_tcp_send_request(void *const handle, uint8_t *const data,
                                                 int const len)
{
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    auto *self = reinterpret_cast<ComChannelTCP *>(handle);
    QByteArray msg = "";
    if (self->m_dev_addr != 0) {
        msg += QByteArray::number(self->m_dev_addr) + " ";
    }
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    msg += QByteArray(reinterpret_cast<const char *>(data), len);

    // ignore the "flush" command
    if (!(msg.contains("\n\n\n\n\n"))) {

        qDebug().noquote().nospace() << "tcp_send_request(" << self->getAddress() << ":"
                                     << self->getPort() << "): " << msg;

        if (self->m_tcp_socket->isOpen()) {

            if (self->m_emit_data) {
                self->emitDataReceived(QString(msg).toLocal8Bit().data());
            }

            self->m_tcp_socket->write(msg);

            if (!(self->m_tcp_socket->waitForBytesWritten(10000))) {
                qWarning() << "tcp_send_request: could not send bytes";
                return -1;
            }
            if (!(self->m_tcp_socket->waitForReadyRead(10000))) {
                qWarning() << "tcp_send_request: didn't received an answer";
                return -1;
            }
        }
    }

    return 0;
}

/*
 * ctrl_channel_tcp_receive_response
 *****************************************************************************/
int ComChannelTCP::ctrl_channel_tcp_get_response(void *const handle, uint8_t *const data,
                                                 int const len)
{
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    auto *self = reinterpret_cast<ComChannelTCP *>(handle);

    if (self->m_tcp_socket->bytesAvailable() > 0) {
        QByteArray bytes = self->m_tcp_socket->readAll();
        int size = bytes.size();
        qDebug().nospace() << "tcp_get_response(" << len << ", " << size << "): " << QString(bytes);
        self->emitDataReceived(bytes);

        memcpy(data, bytes.data(), size);
        return size;
    }
    return 0;
}

void ComChannelTCP::onSendData(QString data, int responseWaitTime)
{
    // we don't respect the response timeout, because we are on a network device,
    // that may have longer response time => we are waiting 10s max
    (void)responseWaitTime;

    if (isOpen() && !data.isEmpty() && (data != "\n")) {
        // send the request without sending it to the debug-console => no doubled data
        m_emit_data = false;
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
        ctrl_channel_tcp_send_request(this, reinterpret_cast<uint8_t *>(data.toUtf8().data()),
                                      data.size());
        m_emit_data = true;

        // Get reply from device
        static std::array<uint8_t, CMD_SINGLE_LINE_RESPONSE_SIZE> response = { 0 };

        // try to get a response within 10s
        size_t loops = 0;
        int rlen = 0;
        do {
            rlen = ctrl_channel_tcp_get_response(this, response.data(), sizeof(response));
            loops += 1;
            QThread::msleep(100);
        } while ((rlen == 0) && (loops <= 100));
    }
}

