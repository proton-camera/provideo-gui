/*
 * Copyright (C) 2017 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    common.h
 *
 * @brief
 *
 *****************************************************************************/
#ifndef COM_CTRL_COMMON_H
#define COM_CTRL_COMMON_H

#include <QDebug>

/*
 * showError
 *****************************************************************************/
inline void showError(int const res, const char *file, const char *func, int const line)
{
    auto text = QString("%2:%4 %3() -> result = %1").arg(res).arg(file, func).arg(line);
    qCritical() << text;
}

#define HANDLE_ERROR(result) /* NOLINT(cppcoreguidelines-macro-usage) */                           \
    {                                                                                              \
        if ((result) != 0) {                                                                       \
            showError(result, static_cast<const char *>(__FILE__),                                 \
                      static_cast<const char *>(__FUNCTION__), __LINE__);                          \
            return;                                                                                \
        }                                                                                          \
    }

#endif // COM_CTRL_COMMON_H
