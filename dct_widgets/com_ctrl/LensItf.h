/******************************************************************************
 * Copyright (C) 2017 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    LensItf.h
 *
 * @brief   Lens Control Interface
 *
 * @note    Multiple Inheritance of an QObject is not allowed.
 *
 *****************************************************************************/
#ifndef LENS_INTERFACE_H
#define LENS_INTERFACE_H

#include <QObject>

#include "ProVideoItf.h"

#include <cstdint>

class LensItf : public ProVideoItf
{
    Q_OBJECT

public:
    explicit LensItf(ComChannel *chan, ComProtocol *proto) : ProVideoItf(chan, proto) { }

    // resync all settings
    void resync() override;

    void GetLensSettings();
    void GetLensActive();
    void GetLensInvert();
    void GetLensAutoTorque();

    void GetLensFocusFine();
    void GetLensZoomFine();
    void GetLensIrisFine();

    void GetLensFocusPosition();
    std::int32_t GetLensFocusMotorPosition();
    void GetLensZoomPosition();
    void GetLensIrisPosition();
    void GetLensIrisAperture();
    void GetLensFilterPosition();

    void GetLensZoomDirection();

    void GetLensFocusSettings();
    void GetLensZoomSettings();
    void GetLensIrisSettings();
    void GetLensIrisTable();
    void GetLensFilterSettings();

    void GetLensMode();
    std::uint16_t GetLensMotorOffset();
    std::uint16_t GetLensFocusOffset();

    void GetMotorRotating();

    bool isFineIrisSupported();
    QString getB4Info();

signals:
    void LensSettingsChanged(QVector<int> values);
    void LensActiveChanged(int active);
    void LensInvertChanged(QVector<int> values);
    void LensAutoTorqueChanged(bool enable);

    void LensFocusFineChanged(bool enable);
    void LensZoomFineChanged(bool enable);
    void LensIrisFineChanged(bool enable);

    void LensFocusPositionChanged(int pos);
    void LensFocusMotorPositionChanged(int pos);
    void LensZoomPositionChanged(int pos);
    void LensIrisPositionChanged(int pos);
    void LensIrisApertureChanged(int apt);
    void LensFilterPositionChanged(int pos);

    void LensZoomDirectionChanged(int dir);

    void LensFocusSettingsChanged(QVector<int> values);
    void LensZoomSettingsChanged(QVector<int> values);
    void LensIrisSettingsChanged(QVector<int> values);
    void LensIrisTableChanged(QVector<int> values);
    void LensFilterSettingsChanged(QVector<int> values);

    void LensModeChanged(int mode);
    void LensMotorOffsetChanged(int offset);
    void LensFocusOffsetChanged(int offset);

    void LensMotorRotatingChanged(bool rotating);

public slots:
    void onLensSettingsChange(QVector<int> values);
    void onLensActiveChange(int active);
    void onLensInvertChange(QVector<int> values);
    void onLensAutoTorqueChange(bool enable);

    void onLensFocusFineChange(bool enable);
    void onLensZoomFineChange(bool enable);
    void onLensIrisFineChange(bool enable);

    void onLensFocusPositionChange(int pos);
    void onLensFocusMotorPositionChange(int pos);
    void onLensZoomPositionChange(int pos);
    void onLensIrisPositionChange(int pos);
    void onLensIrisApertureChange(int apt);
    void onLensFilterPositionChange(int pos);

    void onLensZoomDirectionChange(int dir);

    void onLensFocusSettingsChange(QVector<int> values);
    void onLensZoomSettingsChange(QVector<int> values);
    void onLensIrisSettingsChange(QVector<int> values);
    void onLensIrisTableChange(QVector<int> values);
    void onLensFilterSettingsChange(QVector<int> values);

    void onLensModeChange(int mode);
    void onLensMotorOffsetChange(int offset);
    void onLensFocusOffsetChange(int offset);

    void ResyncRequestIris();
    void ResyncFocus();
    void ResyncFocusMotor();
    void ResyncZoom();
};

#endif // LENS_INTERFACE_H
