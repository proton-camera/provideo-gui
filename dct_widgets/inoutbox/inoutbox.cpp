/*
 * Copyright (C) 2017 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    inoutbox.cpp
 *
 * @brief   Implementation of input/output setting box
 *
 *****************************************************************************/
#include <QtDebug>
#include <QProxyStyle>
#include <QThread>
#include <QTime>
#include <QMessageBox>

#include "inoutbox.h"
#include "ui_inoutbox.h"
#include "defines.h"

#include <aecweightsdialog.h>
#include <lensdriverbox.h>

/*
 * namespaces
 *****************************************************************************/
namespace Ui {
class UI_InOutBox;
}

#define AUTO_REPEAT_THRESHOLD (10000)

/*
 * Overrule unconfigureable timer implementation in qt, there's  no property
 * in spinbox class to do this => use proxy-style (Arrggghhh!!!)
 *
 * Problem: Due to a long processing time of some provideo commands the
 *          internal repeat-timer of spin-box gets expired. This leads to an
 *          repetation of setValue event in the spin-box instance by button
 *          releases and finally the spinbox-value is decreased or increased
 *          twice by clicking up- or down-button once.
 *
 * Hope my qt-patch will be accepted to make it configurable.
 *****************************************************************************/
class SpinBoxStyle : public QProxyStyle
{
public:
    explicit SpinBoxStyle(QStyle *style = nullptr) : QProxyStyle(style) { }

    int styleHint(StyleHint hint, const QStyleOption *option = nullptr,
                  const QWidget *widget = nullptr,
                  QStyleHintReturn *returnData = nullptr) const Q_DECL_OVERRIDE
    {
        if (hint == QStyle::SH_SpinBox_ClickAutoRepeatThreshold) {
            return (AUTO_REPEAT_THRESHOLD);
        }

        return (QProxyStyle::styleHint(hint, option, widget, returnData));
    }
};

/*
 * local definitions
 *****************************************************************************/
#define INOUT_SETTINGS_SECTION_NAME ("INOUT")

#define INOUT_SETTINGS_BAYER_PATTERN ("bayer")
#define INOUT_SETTINGS_CAMERA_ISO ("iso")
#define INOUT_SETTINGS_CAMERA_EXPOSURE ("shutter")

#define INOUT_SETTINGS_AEC_ENABLE ("aec_enable")
#define INOUT_SETTINGS_AEC_USE_CUSTOM_WEIGHTS ("aec_use_custom_weights")
#define INOUT_SETTINGS_AEC_WEIGHTS ("aec_weights")
#define INOUT_SETTINGS_AEC_SETPOINT ("aec_setpoint")
#define INOUT_SETTINGS_AEC_TINT_COST ("aec_tint_cost")
#define INOUT_SETTINGS_AEC_IRIS_COST ("aec_iris_cost")
#define INOUT_SETTINGS_AEC_MAX_ISO ("aec_max_iso")
#define INOUT_SETTINGS_AEC_SPEED ("aec_speed")
#define INOUT_SETTINGS_AEC_FLICKER ("aec_flicker")

#define INOUT_SETTINGS_LSC_ENABLE ("lsc_enable")
#define INOUT_SETTINGS_LSC_K ("lsc_k")
#define INOUT_SETTINGS_LSC_OFFSET ("lsc_offset")
#define INOUT_SETTINGS_LSC_SLOPE ("lsc_slope")

#define INOUT_SETTINGS_FPS_MODE ("fps_mode")
#define INOUT_SETTINGS_PHASES ("phases")
#define INOUT_SETTINGS_VIDEO_MODE ("video_mode")
#define INOUT_SETTINGS_SDI2_MODE ("sdi2_mode")
#define INOUT_SETTINGS_SDI1_DOWNSCALER ("sdi1_downscaler")
#define INOUT_SETTINGS_SDI2_DOWNSCALER ("sdi2_downscaler")
#define INOUT_SETTINGS_FLIP_MODE ("flip_mode")
#define INOUT_SETTINGS_LOG_MODE ("log_mode")
#define INOUT_SETTINGS_COLOR_SPACE ("color_space")
#define INOUT_SETTINGS_PQ_MAX_BRIGHTNESS ("pq_max_brightness")
#define INOUT_SETTINGS_SLOG3_MASTER_GAIN ("slog3_master_gain")
#define INOUT_SETTINGS_TEST_PATTERN ("test_pattern")
#define INOUT_SETTINGS_AUDIO_ENABLE ("audio_enable")
#define INOUT_SETTINGS_AUDIO_GAIN ("audio_gain")

#define INOUT_SETTINGS_GENLOCK_MODE ("genlock_mode")
#define INOUT_SETTINGS_GENLOCK_CROSSLOCK_ENABLE ("genlock_crosslock_enable")
#define INOUT_SETTINGS_GENLOCK_CROSSLOCK_VMODE ("genlock_crosslock_vmode")
#define INOUT_SETTINGS_GENLOCK_OFFSET_VERTICAL ("genlock_offset_vertical")
#define INOUT_SETTINGS_GENLOCK_OFFSET_HORIZONTAL ("genlock_offset_horizontal")
#define INOUT_SETTINGS_GENLOCK_TERMINATION ("genlock_termination")

/*
 * InOutBox::PrivateData
 *****************************************************************************/
class InOutBox::PrivateData
{
public:
    PrivateData()
        : m_ui(new Ui::UI_InOutBox),
          m_sbxStyle(new SpinBoxStyle()),
          m_AecSetup{},
          m_LscSetup{},
          m_weightDialog(new AecWeightsDialog())
    {
        // do nothing
    }

    ~PrivateData()
    {
        delete m_ui;
        delete m_sbxStyle;
        delete m_weightDialog;
    }

    // NOLINTBEGIN(misc-non-private-member-variables-in-classes)
    Ui::UI_InOutBox *m_ui; /**< ui handle */
    int m_cntEvents{ 0 }; /**< ignore move-events if slider moving */
    int m_maxEvents{ 5 }; /**< number of ignored move-events */
    SpinBoxStyle *m_sbxStyle; /**< proxy style to overrule not implemented spinbox properties */

    aec_setup_t m_AecSetup;

    lsc_setup_t m_LscSetup;

    int m_AptMin{ 125 };
    int m_AptMax{ 16000 };
    bool m_AptEnable{ true };

    int m_minIso{ 1 }; /**< iso value of the device at gain 1 (1000) */
    int m_gain{ 1000 }; /**< gain value which is currently configured */
    /* Use maximum int value to disable gain clipping by default */
    int m_gainClip{ INT_MAX }; /**< gain value at which the expsoure is clipped to max */

    AecWeightsDialog *m_weightDialog;
    // NOLINTEND(misc-non-private-member-variables-in-classes)
};

/*
 * InOutBox::InOutBox
 *****************************************************************************/
InOutBox::InOutBox(QWidget *parent) : DctWidgetBox(parent), d_data(new PrivateData)
{
    // initialize UI
    d_data->m_ui->setupUi(this);

    d_data->m_weightDialog->setParent(this, Qt::Dialog);

    d_data->m_ui->sbxIso->setRange(80, 400);
    d_data->m_ui->sldIso->setRange(80, 400);

    d_data->m_ui->lblExposureNote->setVisible(false);
    d_data->m_ui->sbxExposure->setRange(100, 20000);
    d_data->m_ui->sldExposure->setRange(100, 20000);

    d_data->m_ui->sbxSetPoint->setRange(50, 3000);
    d_data->m_ui->sldSetPoint->setRange(50, 3000);

    d_data->m_ui->sbxMaxIso->setRange(80, 400);
    d_data->m_ui->sldMaxIso->setRange(80, 400);

    d_data->m_ui->sbxControlSpeed->setRange(3, 30);
    d_data->m_ui->sldControlSpeed->setRange(3, 30);

    d_data->m_ui->sbxK->setRange(0.0, 2.0);
    d_data->m_ui->sbxK->setSingleStep(0.01);
    d_data->m_ui->sbxK->setKeyboardTracking(false);
    d_data->m_ui->sldK->setRange(0, 200);
    d_data->m_ui->sldK->setPageStep(10);

    d_data->m_ui->sbxOffset->setRange(0.0, 1.0);
    d_data->m_ui->sbxOffset->setSingleStep(0.01);
    d_data->m_ui->sbxOffset->setKeyboardTracking(false);
    d_data->m_ui->sldOffset->setRange(0, 100);
    d_data->m_ui->sldOffset->setPageStep(5);

    d_data->m_ui->sbxSlope->setRange(0.0, 2.0);
    d_data->m_ui->sbxSlope->setSingleStep(0.01);
    d_data->m_ui->sbxSlope->setKeyboardTracking(false);
    d_data->m_ui->sldSlope->setRange(0, 200);
    d_data->m_ui->sldSlope->setPageStep(10);

    d_data->m_ui->tmeTimecode->setTime(QTime(0, 0, 0));

    d_data->m_ui->sbxGenLockOffsetVertical->setRange(-4095, 4095);
    d_data->m_ui->sbxGenLockOffsetVertical->setKeyboardTracking(false);
    d_data->m_ui->sbxGenlockOffsetHorizontal->setRange(-4095, 4095);
    d_data->m_ui->sbxGenlockOffsetHorizontal->setKeyboardTracking(false);

    // fill bayer pattern combo box
    for (int i = BayerPatternFirst; i < BayerPatternMax; i++) {
        addBayerPattern(GetBayerPatternName(static_cast<enum BayerPattern>(i)), i);
    }

    // fill fps-mode combo box
    for (int i = FpsModeFirst; i < FpsModeMax; i++) {
        addFpsMode(GetFpsModeName(static_cast<enum FpsMode>(i)), i);
    }

    // fill sdi2-mode combo box
    for (int i = Sdi2ModeFirst; i < Sdi2ModeMax; i++) {
        addSdi2Mode(GetSdi2ModeName(static_cast<enum Sdi2Mode>(i)), i);
    }

    // fill downscale mode combo boxes
    for (int i = DownscaleModeFirst; i < DownscaleModeMax; i++) {
        addDownscaleMode(GetDownscaleModeName(static_cast<enum DownscaleMode>(i)), i);
    }

    // Note: Flip modes depend on the device and are added in "setFlipModeVisible()"

    // fill log mode combo box
    for (int i = LogModeFirst; i < LogModeMax; i++) {
        addLogMode(GetLogModeName(static_cast<enum LogMode>(i)), i);
    }

    // fill color space combo box
    for (int i = ColorSpaceFirst; i < ColorSpaceMax; i++) {
        addColorSpace(GetColorSpaceName(static_cast<enum ColorSpace>(i)), i);
    }

    // fill genlock-mode combo box
    for (int i = GenLockModeFirst; i < GenLockModeMax; i++) {
        addGenlockMode(GetGenlockModeName(static_cast<enum GenLockMode>(i)), i);
    }

    // fill genlock-crosslock combo boxes
    for (int i = GenLockCrosslockEnableFirst; i < GenLockCrosslockEnableMax; i++) {
        addGenlockCrosslockEnable(
                GetGenlockCrosslockEnableName(static_cast<enum GenLockCrosslockEnable>(i)), i);
    }

    // overrule auto-repeat threshold
    d_data->m_ui->sbxIso->setStyle(d_data->m_sbxStyle);
    d_data->m_ui->sbxExposure->setStyle(d_data->m_sbxStyle);
    d_data->m_ui->sbxGenLockOffsetVertical->setStyle(d_data->m_sbxStyle);
    d_data->m_ui->sbxGenlockOffsetHorizontal->setStyle(d_data->m_sbxStyle);
    d_data->m_ui->sbxSetPoint->setStyle(d_data->m_sbxStyle);
    d_data->m_ui->sbxMaxIso->setStyle(d_data->m_sbxStyle);
    d_data->m_ui->sbxK->setStyle(d_data->m_sbxStyle);
    d_data->m_ui->sbxOffset->setStyle(d_data->m_sbxStyle);
    d_data->m_ui->sbxSlope->setStyle(d_data->m_sbxStyle);
    d_data->m_ui->sbxPhases->setStyle(d_data->m_sbxStyle);

    // connect internal signals
    // bayer pattern
    connect(d_data->m_ui->cbxBayerPattern, SIGNAL(currentIndexChanged(int)), this,
            SLOT(onCbxBayerPatternChange(int)));

    // gain
    connect(d_data->m_ui->sldIso, SIGNAL(valueChanged(int)), this, SLOT(onSldIsoChange(int)));
    connect(d_data->m_ui->sldIso, SIGNAL(sliderReleased()), this, SLOT(onSldIsoReleased()));
    connect(d_data->m_ui->sbxIso, SIGNAL(valueChanged(int)), this, SLOT(onSbxIsoChange(int)));
    connect(d_data->m_ui->cbxIso, SIGNAL(activated(int)), this, SLOT(onCbxIsoChange(int)));

    // exposure
    connect(d_data->m_ui->sldExposure, SIGNAL(valueChanged(int)), this,
            SLOT(onSldExposureChange(int)));
    connect(d_data->m_ui->sldExposure, SIGNAL(sliderReleased()), this,
            SLOT(onSldExposureReleased()));
    connect(d_data->m_ui->sbxExposure, SIGNAL(valueChanged(int)), this,
            SLOT(onSbxExposureChange(int)));
    connect(d_data->m_ui->cbxExposure, SIGNAL(activated(int)), this,
            SLOT(onCbxExposureChange(int)));
    connect(d_data->m_ui->btnExposureMinus, SIGNAL(clicked()), this,
            SLOT(onBtnExposureMinusClicked()));
    connect(d_data->m_ui->btnExposurePlus, SIGNAL(clicked()), this,
            SLOT(onBtnExposurePlusClicked()));

    // video
    connect(d_data->m_ui->cbxFpsMode, SIGNAL(currentIndexChanged(int)), this,
            SLOT(onCbxFpsModeChange(int)));
    connect(d_data->m_ui->sbxPhases, SIGNAL(valueChanged(int)), this, SLOT(onSbxPhasesChange(int)));
    connect(d_data->m_ui->cbxVideoMode, SIGNAL(currentIndexChanged(int)), this,
            SLOT(onCbxVideoModeChange(int)));
    connect(d_data->m_ui->cbxSdi2Mode, SIGNAL(currentIndexChanged(int)), this,
            SLOT(onCbxSdi2ModeChange(int)));
    connect(d_data->m_ui->cbxSdi1Downscaler, SIGNAL(currentIndexChanged(int)), this,
            SLOT(onCbxSdi1DownscalerChange(int)));
    connect(d_data->m_ui->cbxSdi2Downscaler, SIGNAL(currentIndexChanged(int)), this,
            SLOT(onCbxSdi2DownscalerChange(int)));
    connect(d_data->m_ui->cbxFlipMode, SIGNAL(currentIndexChanged(int)), this,
            SLOT(onCbxFlipModeChange(int)));
    connect(d_data->m_ui->cbxLogMode, SIGNAL(currentIndexChanged(int)), this,
            SLOT(onCbxLogModeChange(int)));
    connect(d_data->m_ui->sbxPQMaxBrightness, SIGNAL(valueChanged(int)), this,
            SLOT(onSbxPQMaxBrightnessChange(int)));
    connect(d_data->m_ui->sbxSLog3MasterGain, SIGNAL(valueChanged(int)), this,
            SLOT(onSbxSLog3MasterGainChange(int)));
    connect(d_data->m_ui->cbxColorSpace, SIGNAL(currentIndexChanged(int)), this,
            SLOT(onCbxColorSpaceChange(int)));
    connect(d_data->m_ui->cbxTestPattern, SIGNAL(stateChanged(int)), this,
            SLOT(onCbxTestPatternChange(int)));
    connect(d_data->m_ui->btnIsoMinus, SIGNAL(clicked()), this, SLOT(onBtnIsoMinusClicked()));
    connect(d_data->m_ui->btnIsoPlus, SIGNAL(clicked()), this, SLOT(onBtnIsoPlusClicked()));

    // audio
    connect(d_data->m_ui->cbxAudioEnable, SIGNAL(stateChanged(int)), this,
            SLOT(onCbxAudioEnableChange(int)));
    connect(d_data->m_ui->sbxAudioGain, SIGNAL(valueChanged(double)), this,
            SLOT(onSbxAudioGainChange(double)));

    // genlock
    connect(d_data->m_ui->cbxGenLockMode, SIGNAL(currentIndexChanged(int)), this,
            SLOT(onCbxGenlockModeChange(int)));
    connect(d_data->m_ui->cbxGenlockCrosslockEnable, SIGNAL(currentIndexChanged(int)), this,
            SLOT(onCbxGenlockCrosslockEnableChange(int)));
    connect(d_data->m_ui->cbxGenlockCrosslockVmode, SIGNAL(currentIndexChanged(int)), this,
            SLOT(onCbxGenlockCrosslockVmodeChange(int)));
    connect(d_data->m_ui->sbxGenLockOffsetVertical, SIGNAL(valueChanged(int)), this,
            SLOT(onSbxGenlockOffsetVerticalChange(int)));
    connect(d_data->m_ui->sbxGenlockOffsetHorizontal, SIGNAL(valueChanged(int)), this,
            SLOT(onSbxGenlockOffsetHorizontalChange(int)));
    connect(d_data->m_ui->cbxGenLockTermination, SIGNAL(stateChanged(int)), this,
            SLOT(onCbxGenlockTerminationChange(int)));
    connect(d_data->m_ui->pbGenlockSync, &QPushButton::clicked, this, [=]() {
        setWaitCursor();
        emit ChainGenlockOffsetChanged(GenLockOffsetVertical(), GenLockOffsetHorizontal());
        // waiting for the answer from the camera
        QApplication::processEvents();
        setNormalCursor();
    });

    // auto exposure
    connect(d_data->m_ui->cbxAecEnable, SIGNAL(stateChanged(int)), this,
            SLOT(onCbxAecEnableChange(int)));
    connect(d_data->m_ui->cbxAecWeight, SIGNAL(stateChanged(int)), this,
            SLOT(onCbxAecWeightChange(int)));

    connect(this, SIGNAL(WeightDialogAecWeightsChanged(QVector<int>)), d_data->m_weightDialog,
            SLOT(onAecWeightsChange(QVector<int>)));
    // clang-format off
    connect(d_data->m_weightDialog, SIGNAL(AecWeightChanged(int,int)), this,
            SIGNAL(AecWeightChanged(int,int)));
    // clang-format on

    connect(d_data->m_ui->btnAecWeight, SIGNAL(clicked()), d_data->m_weightDialog, SLOT(show()));

    connect(d_data->m_ui->sldSetPoint, SIGNAL(valueChanged(int)), this,
            SLOT(onSldSetPointChange(int)));
    connect(d_data->m_ui->sldSetPoint, SIGNAL(sliderReleased()), this,
            SLOT(onSldSetPointReleased()));
    connect(d_data->m_ui->sbxSetPoint, SIGNAL(valueChanged(int)), this,
            SLOT(onSbxSetPointChange(int)));

    connect(d_data->m_ui->sldMaxIso, SIGNAL(valueChanged(int)), this, SLOT(onSldMaxIsoChange(int)));
    connect(d_data->m_ui->sldMaxIso, SIGNAL(sliderReleased()), this, SLOT(onSldMaxIsoReleased()));
    connect(d_data->m_ui->sbxMaxIso, SIGNAL(valueChanged(int)), this, SLOT(onSbxMaxIsoChange(int)));

    connect(d_data->m_ui->sldControlSpeed, SIGNAL(valueChanged(int)), this,
            SLOT(onSldControlSpeedChange(int)));
    connect(d_data->m_ui->sldControlSpeed, SIGNAL(sliderReleased()), this,
            SLOT(onSldControlSpeedReleased()));
    connect(d_data->m_ui->sbxControlSpeed, SIGNAL(valueChanged(int)), this,
            SLOT(onSbxControlSpeedChange(int)));

    connect(d_data->m_ui->rdbTaf50Hz, SIGNAL(toggled(bool)), this, SLOT(onTaf50Toggle(bool)));
    connect(d_data->m_ui->rdbTaf60Hz, SIGNAL(toggled(bool)), this, SLOT(onTaf60Toggle(bool)));

    connect(d_data->m_ui->rdbAecAutoIris, SIGNAL(toggled(bool)), this,
            SLOT(onAecAutoIrisToggle(bool)));
    connect(d_data->m_ui->rdbAecManIris, SIGNAL(toggled(bool)), this,
            SLOT(onAecManualIrisToggle(bool)));
    connect(d_data->m_ui->rdbAecManShutter, SIGNAL(toggled(bool)), this,
            SLOT(onAecManualShutterToggle(bool)));

    // timecode
    connect(d_data->m_ui->btnSetTimecode, SIGNAL(clicked()), this, SLOT(onBtnTimecodeSetClicked()));
    connect(d_data->m_ui->btnGetTimecode, SIGNAL(clicked()), this, SLOT(onBtnTimecodeGetClicked()));
    connect(d_data->m_ui->btnHoldTimecode, SIGNAL(clicked(bool)), this,
            SLOT(onBtnTimecodeHoldClicked(bool)));

    // lense shading correction
    connect(d_data->m_ui->cbxLscEnable, SIGNAL(stateChanged(int)), this,
            SLOT(onCbxLscEnableChange(int)));

    connect(d_data->m_ui->sbxK, SIGNAL(valueChanged(double)), this, SLOT(onSbxKChange(double)));
    connect(d_data->m_ui->sldK, SIGNAL(valueChanged(int)), this, SLOT(onSldKChange(int)));
    connect(d_data->m_ui->sldK, SIGNAL(sliderReleased()), this, SLOT(onSldKReleased()));

    connect(d_data->m_ui->sbxOffset, SIGNAL(valueChanged(double)), this,
            SLOT(onSbxOffsetChange(double)));
    connect(d_data->m_ui->sldOffset, SIGNAL(valueChanged(int)), this, SLOT(onSldOffsetChange(int)));
    connect(d_data->m_ui->sldOffset, SIGNAL(sliderReleased()), this, SLOT(onSldOffsetReleased()));

    connect(d_data->m_ui->sbxSlope, SIGNAL(valueChanged(double)), this,
            SLOT(onSbxSlopeChange(double)));
    connect(d_data->m_ui->sldSlope, SIGNAL(valueChanged(int)), this, SLOT(onSldSlopeChange(int)));
    connect(d_data->m_ui->sldSlope, SIGNAL(sliderReleased()), this, SLOT(onSldSlopeReleased()));

    ////////////////////
    // operation mode
    ////////////////////
    prepareMode(mode());
}

/*
 * InOutBox::~InOutBox
 *****************************************************************************/
InOutBox::~InOutBox()
{
    delete d_data;
}

/*
 * gain_to_iso
 *****************************************************************************/
int InOutBox::gainToIso(int gain) const
{
    return (((gain * d_data->m_minIso) + 500) / 1000);
}

/*
 * isoToGain
 *****************************************************************************/
int InOutBox::isoToGain(int iso) const
{
    return ((iso * 1000) / d_data->m_minIso);
}

/*
 * checkGainClip
 *****************************************************************************/
void InOutBox::checkGainClip(int newGain)
{
    /* If new gain is higher than gain clip, and old gain wasn't get new,
     * clipped exposure value and update the camera control UI elements */
    if (d_data->m_gain <= d_data->m_gainClip && newGain > d_data->m_gainClip) {
        emit UpdateCameraExposure();
        /* Note: Update stored gain now, so that it can be used by
         * enableCamConfWidgets(). */
        d_data->m_gain = newGain;
        enableCamConfWidgets(!d_data->m_AecSetup.run);
    }
    /* Else if new gain is lower or equal to gain clip and the old gain
     * was higher, we have to re-enable the exposure controls. We
     * do not have to get the exposure again as it will not change. */
    else if (d_data->m_gain > d_data->m_gainClip && newGain <= d_data->m_gainClip) {
        /* Note: Update stored gain now, so that it can be used by
         * enableCamConfWidgets(). */
        d_data->m_gain = newGain;
        enableCamConfWidgets(!d_data->m_AecSetup.run);
    }
    /* Else just update stored gain. */
    else {
        d_data->m_gain = newGain;
    }
}

/*
 * show4kGenlockNote
 *****************************************************************************/
void InOutBox::show4kGenlockNote(int mode)
{
    /* If this is a 4K / UHD video mode, show the note on genlock and 4k */
    if (mode >= VideoModeFirstUHD && mode <= VideoModeLast4K) {
        d_data->m_ui->lblGenlock4kNote->setVisible(true);
    } else {
        d_data->m_ui->lblGenlock4kNote->setVisible(false);
    }
}

/*
 * isoToGain
 *****************************************************************************/
void InOutBox::EmitDownscaleChanged(int sdi_out_idx, int combo_box_idx)
{
    switch (combo_box_idx) {
    case 0: // Disable downscaler
        emit ChainDownscaleModeChanged(sdi_out_idx, false, false);
        break;
    case 1: // Enable downscaler
        emit ChainDownscaleModeChanged(sdi_out_idx, true, false);
        break;
    case 2: // Enable downscaler and interlacer
        emit ChainDownscaleModeChanged(sdi_out_idx, true, true);
        break;
    default:
        // ignore
        break;
    }
}

/*
 * InOutBox::BayerPattern
 *****************************************************************************/
int InOutBox::BayerPattern() const
{
    return (d_data->m_ui->cbxBayerPattern->currentData().toInt());
}

/*
 * InOutBox::setBayerPattern
 *****************************************************************************/
void InOutBox::setBayerPattern(const int value)
{
    const int index = d_data->m_ui->cbxBayerPattern->findData(value);
    if (index != -1) {
        d_data->m_ui->cbxBayerPattern->blockSignals(true);
        d_data->m_ui->cbxBayerPattern->setCurrentIndex(index);
        d_data->m_ui->cbxBayerPattern->blockSignals(false);

        emit BayerPatternChanged(value);
    }
}

/*
 * InOutBox::CameraIso
 *****************************************************************************/
int InOutBox::CameraIso() const
{
    return (d_data->m_ui->sbxIso->value());
}

/*
 * InOutBox::setCameraGain
 *****************************************************************************/
void InOutBox::setCameraIso(const int value)
{
    d_data->m_ui->sldIso->blockSignals(true);
    d_data->m_ui->sldIso->setValue(value);
    d_data->m_ui->sldIso->blockSignals(false);

    d_data->m_ui->sbxIso->blockSignals(true);
    d_data->m_ui->sbxIso->setValue(value);
    d_data->m_ui->sbxIso->blockSignals(false);

    const int gain = isoToGain(value);

    setWaitCursor();
    emit CameraGainChanged(gain);
    setNormalCursor();

    checkGainClip(gain);
}

/*
 * InOutBox::CameraExposure
 *****************************************************************************/
int InOutBox::CameraExposure() const
{
    return (d_data->m_ui->sbxExposure->value());
}

/*
 * InOutBox::setCameraExposure
 *****************************************************************************/
void InOutBox::setCameraExposure(const int value)
{
    d_data->m_ui->sldExposure->blockSignals(true);
    d_data->m_ui->sldExposure->setValue(value);
    d_data->m_ui->sldExposure->blockSignals(false);

    d_data->m_ui->sbxExposure->blockSignals(true);
    d_data->m_ui->sbxExposure->setValue(value);
    d_data->m_ui->sbxExposure->blockSignals(false);

    setWaitCursor();
    emit CameraExposureChanged(value);
    setNormalCursor();
}

/*
 * InOutBox::AecEnable
 *****************************************************************************/
bool InOutBox::AecEnable() const
{
    return (d_data->m_AecSetup.run);
}

/*
 * InOutBox::AecEnable
 *****************************************************************************/
void InOutBox::setAecEnable(const bool value)
{
    d_data->m_AecSetup.run = value;

    enableAecWidgets(value);
    enableCamConfWidgets(!value);

    d_data->m_ui->cbxAecEnable->blockSignals(true);
    d_data->m_ui->cbxAecEnable->setCheckState(value ? Qt::Checked : Qt::Unchecked);
    d_data->m_ui->cbxAecEnable->blockSignals(false);

    setWaitCursor();
    emit AecEnableChanged(static_cast<int>(d_data->m_AecSetup.run));
    setNormalCursor();

    // aec disabled, update exposure and gain widgets
    if (!value) {
        setWaitCursor();
        emit ResyncRequest();
        setNormalCursor();
    }
}

/*
 * InOutBox::AecManualShutter
 *****************************************************************************/
int InOutBox::AecCostTInt() const
{
    return (d_data->m_AecSetup.costTInt);
}

/*
 * InOutBox::setAecManualShutter
 *****************************************************************************/
void InOutBox::setAecCostTInt(const int value)
{
    d_data->m_AecSetup.costTInt = value;

    d_data->m_ui->rdbAecManShutter->blockSignals(true);
    d_data->m_ui->rdbAecManShutter->setChecked(d_data->m_AecSetup.costTInt > 0);
    d_data->m_ui->rdbAecManShutter->blockSignals(false);

    d_data->m_ui->rdbAecManIris->blockSignals(true);
    d_data->m_ui->rdbAecManIris->setChecked(d_data->m_AecSetup.costTInt == 0);
    d_data->m_ui->rdbAecManIris->blockSignals(false);

    setWaitCursor();
    emit AecSetupChanged(createAecVector());
    setNormalCursor();
}

/*
 * InOutBox::AecManualIris
 *****************************************************************************/
int InOutBox::AecCostIris() const
{
    return (d_data->m_AecSetup.costAperture);
}

/*
 * InOutBox::setAecManualIris
 *****************************************************************************/
void InOutBox::setAecCostIris(const int value)
{
    d_data->m_AecSetup.costAperture = value;

    d_data->m_ui->rdbAecManShutter->blockSignals(true);
    d_data->m_ui->rdbAecManShutter->setChecked(d_data->m_AecSetup.costAperture == 0);
    d_data->m_ui->rdbAecManShutter->blockSignals(false);

    d_data->m_ui->rdbAecManIris->blockSignals(true);
    d_data->m_ui->rdbAecManIris->setChecked(d_data->m_AecSetup.costAperture > 0);
    d_data->m_ui->rdbAecManIris->blockSignals(false);

    setWaitCursor();
    auto aecVector = createAecVector();
    emit AecSetupChanged(aecVector);
    emit AecSetupChangedForLensDriver(aecVector);
    setNormalCursor();
}

/*
 * InOutBox::AecSetPoint
 *****************************************************************************/
int InOutBox::AecSetPoint() const
{
    return (d_data->m_AecSetup.setPoint);
}

/*
 * InOutBox::setAecSetPoint
 *****************************************************************************/
void InOutBox::setAecSetPoint(const int value)
{
    d_data->m_ui->sbxSetPoint->blockSignals(true);
    d_data->m_ui->sbxSetPoint->setValue(value);
    d_data->m_ui->sbxSetPoint->blockSignals(false);

    d_data->m_ui->sldSetPoint->blockSignals(true);
    d_data->m_ui->sldSetPoint->setValue(value);
    d_data->m_ui->sldSetPoint->blockSignals(false);

    d_data->m_AecSetup.setPoint = value;

    setWaitCursor();
    emit AecSetupChanged(createAecVector());
    setNormalCursor();
}

/*
 * InOutBox::AecMaxIso
 *****************************************************************************/
int InOutBox::AecMaxIso() const
{
    return (gainToIso(d_data->m_AecSetup.maxGain));
}

/*
 * InOutBox::setAecMaxIso
 *****************************************************************************/
void InOutBox::setAecMaxIso(const int value)
{
    d_data->m_ui->sbxMaxIso->blockSignals(true);
    d_data->m_ui->sbxMaxIso->setValue(value);
    d_data->m_ui->sbxMaxIso->blockSignals(false);

    d_data->m_ui->sldMaxIso->blockSignals(true);
    d_data->m_ui->sldMaxIso->setValue(value);
    d_data->m_ui->sldMaxIso->blockSignals(false);

    d_data->m_AecSetup.maxGain = isoToGain(value);

    setWaitCursor();
    emit AecSetupChanged(createAecVector());
    setNormalCursor();
}

/*
 * InOutBox::AecControlSpeed
 *****************************************************************************/
int InOutBox::AecControlSpeed() const
{
    return (d_data->m_AecSetup.speed);
}

/*
 * InOutBox::setAecControlSpeed
 *****************************************************************************/
void InOutBox::setAecControlSpeed(const int value)
{
    d_data->m_ui->sbxControlSpeed->blockSignals(true);
    d_data->m_ui->sbxControlSpeed->setValue(value);
    d_data->m_ui->sbxControlSpeed->blockSignals(false);

    d_data->m_ui->sldControlSpeed->blockSignals(true);
    d_data->m_ui->sldControlSpeed->setValue(value);
    d_data->m_ui->sldControlSpeed->blockSignals(false);

    d_data->m_AecSetup.speed = value;

    setWaitCursor();
    emit AecSetupChanged(createAecVector());
    setNormalCursor();
}

/*
 * InOutBox::AecFlickerFrequency
 *****************************************************************************/
int InOutBox::AecFlickerFrequency() const
{
    return (d_data->m_AecSetup.tAf);
}

/*
 * InOutBox::setAecFlickerFrequency
 *****************************************************************************/
void InOutBox::setAecFlickerFrequency(const int value)
{
    if (value == 8333 || value == 10000) {
        d_data->m_AecSetup.tAf = value;
    } else {
        d_data->m_AecSetup.tAf = 10000;
    }

    d_data->m_ui->rdbTaf50Hz->blockSignals(true);
    d_data->m_ui->rdbTaf50Hz->setChecked(d_data->m_AecSetup.tAf == 10000);
    d_data->m_ui->rdbTaf50Hz->blockSignals(false);

    d_data->m_ui->rdbTaf60Hz->blockSignals(true);
    d_data->m_ui->rdbTaf60Hz->setChecked(d_data->m_AecSetup.tAf == 8333);
    d_data->m_ui->rdbTaf60Hz->blockSignals(false);

    setWaitCursor();
    emit AecSetupChanged(createAecVector());
    setNormalCursor();
}

/*
 * InOutBox::AecCustomWeights
 *****************************************************************************/
bool InOutBox::AecCustomWeights() const
{
    return (d_data->m_AecSetup.useCustomWeighting);
}

/*
 * InOutBox::setAecCustomWeights
 *****************************************************************************/
void InOutBox::setAecCustomWeights(const bool value)
{
    d_data->m_ui->cbxAecWeight->blockSignals(true);
    d_data->m_ui->cbxAecWeight->setCheckState(value ? Qt::Checked : Qt::Unchecked);
    d_data->m_ui->cbxAecWeight->blockSignals(false);

    d_data->m_AecSetup.useCustomWeighting = value;

    // Run enable aec widgets to enable / disable the set weighting button
    enableAecWidgets(d_data->m_AecSetup.run);

    setWaitCursor();
    emit AecSetupChanged(createAecVector());
    setNormalCursor();
}

/*
 * InOutBox::LscEnable
 *****************************************************************************/
bool InOutBox::LscEnable() const
{
    return (d_data->m_LscSetup.enable);
}

/*
 * InOutBox::LscK
 *****************************************************************************/
float InOutBox::LscK() const
{
    return (d_data->m_LscSetup.k);
}

/*
 * InOutBox::LscOffset
 *****************************************************************************/
float InOutBox::LscOffset() const
{
    return (d_data->m_LscSetup.offset);
}

/*
 * InOutBox::LscSlope
 *****************************************************************************/
float InOutBox::LscSlope() const
{
    return (d_data->m_LscSetup.slope);
}

/*
 * InOutBox::setLsc
 *****************************************************************************/
void InOutBox::setLsc(lsc_setup_t setup)
{
    d_data->m_LscSetup = setup;

    updateLscWidgets();

    emit LscChanged(createLscVector());
}

/*
 * InOutBox::FpsMode
 *****************************************************************************/
QString InOutBox::FpsMode() const
{
    return (d_data->m_ui->cbxFpsMode->currentText());
}

/*
 * InOutBox::setFpsMode
 *****************************************************************************/
void InOutBox::setFpsMode(const QString &mode)
{
    const int index = d_data->m_ui->cbxFpsMode->findText(mode);
    if (index != -1) {
        d_data->m_ui->cbxFpsMode->blockSignals(true);
        d_data->m_ui->cbxFpsMode->setCurrentIndex(index);
        d_data->m_ui->cbxFpsMode->blockSignals(false);

        emit ChainFpsModeChanged(d_data->m_ui->cbxFpsMode->itemData(index).toInt());
    }
}

/*
 * InOutBox::Phases
 *****************************************************************************/
int InOutBox::Phases() const
{
    return (d_data->m_ui->sbxPhases->value());
}

/*
 * InOutBox::setPhases
 *****************************************************************************/
void InOutBox::setPhases(const int phases)
{
    d_data->m_ui->sbxPhases->blockSignals(true);
    d_data->m_ui->sbxPhases->setValue(phases);
    d_data->m_ui->sbxPhases->blockSignals(false);

    emit ChainPhasesChanged(phases);
}

/*
 * InOutBox::VideoMode
 *****************************************************************************/
QString InOutBox::VideoMode() const
{
    return (d_data->m_ui->cbxVideoMode->currentText());
}

/*
 * InOutBox::setVideoMode
 *****************************************************************************/
void InOutBox::setVideoMode(const QString &mode)
{
    const int index = d_data->m_ui->cbxVideoMode->findText(mode);
    if (index != -1) {
        d_data->m_ui->cbxVideoMode->blockSignals(true);
        d_data->m_ui->cbxVideoMode->setCurrentIndex(index);
        show4kGenlockNote(d_data->m_ui->cbxVideoMode->itemData(index).toInt());
        d_data->m_ui->cbxVideoMode->blockSignals(false);

        emit ChainVideoModeChanged(d_data->m_ui->cbxVideoMode->itemData(index).toInt());
    }
}

/*
 * InOutBox::Sdi2Mode
 *****************************************************************************/
QString InOutBox::Sdi2Mode() const
{
    return (d_data->m_ui->cbxSdi2Mode->currentText());
}

/*
 * InOutBox::setSdi2Mode
 *****************************************************************************/
void InOutBox::setSdi2Mode(const QString &mode)
{
    const int index = d_data->m_ui->cbxSdi2Mode->findText(mode);
    if (index != -1) {
        d_data->m_ui->cbxSdi2Mode->blockSignals(true);
        d_data->m_ui->cbxSdi2Mode->setCurrentIndex(index);
        d_data->m_ui->cbxSdi2Mode->blockSignals(false);

        emit ChainSdi2ModeChanged(d_data->m_ui->cbxSdi2Mode->itemData(index).toInt());
    }
}

/*
 * InOutBox::Sdi1Downscaler
 *****************************************************************************/
QString InOutBox::Sdi1Downscaler() const
{
    return (d_data->m_ui->cbxSdi1Downscaler->currentText());
}

/*
 * InOutBox::setSdi1Downscaler
 *****************************************************************************/
void InOutBox::setSdi1Downscaler(const QString &mode)
{
    const int index = d_data->m_ui->cbxSdi1Downscaler->findText(mode);
    if (index != -1) {
        d_data->m_ui->cbxSdi1Downscaler->blockSignals(true);
        d_data->m_ui->cbxSdi1Downscaler->setCurrentIndex(index);
        d_data->m_ui->cbxSdi1Downscaler->blockSignals(false);

        EmitDownscaleChanged(1, index);
    }
}

/*
 * InOutBox::Sdi2Downscaler
 *****************************************************************************/
QString InOutBox::Sdi2Downscaler() const
{
    return (d_data->m_ui->cbxSdi2Downscaler->currentText());
}

/*
 * InOutBox::setSdi2Downscaler
 *****************************************************************************/
void InOutBox::setSdi2Downscaler(const QString &mode)
{
    const int index = d_data->m_ui->cbxSdi2Downscaler->findText(mode);
    if (index != -1) {
        d_data->m_ui->cbxSdi2Downscaler->blockSignals(true);
        d_data->m_ui->cbxSdi2Downscaler->setCurrentIndex(index);
        d_data->m_ui->cbxSdi2Downscaler->blockSignals(false);

        EmitDownscaleChanged(2, index);
    }
}

/*
 * InOutBox::FlipMode
 *****************************************************************************/
QString InOutBox::FlipMode() const
{
    return (d_data->m_ui->cbxFlipMode->currentText());
}

/*
 * InOutBox::setFlipMode
 *****************************************************************************/
void InOutBox::setFlipMode(const QString &mode)
{
    const int index = d_data->m_ui->cbxFlipMode->findText(mode);
    if (index != -1) {
        d_data->m_ui->cbxFlipMode->blockSignals(true);
        d_data->m_ui->cbxFlipMode->setCurrentIndex(index);
        d_data->m_ui->cbxFlipMode->blockSignals(false);

        emit ChainFlipModeChanged(d_data->m_ui->cbxFlipMode->itemData(index).toInt());
    }
}

/*
 * InOutBox::LogMode
 *****************************************************************************/
QString InOutBox::LogMode() const
{
    return (d_data->m_ui->cbxLogMode->currentText());
}

/*
 * InOutBox::setLogMode
 *****************************************************************************/
void InOutBox::setLogMode(const QString &mode)
{
    const int index = d_data->m_ui->cbxLogMode->findText(mode);
    if (index != -1) {
        d_data->m_ui->cbxLogMode->blockSignals(true);
        d_data->m_ui->cbxLogMode->setCurrentIndex(index);
        d_data->m_ui->cbxLogMode->blockSignals(false);

        emit LogModeChanged(d_data->m_ui->cbxLogMode->itemData(index).toInt());
    }
}

/*
 * InOutBox::PQMaxBrightness
 *****************************************************************************/
int InOutBox::PQMaxBrightness() const
{
    return (d_data->m_ui->sbxPQMaxBrightness->value());
}

/*
 * InOutBox::setPQMaxBrightness
 *****************************************************************************/
void InOutBox::setPQMaxBrightness(const int value)
{
    d_data->m_ui->sbxPQMaxBrightness->blockSignals(true);
    d_data->m_ui->sbxPQMaxBrightness->setValue(value);
    d_data->m_ui->sbxPQMaxBrightness->blockSignals(false);

    emit PQMaxBrightnessChanged(value);
}

/*
 * InOutBox::SLog3MasterGain
 *****************************************************************************/
int InOutBox::SLog3MasterGain() const
{
    return (d_data->m_ui->sbxSLog3MasterGain->value());
}

/*
 * InOutBox::setSLog3MasterGain
 *****************************************************************************/
void InOutBox::setSLog3MasterGain(const int value)
{
    d_data->m_ui->sbxSLog3MasterGain->blockSignals(true);
    d_data->m_ui->sbxSLog3MasterGain->setValue(value);
    d_data->m_ui->sbxSLog3MasterGain->blockSignals(false);

    emit SLog3MasterGainChanged(value);
}

/*
 * InOutBox::ColorSpace
 *****************************************************************************/
QString InOutBox::ColorSpace() const
{
    return (d_data->m_ui->cbxLogMode->currentText());
}

/*
 * InOutBox::setColorSpace
 *****************************************************************************/
void InOutBox::setColorSpace(const QString &mode)
{
    const int index = d_data->m_ui->cbxColorSpace->findText(mode);
    if (index != -1) {
        d_data->m_ui->cbxColorSpace->blockSignals(true);
        d_data->m_ui->cbxColorSpace->setCurrentIndex(index);
        d_data->m_ui->cbxColorSpace->blockSignals(false);

        emit ColorSpaceChanged(d_data->m_ui->cbxColorSpace->itemData(index).toInt());
    }
}

/*
 * InOutBox::TestPattern
 *****************************************************************************/
bool InOutBox::TestPattern() const
{
    return (d_data->m_ui->cbxTestPattern->isChecked());
}

/*
 * InOutBox::setTestPattern
 *****************************************************************************/
void InOutBox::setTestPattern(const bool value)
{
    d_data->m_ui->cbxTestPattern->blockSignals(true);
    d_data->m_ui->cbxTestPattern->setChecked(value);
    d_data->m_ui->cbxTestPattern->blockSignals(false);

    emit OsdTestPatternChanged(value ? 1 : 0);
}

/*
 * InOutBox::AudioEnable
 *****************************************************************************/
bool InOutBox::AudioEnable() const
{
    return (d_data->m_ui->cbxAudioEnable->isChecked());
}

/*
 * InOutBox::setAudioEnable
 *****************************************************************************/
void InOutBox::setAudioEnable(const bool value)
{
    d_data->m_ui->cbxAudioEnable->blockSignals(true);
    d_data->m_ui->cbxAudioEnable->setChecked(value);
    d_data->m_ui->cbxAudioEnable->blockSignals(false);

    emit ChainAudioEnableChanged(value);
}

/*
 * InOutBox::AudioGain
 *****************************************************************************/
double InOutBox::AudioGain() const
{
    return (d_data->m_ui->sbxAudioGain->value());
}

/*
 * InOutBox::setAudioGain
 *****************************************************************************/
void InOutBox::setAudioGain(const double gain)
{
    d_data->m_ui->sbxAudioGain->blockSignals(true);
    d_data->m_ui->sbxAudioGain->setValue(gain);
    d_data->m_ui->sbxAudioGain->blockSignals(false);

    emit ChainAudioGainChanged(gain);
}

/*
 * InOutBox::GenLockMode
 *****************************************************************************/
QString InOutBox::GenLockMode() const
{
    return (d_data->m_ui->cbxGenLockMode->currentText());
}

/*
 * InOutBox::setGenLockMode
 *****************************************************************************/
void InOutBox::setGenLockMode(const QString &mode)
{
    const int index = d_data->m_ui->cbxGenLockMode->findText(mode);
    if (index != -1) {
        d_data->m_ui->cbxGenLockMode->blockSignals(true);
        d_data->m_ui->cbxGenLockMode->setCurrentIndex(index);
        d_data->m_ui->cbxGenLockMode->blockSignals(false);

        emit ChainGenlockModeChanged(d_data->m_ui->cbxGenLockMode->itemData(index).toInt());
    }
}

/*
 * InOutBox::GenLockCrosslockEnable
 *****************************************************************************/
QString InOutBox::GenLockCrosslockEnable() const
{
    return (d_data->m_ui->cbxGenlockCrosslockEnable->currentText());
}

/*
 * InOutBox::GenLockCrosslockVmode
 *****************************************************************************/
QString InOutBox::GenLockCrosslockVmode() const
{
    return (d_data->m_ui->cbxGenlockCrosslockVmode->currentText());
}

/*
 * InOutBox::setGenLockCrosslock
 *****************************************************************************/
void InOutBox::setGenLockCrosslock(const QString &enable, const QString &vmode)
{
    int index = d_data->m_ui->cbxGenlockCrosslockEnable->findText(enable);
    if (index != -1) {
        d_data->m_ui->cbxGenlockCrosslockEnable->blockSignals(true);
        d_data->m_ui->cbxGenlockCrosslockEnable->setCurrentIndex(index);
        d_data->m_ui->cbxGenlockCrosslockEnable->blockSignals(false);
    }

    index = d_data->m_ui->cbxGenlockCrosslockVmode->findText(vmode);
    if (index != -1) {
        d_data->m_ui->cbxGenlockCrosslockVmode->blockSignals(true);
        d_data->m_ui->cbxGenlockCrosslockVmode->setCurrentIndex(index);
        d_data->m_ui->cbxGenlockCrosslockVmode->blockSignals(false);
    }

    emit ChainGenlockCrosslockChanged(
            d_data->m_ui->cbxGenlockCrosslockEnable->currentData().toInt(),
            d_data->m_ui->cbxGenlockCrosslockVmode->currentData().toInt());
}

/*
 * InOutBox::GenLockOffsetVertical
 *****************************************************************************/
int InOutBox::GenLockOffsetVertical() const
{
    return (d_data->m_ui->sbxGenLockOffsetVertical->value());
}

/**
 * InOutBox::setGenLockOffsetVertical
 *****************************************************************************/
void InOutBox::setGenLockOffsetVertical(const int value)
{
    d_data->m_ui->sbxGenLockOffsetVertical->blockSignals(true);
    d_data->m_ui->sbxGenLockOffsetVertical->setValue(value);
    d_data->m_ui->sbxGenLockOffsetVertical->blockSignals(false);

    emit ChainGenlockOffsetChanged(value, GenLockOffsetHorizontal());
}

/*
 * InOutBox::GenLockOffsetHorizontal
 *****************************************************************************/
int InOutBox::GenLockOffsetHorizontal() const
{
    return (d_data->m_ui->sbxGenlockOffsetHorizontal->value());
}

/*
 * InOutBox::setGenLockOffsetVertical
 *****************************************************************************/
void InOutBox::setGenLockOffsetHorizontal(const int value)
{
    d_data->m_ui->sbxGenlockOffsetHorizontal->blockSignals(true);
    d_data->m_ui->sbxGenlockOffsetHorizontal->setValue(value);
    d_data->m_ui->sbxGenlockOffsetHorizontal->blockSignals(false);

    emit ChainGenlockOffsetChanged(GenLockOffsetVertical(), value);
}

/*
 * InOutBox::GenLockTermination
 *****************************************************************************/
bool InOutBox::GenLockTermination() const
{
    return (d_data->m_ui->cbxGenLockTermination->isChecked());
}

/*
 * InOutBox::setGenLockTermination
 *****************************************************************************/
void InOutBox::setGenLockTermination(const bool value)
{
    d_data->m_ui->cbxGenLockTermination->blockSignals(true);
    d_data->m_ui->cbxGenLockTermination->setChecked(value);
    d_data->m_ui->cbxGenLockTermination->blockSignals(false);

    emit ChainGenlockTerminationChanged(value ? 1 : 0);
}

/*
 * InOutBox::prepareMode
 *****************************************************************************/
void InOutBox::prepareMode(const Mode mode)
{
    const bool v = DctWidgetBox::Normal != mode;
    d_data->m_ui->lblBayerPattern->setVisible(v);
    d_data->m_ui->cbxBayerPattern->setVisible(v);
}

/*
 * InOutBox::loadSettings
 *****************************************************************************/
void InOutBox::loadSettings(QSettings &s)
{
    s.beginGroup(INOUT_SETTINGS_SECTION_NAME);

    /* Disable genlock before making changes to the settings, otherwise the user might see
     * multiple warnings if no sync signal is available in slave mode and the process takes
     * very long because the device will run into the genlock lock timeout multiple times. */
    emit ChainGenlockModeChanged(GenLockModeDisabled);

    // Note: Set video mode first, otherwise gain and exposure will not be correctly set
    setFpsMode(s.value(INOUT_SETTINGS_FPS_MODE).toString());
    setPhases(s.value(INOUT_SETTINGS_PHASES).toInt());
    setVideoMode(s.value(INOUT_SETTINGS_VIDEO_MODE).toString());
    setSdi2Mode(s.value(INOUT_SETTINGS_SDI2_MODE).toString());
    setSdi1Downscaler(s.value(INOUT_SETTINGS_SDI1_DOWNSCALER).toString());
    setSdi2Downscaler(s.value(INOUT_SETTINGS_SDI2_DOWNSCALER).toString());
    setFlipMode(s.value(INOUT_SETTINGS_FLIP_MODE).toString());
    setLogMode(s.value(INOUT_SETTINGS_LOG_MODE).toString());
    setPQMaxBrightness(s.value(INOUT_SETTINGS_PQ_MAX_BRIGHTNESS).toInt());
    setSLog3MasterGain(s.value(INOUT_SETTINGS_SLOG3_MASTER_GAIN).toInt());
    setColorSpace(s.value(INOUT_SETTINGS_COLOR_SPACE).toString());
    setTestPattern(s.value(INOUT_SETTINGS_TEST_PATTERN).toBool());
    setAudioEnable(s.value(INOUT_SETTINGS_AUDIO_ENABLE).toBool());
    setAudioGain(s.value(INOUT_SETTINGS_AUDIO_GAIN).toDouble());

    setBayerPattern(s.value(INOUT_SETTINGS_BAYER_PATTERN).toInt());
    setCameraIso(s.value(INOUT_SETTINGS_CAMERA_ISO).toInt());
    setCameraExposure(s.value(INOUT_SETTINGS_CAMERA_EXPOSURE).toInt());

    setAecSetPoint(s.value(INOUT_SETTINGS_AEC_SETPOINT).toInt());
    setAecMaxIso(s.value(INOUT_SETTINGS_AEC_MAX_ISO).toInt());
    setAecControlSpeed(s.value(INOUT_SETTINGS_AEC_SPEED).toInt());
    setAecFlickerFrequency(s.value(INOUT_SETTINGS_AEC_FLICKER).toInt());
    setAecCostTInt(s.value(INOUT_SETTINGS_AEC_TINT_COST).toInt());
    setAecCostIris(s.value(INOUT_SETTINGS_AEC_IRIS_COST).toInt());
    setAecEnable(s.value(INOUT_SETTINGS_AEC_ENABLE).toBool());
    setAecCustomWeights(s.value(INOUT_SETTINGS_AEC_USE_CUSTOM_WEIGHTS).toBool());
    // NOTE: call qRegisterMetaTypeStreamOperators<QVector<int> >("QVector<int>"); in main.cpp
    d_data->m_weightDialog->setAecWeights(
            s.value(INOUT_SETTINGS_AEC_WEIGHTS).value<QVector<int>>());

    lsc_setup_t lscSetup;
    lscSetup.enable = s.value(INOUT_SETTINGS_LSC_ENABLE).toBool();
    lscSetup.k = s.value(INOUT_SETTINGS_LSC_K).toFloat();
    lscSetup.offset = s.value(INOUT_SETTINGS_LSC_OFFSET).toFloat();
    lscSetup.slope = s.value(INOUT_SETTINGS_LSC_SLOPE).toFloat();

    setLsc(lscSetup);

    setGenLockOffsetVertical(s.value(INOUT_SETTINGS_GENLOCK_OFFSET_VERTICAL).toInt());
    setGenLockOffsetHorizontal(s.value(INOUT_SETTINGS_GENLOCK_OFFSET_HORIZONTAL).toInt());
    setGenLockTermination(s.value(INOUT_SETTINGS_GENLOCK_TERMINATION).toBool());
    setGenLockCrosslock(s.value(INOUT_SETTINGS_GENLOCK_CROSSLOCK_ENABLE).toString(),
                        s.value(INOUT_SETTINGS_GENLOCK_CROSSLOCK_VMODE).toString());
    setGenLockMode(s.value(INOUT_SETTINGS_GENLOCK_MODE).toString());

    s.endGroup();
}

/*
 * InOutBox::saveSettings
 *****************************************************************************/
void InOutBox::saveSettings(QSettings &s)
{
    s.beginGroup(INOUT_SETTINGS_SECTION_NAME);

    s.setValue(INOUT_SETTINGS_BAYER_PATTERN, BayerPattern());
    s.setValue(INOUT_SETTINGS_CAMERA_ISO, CameraIso());
    s.setValue(INOUT_SETTINGS_CAMERA_EXPOSURE, CameraExposure());

    s.setValue(INOUT_SETTINGS_AEC_ENABLE, AecEnable());
    s.setValue(INOUT_SETTINGS_AEC_USE_CUSTOM_WEIGHTS, AecCustomWeights());
    // NOTE: call qRegisterMetaTypeStreamOperators<QVector<int> >("QVector<int>"); in main.cpp
    s.setValue(INOUT_SETTINGS_AEC_WEIGHTS,
               QVariant::fromValue<QVector<int>>(d_data->m_weightDialog->getAecWeights()));
    s.setValue(INOUT_SETTINGS_AEC_SETPOINT, AecSetPoint());
    s.setValue(INOUT_SETTINGS_AEC_MAX_ISO, AecMaxIso());
    s.setValue(INOUT_SETTINGS_AEC_SPEED, AecControlSpeed());
    s.setValue(INOUT_SETTINGS_AEC_FLICKER, AecFlickerFrequency());
    s.setValue(INOUT_SETTINGS_AEC_TINT_COST, AecCostTInt());
    s.setValue(INOUT_SETTINGS_AEC_IRIS_COST, AecCostIris());

    s.setValue(INOUT_SETTINGS_LSC_ENABLE, LscEnable());
    s.setValue(INOUT_SETTINGS_LSC_K, LscK());
    s.setValue(INOUT_SETTINGS_LSC_OFFSET, LscOffset());
    s.setValue(INOUT_SETTINGS_LSC_SLOPE, LscSlope());

    s.setValue(INOUT_SETTINGS_FPS_MODE, FpsMode());
    s.setValue(INOUT_SETTINGS_PHASES, Phases());
    s.setValue(INOUT_SETTINGS_VIDEO_MODE, VideoMode());
    s.setValue(INOUT_SETTINGS_SDI2_MODE, Sdi2Mode());
    s.setValue(INOUT_SETTINGS_SDI1_DOWNSCALER, Sdi1Downscaler());
    s.setValue(INOUT_SETTINGS_SDI2_DOWNSCALER, Sdi2Downscaler());
    s.setValue(INOUT_SETTINGS_FLIP_MODE, FlipMode());
    s.setValue(INOUT_SETTINGS_LOG_MODE, LogMode());
    s.setValue(INOUT_SETTINGS_PQ_MAX_BRIGHTNESS, PQMaxBrightness());
    s.setValue(INOUT_SETTINGS_SLOG3_MASTER_GAIN, SLog3MasterGain());
    s.setValue(INOUT_SETTINGS_COLOR_SPACE, ColorSpace());
    s.setValue(INOUT_SETTINGS_TEST_PATTERN, TestPattern());
    s.setValue(INOUT_SETTINGS_AUDIO_ENABLE, AudioEnable());
    s.setValue(INOUT_SETTINGS_AUDIO_GAIN, AudioGain());

    s.setValue(INOUT_SETTINGS_GENLOCK_MODE, GenLockMode());
    s.setValue(INOUT_SETTINGS_GENLOCK_CROSSLOCK_ENABLE, GenLockCrosslockEnable());
    s.setValue(INOUT_SETTINGS_GENLOCK_CROSSLOCK_VMODE, GenLockCrosslockVmode());
    s.setValue(INOUT_SETTINGS_GENLOCK_OFFSET_VERTICAL, GenLockOffsetVertical());
    s.setValue(INOUT_SETTINGS_GENLOCK_OFFSET_HORIZONTAL, GenLockOffsetHorizontal());
    s.setValue(INOUT_SETTINGS_GENLOCK_TERMINATION, GenLockTermination());

    s.endGroup();
}

/*
 * InOutBox::applySettings
 *****************************************************************************/
void InOutBox::applySettings()
{
    /* Disable genlock before making changes to the settings, otherwise the user might see
     * multiple warnings if no sync signal is available in slave mode and the process takes
     * very long because the device will run into the genlock lock timeout multiple times. */
    emit ChainGenlockModeChanged(GenLockModeDisabled);

    // Note: Set video mode first, otherwise gain and exposure will not be correctly set
    emit ChainFpsModeChanged(d_data->m_ui->cbxFpsMode->currentData().toInt());
    emit ChainPhasesChanged(Phases());
    emit ChainVideoModeChanged(d_data->m_ui->cbxVideoMode->currentData().toInt());
    emit ChainSdi2ModeChanged(d_data->m_ui->cbxSdi2Mode->currentData().toInt());
    EmitDownscaleChanged(1, d_data->m_ui->cbxSdi1Downscaler->currentData().toInt());
    EmitDownscaleChanged(2, d_data->m_ui->cbxSdi2Downscaler->currentData().toInt());
    emit ChainFlipModeChanged(d_data->m_ui->cbxFlipMode->currentData().toInt());
    emit LogModeChanged(d_data->m_ui->cbxLogMode->currentData().toInt());
    emit ColorSpaceChanged(d_data->m_ui->cbxColorSpace->currentData().toInt());
    emit OsdTestPatternChanged(TestPattern() ? 1 : 0);
    emit ChainAudioEnableChanged(AudioEnable());
    emit ChainAudioGainChanged(AudioGain());

    emit BayerPatternChanged(BayerPattern());
    emit CameraGainChanged(gainToIso(CameraIso()));
    emit CameraExposureChanged(CameraExposure());

    auto aecVector = createAecVector();
    emit AecSetupChanged(aecVector);
    emit AecSetupChangedForLensDriver(aecVector);
    d_data->m_weightDialog->setAecWeights(d_data->m_weightDialog->getAecWeights());

    emit ChainGenlockOffsetChanged(GenLockOffsetVertical(), GenLockOffsetHorizontal());
    emit ChainGenlockTerminationChanged(static_cast<int>(GenLockTermination()));
    emit ChainGenlockCrosslockChanged(
            d_data->m_ui->cbxGenlockCrosslockEnable->currentData().toInt(),
            d_data->m_ui->cbxGenlockCrosslockVmode->currentData().toInt());
    emit ChainGenlockModeChanged(d_data->m_ui->cbxGenLockMode->currentData().toInt());
}

/*
 * InOutBox::addBayerPattern
 *****************************************************************************/
void InOutBox::addBayerPattern(const QString &name, int id)
{
    d_data->m_ui->cbxBayerPattern->blockSignals(true);
    d_data->m_ui->cbxBayerPattern->addItem(name, id);
    d_data->m_ui->cbxBayerPattern->blockSignals(false);
}

/*
 * InOutBox::addFpsMode
 *****************************************************************************/
void InOutBox::addFpsMode(const QString &name, int id)
{
    d_data->m_ui->cbxFpsMode->blockSignals(true);
    d_data->m_ui->cbxFpsMode->addItem(name, id);
    d_data->m_ui->cbxFpsMode->blockSignals(false);
}

/*
 * InOutBox::addVideoMode
 *****************************************************************************/
void InOutBox::addVideoMode(const QString &name, int id)
{
    d_data->m_ui->cbxVideoMode->blockSignals(true);
    d_data->m_ui->cbxVideoMode->addItem(name, id);
    d_data->m_ui->cbxVideoMode->blockSignals(false);
}

/*
 * InOutBox::clearAllVideoModes
 *****************************************************************************/
void InOutBox::clearAllVideoModes()
{
    d_data->m_ui->cbxVideoMode->blockSignals(true);
    d_data->m_ui->cbxVideoMode->clear();
    d_data->m_ui->cbxVideoMode->blockSignals(false);
}

/*
 * InOutBox::addGenlockCrosslockVideoMode
 *****************************************************************************/
void InOutBox::addGenlockCrosslockVideoMode(const QString &name, int id)
{
    d_data->m_ui->cbxGenlockCrosslockVmode->blockSignals(true);
    d_data->m_ui->cbxGenlockCrosslockVmode->addItem(name, id);
    d_data->m_ui->cbxGenlockCrosslockVmode->blockSignals(false);
}

/*
 * InOutBox::clearAllGenlockCrosslockVideoModes
 *****************************************************************************/
void InOutBox::clearAllGenlockCrosslockVideoModes()
{
    d_data->m_ui->cbxGenlockCrosslockVmode->blockSignals(true);
    d_data->m_ui->cbxGenlockCrosslockVmode->clear();
    d_data->m_ui->cbxGenlockCrosslockVmode->blockSignals(false);
}

/*
 * InOutBox::addGenlockMode
 *****************************************************************************/
void InOutBox::addGenlockMode(const QString &name, int id)
{
    d_data->m_ui->cbxGenLockMode->blockSignals(true);
    d_data->m_ui->cbxGenLockMode->addItem(name, id);
    d_data->m_ui->cbxGenLockMode->blockSignals(false);
}

/*
 * InOutBox::addGenlockCrosslockEnable
 *****************************************************************************/
void InOutBox::addGenlockCrosslockEnable(const QString &name, int id)
{
    d_data->m_ui->cbxGenlockCrosslockEnable->blockSignals(true);
    d_data->m_ui->cbxGenlockCrosslockEnable->addItem(name, id);
    d_data->m_ui->cbxGenlockCrosslockEnable->blockSignals(false);
}

/*
 * InOutBox::addSdi2Mode
 *****************************************************************************/
void InOutBox::addSdi2Mode(const QString &name, int id)
{
    d_data->m_ui->cbxSdi2Mode->blockSignals(true);
    d_data->m_ui->cbxSdi2Mode->addItem(name, id);
    d_data->m_ui->cbxSdi2Mode->blockSignals(false);
}

/*
 * InOutBox::addDownscaleMode
 *****************************************************************************/
void InOutBox::addDownscaleMode(const QString &name, int id)
{
    d_data->m_ui->cbxSdi1Downscaler->blockSignals(true);
    d_data->m_ui->cbxSdi1Downscaler->addItem(name, id);
    d_data->m_ui->cbxSdi1Downscaler->blockSignals(false);

    d_data->m_ui->cbxSdi2Downscaler->blockSignals(true);
    d_data->m_ui->cbxSdi2Downscaler->addItem(name, id);
    d_data->m_ui->cbxSdi2Downscaler->blockSignals(false);
}

/*
 * InOutBox::addFlipMode
 *****************************************************************************/
void InOutBox::addFlipMode(const QString &name, int id)
{
    d_data->m_ui->cbxFlipMode->blockSignals(true);
    d_data->m_ui->cbxFlipMode->addItem(name, id);
    d_data->m_ui->cbxFlipMode->blockSignals(false);
}

/*
 * InOutBox::addLogMode
 *****************************************************************************/
void InOutBox::addLogMode(const QString &name, int id)
{
    d_data->m_ui->cbxLogMode->blockSignals(true);
    d_data->m_ui->cbxLogMode->addItem(name, id);
    d_data->m_ui->cbxLogMode->blockSignals(false);
}

/*
 * InOutBox::addColorSpace
 *****************************************************************************/
void InOutBox::addColorSpace(const QString &name, int id)
{
    d_data->m_ui->cbxColorSpace->blockSignals(true);
    d_data->m_ui->cbxColorSpace->addItem(name, id);
    d_data->m_ui->cbxColorSpace->blockSignals(false);
}

/*
 * InOutBox::setCameraSettingsVisible
 *****************************************************************************/
void InOutBox::setCameraSettingsVisible(const bool value)
{
    d_data->m_ui->gbxCameraSettings->setVisible(value);
}

/*
 * InOutBox::setAutoExposureSettingsVisible
 *****************************************************************************/
void InOutBox::setAutoExposureSettingsVisible(const bool value)
{
    d_data->m_ui->gbxAutoExposureSettings->setVisible(value);
}

/*
 * InOutBox::setLensChadingCorrectionSettingsVisible
 *****************************************************************************/
void InOutBox::setLensChadingCorrectionSettingsVisible(const bool value)
{
    d_data->m_ui->gbxLensShadingCorrectionSettings->setVisible(value);
}

/*
 * InOutBox::setGenLockVisible
 *****************************************************************************/
void InOutBox::setGenLockVisible(const bool genlock_visible, const bool crosslock_visible)
{
    d_data->m_ui->gbxGenLockSettings->setVisible(genlock_visible);
    d_data->m_ui->lblGenlockCrosslock->setVisible(crosslock_visible);
    d_data->m_ui->cbxGenlockCrosslockEnable->setVisible(crosslock_visible);
    d_data->m_ui->cbxGenlockCrosslockVmode->setVisible(crosslock_visible);
}

/*
 * InOutBox::setTimeCodeVisible
 *****************************************************************************/
void InOutBox::setTimeCodeVisible(const bool groupbox_visible, const bool hold_visible)
{
    d_data->m_ui->gbxTimeCodeSettings->setVisible(groupbox_visible);
    d_data->m_ui->btnHoldTimecode->setVisible(hold_visible);
}

/*
 * InOutBox::setFpsModeVisible
 *****************************************************************************/
void InOutBox::setFpsModeVisible(const bool value)
{
    d_data->m_ui->lblFpsMode->setVisible(value);
    d_data->m_ui->cbxFpsMode->setVisible(value);
}

/*
 * InOutBox::setPhasesVisible
 *****************************************************************************/
void InOutBox::setPhasesVisible(const bool value)
{
    d_data->m_ui->lblPhases->setVisible(value);
    d_data->m_ui->sbxPhases->setVisible(value);
}

/*
 * InOutBox::setSdi2ModeVisible
 *****************************************************************************/
void InOutBox::setSdi2ModeVisible(const bool value)
{
    d_data->m_ui->lblSdi2Mode->setVisible(value);
    d_data->m_ui->cbxSdi2Mode->setVisible(value);
}

/*
 * InOutBox::setDownscaleModeVisible
 *****************************************************************************/
void InOutBox::setDownscaleModeVisible(const bool value)
{
    d_data->m_ui->lblSdi1Downscaler->setVisible(value);
    d_data->m_ui->cbxSdi1Downscaler->setVisible(value);

    d_data->m_ui->lblSdi2Downscaler->setVisible(value);
    d_data->m_ui->cbxSdi2Downscaler->setVisible(value);
}

/*
 * InOutBox::setFlipModeVisible
 *****************************************************************************/
void InOutBox::setFlipModeVisible(const bool vertical, const bool horizontal)
{
    // clear flip-mode combo box
    d_data->m_ui->cbxFlipMode->blockSignals(true);
    d_data->m_ui->cbxFlipMode->clear();
    d_data->m_ui->cbxFlipMode->blockSignals(false);

    // if at least one flip mode is available
    if (vertical || horizontal) {
        // add "off" mode to combo box
        addFlipMode(GetFlipModeName(FlipModeOff), FlipModeOff);

        // if vertical flip is available, add it
        if (vertical) {
            addFlipMode(GetFlipModeName(FlipModeVertical), FlipModeVertical);
        }

        // if vertical flip is available, add it
        if (horizontal) {
            addFlipMode(GetFlipModeName(FlipModeHorizontal), FlipModeHorizontal);
        }

        // if both are available, add rotation mode
        if (vertical && horizontal) {
            addFlipMode(GetFlipModeName(FlipModeRotated), FlipModeRotated);
        }

        // show combo box and label
        d_data->m_ui->lblFlipMode->setVisible(true);
        d_data->m_ui->cbxFlipMode->setVisible(true);
    }
    // else if flip is not available
    else {
        // hide combo box and label
        d_data->m_ui->lblFlipMode->setVisible(false);
        d_data->m_ui->cbxFlipMode->setVisible(false);
    }
}

/*
 * InOutBox::setLogModeVisible
 *****************************************************************************/
void InOutBox::setLogModeVisible(const bool value)
{
    d_data->m_ui->lblLogMode->setVisible(value);
    d_data->m_ui->cbxLogMode->setVisible(value);

    /* PQ max brightness is only setable in PQ log mode, so if log mode is
     * not availbe there is no need to show those settings. */
    d_data->m_ui->lblPQMaxBrightness->setVisible(value);
    d_data->m_ui->sbxPQMaxBrightness->setVisible(value);
}

/*
 * InOutBox::setTestPatternVisible
 *****************************************************************************/
void InOutBox::setTestPatternVisible(const bool value)
{
    d_data->m_ui->lblTestPattern->setVisible(value);
    d_data->m_ui->cbxTestPattern->setVisible(value);
}

/*
 * InOutBox::setAudioEnableVisible
 *****************************************************************************/
void InOutBox::setAudioVisible(const bool value)
{
    d_data->m_ui->lblAudio->setVisible(value);
    d_data->m_ui->cbxAudioEnable->setVisible(value);
    d_data->m_ui->sbxAudioGain->setVisible(value);
}

/*
 * InOutBox::onBayerPatternChange
 *****************************************************************************/
void InOutBox::onBayerPatternChange(int value)
{
    const int index = d_data->m_ui->cbxBayerPattern->findData(value);
    if (index != -1) {
        d_data->m_ui->cbxBayerPattern->blockSignals(true);
        d_data->m_ui->cbxBayerPattern->setCurrentIndex(index);
        d_data->m_ui->cbxBayerPattern->blockSignals(false);
    }
}

/*
 * InOutBox::onCameraInfoChange
 *****************************************************************************/
void InOutBox::onCameraInfoChange(int min_gain, int max_gain, int min_exposure, int max_exposure,
                                  int min_iso)
{
    // Set min iso in class variable
    d_data->m_minIso = min_iso;

    /* Setup Sliders and Spin Boxes */
    // Set gain / iso range
    d_data->m_ui->sbxIso->blockSignals(true);
    d_data->m_ui->sbxIso->setRange(gainToIso(min_gain), gainToIso(max_gain));
    d_data->m_ui->sbxIso->blockSignals(false);

    d_data->m_ui->sldIso->blockSignals(true);
    d_data->m_ui->sldIso->setRange(gainToIso(min_gain), gainToIso(max_gain));
    d_data->m_ui->sldIso->blockSignals(false);

    // Set max iso range for auto exposure control
    d_data->m_ui->sbxMaxIso->blockSignals(true);
    d_data->m_ui->sbxMaxIso->setRange(gainToIso(min_gain), gainToIso(max_gain));
    d_data->m_ui->sbxMaxIso->blockSignals(false);

    d_data->m_ui->sldMaxIso->blockSignals(true);
    d_data->m_ui->sldMaxIso->setRange(gainToIso(min_gain), gainToIso(max_gain));
    d_data->m_ui->sldMaxIso->blockSignals(false);

    // Set exposure range
    d_data->m_ui->sbxExposure->blockSignals(true);
    d_data->m_ui->sbxExposure->setRange(min_exposure, max_exposure);
    d_data->m_ui->sbxExposure->blockSignals(false);

    d_data->m_ui->sldExposure->blockSignals(true);
    d_data->m_ui->sldExposure->setRange(min_exposure, max_exposure);
    d_data->m_ui->sldExposure->blockSignals(false);

    /* Setup the ISO combo box */
    // Clear combo box
    d_data->m_ui->cbxIso->clear();

    // Add the first item
    d_data->m_ui->cbxIso->addItem(QString("Select ISO"), 0);

    // Add iso values
    for (int i = IsoValueFirst; i < IsoValueMax; i++) {
        // Get the iso value
        const int iso = GetIsoValue(static_cast<IsoValue>(i));
        const int gain = isoToGain(iso);

        // Check if iso value is in valid iso range for this video mode
        if (gain < min_gain) {
            // Gain is below minimum, try next value
            continue;
        }
        if (gain > max_gain) {
            // Gain is above maximum, abort
            break;
        } // Gain is within range, add iso value to combo box
        d_data->m_ui->cbxIso->addItem(QString::number(iso), iso);
    }

    // Select the first entry in the list (the "Select ISO" entry)
    d_data->m_ui->cbxIso->setCurrentIndex(0);

    /* Setup the exposure combo box */
    // Clear combo box
    d_data->m_ui->cbxExposure->clear();

    // Add the first item
    d_data->m_ui->cbxExposure->addItem(QString("Select Shutter"), 0);

    // Add exposure times
    for (int i = ExposureTimeFirst; i < ExposureTimeMax; i++) {
        // Get the iso value
        const int exposure = GetExposureTime(static_cast<ExposureTime>(i));

        // Check if iso value is in valid iso range for this video mode
        if (exposure < min_exposure) {
            // Exposure is below minimum, try next value
            continue;
        }
        if (exposure > max_exposure) {
            // Exposure is above maximum, abort
            break;
        } // Gain is within range, add to combo box
        d_data->m_ui->cbxExposure->addItem(GetExposureTimeString(static_cast<ExposureTime>(i)),
                                           exposure);
    }

    // Select the first entry in the list (the "Select ISO" entry)
    d_data->m_ui->cbxExposure->setCurrentIndex(0);
}

/*
 * InOutBox::onCameraGainChange
 *****************************************************************************/
void InOutBox::onCameraGainChange(int value)
{
    d_data->m_ui->sbxIso->blockSignals(true);
    d_data->m_ui->sbxIso->setValue(gainToIso(value));
    d_data->m_ui->sbxIso->blockSignals(false);

    d_data->m_ui->sldIso->blockSignals(true);
    d_data->m_ui->sldIso->setValue(gainToIso(value));
    d_data->m_ui->sldIso->blockSignals(false);

    UpdateIsoComboBox(gainToIso(value));

    // Check if new gain is above clip value
    checkGainClip(value);
}

/*
 * InOutBox::onCameraGainClipChange
 *****************************************************************************/
void InOutBox::onCameraGainClipChange(int value)
{
    // Store new gain clip value and update the camera config widgets
    d_data->m_gainClip = value;

    enableCamConfWidgets(!d_data->m_AecSetup.run);
}

/*
 * InOutBox::onCameraExposureChange
 *****************************************************************************/
void InOutBox::onCameraExposureChange(int value)
{
    d_data->m_ui->sbxExposure->blockSignals(true);
    d_data->m_ui->sbxExposure->setValue(value);
    d_data->m_ui->sbxExposure->blockSignals(false);

    d_data->m_ui->sldExposure->blockSignals(true);
    d_data->m_ui->sldExposure->setValue(value);
    d_data->m_ui->sldExposure->blockSignals(false);

    UpdateExposureComboBox(value);
}

/*
 * InOutBox::onChainFpsModeChange
 *****************************************************************************/
void InOutBox::onChainFpsModeChange(int value)
{
    const int index = d_data->m_ui->cbxFpsMode->findData(value);
    if (index != -1) {
        d_data->m_ui->cbxFpsMode->blockSignals(true);
        d_data->m_ui->cbxFpsMode->setCurrentIndex(index);
        d_data->m_ui->cbxFpsMode->blockSignals(false);
    }
}

/*
 * InOutBox::onChainPhasesChange
 *****************************************************************************/
void InOutBox::onChainPhasesChange(int value)
{
    d_data->m_ui->sbxPhases->blockSignals(true);
    d_data->m_ui->sbxPhases->setValue(value);
    d_data->m_ui->sbxPhases->blockSignals(false);
}

/*
 * InOutBox::onChainVideoModeChange
 *****************************************************************************/
void InOutBox::onChainVideoModeChange(int value)
{
    const int index = d_data->m_ui->cbxVideoMode->findData(value);
    if (index != -1) {
        d_data->m_ui->cbxVideoMode->blockSignals(true);
        d_data->m_ui->cbxVideoMode->setCurrentIndex(index);
        show4kGenlockNote(d_data->m_ui->cbxVideoMode->itemData(index).toInt());
        d_data->m_ui->cbxVideoMode->blockSignals(false);
    }
}

/*
 * InOutBox::onChainSdi2ModeChange
 *****************************************************************************/
void InOutBox::onChainSdi2ModeChange(int value)
{
    const int index = d_data->m_ui->cbxSdi2Mode->findData(value);
    if (index != -1) {
        d_data->m_ui->cbxSdi2Mode->blockSignals(true);
        d_data->m_ui->cbxSdi2Mode->setCurrentIndex(index);
        d_data->m_ui->cbxSdi2Mode->blockSignals(false);
    }
}

/*
 * InOutBox::onChainDownscaleModeChange
 *****************************************************************************/
void InOutBox::onChainDownscaleModeChange(int sdi_out_idx, bool downscale, bool interlace)
{
    // Get index for combo box (1 = downscale, 2 = downscale and interlace)
    int index = 0;
    if (downscale) {
        index = 1;

        if (interlace) {
            index = 2;
        }
    }

    if (sdi_out_idx == 1) {
        d_data->m_ui->cbxSdi1Downscaler->blockSignals(true);
        d_data->m_ui->cbxSdi1Downscaler->setCurrentIndex(index);
        d_data->m_ui->cbxSdi1Downscaler->blockSignals(false);
    } else if (sdi_out_idx == 2) {
        d_data->m_ui->cbxSdi2Downscaler->blockSignals(true);
        d_data->m_ui->cbxSdi2Downscaler->setCurrentIndex(index);
        d_data->m_ui->cbxSdi2Downscaler->blockSignals(false);
    }
}

/*
 * InOutBox::onChainFlipModeChange
 *****************************************************************************/
void InOutBox::onChainFlipModeChange(int value)
{
    const int index = d_data->m_ui->cbxFlipMode->findData(value);
    if (index != -1) {
        d_data->m_ui->cbxFlipMode->blockSignals(true);
        d_data->m_ui->cbxFlipMode->setCurrentIndex(index);
        d_data->m_ui->cbxFlipMode->blockSignals(false);
    }
}

/*
 * InOutBox::onLogModeChange
 *****************************************************************************/
void InOutBox::onLogModeChange(int value)
{
    const int index = d_data->m_ui->cbxLogMode->findData(value);
    if (index != -1) {
        d_data->m_ui->cbxLogMode->blockSignals(true);
        d_data->m_ui->cbxLogMode->setCurrentIndex(index);
        d_data->m_ui->cbxLogMode->blockSignals(false);

        // Show max PQ brightness settings if log mode is PQ
        d_data->m_ui->lblPQMaxBrightness->setVisible(index == LogModePQ);
        d_data->m_ui->sbxPQMaxBrightness->setVisible(index == LogModePQ);

        // Show S-Log3 master gain settings if log mode is S-Log3
        d_data->m_ui->lblSLog3MasterGain->setVisible(index == LogModeSLog3);
        d_data->m_ui->sbxSLog3MasterGain->setVisible(index == LogModeSLog3);
    }
}

/*
 * InOutBox::onPQMaxBrightnessChange
 *****************************************************************************/
void InOutBox::onPQMaxBrightnessChange(int max_brightness)
{
    d_data->m_ui->sbxPQMaxBrightness->blockSignals(true);
    d_data->m_ui->sbxPQMaxBrightness->setValue(max_brightness);
    d_data->m_ui->sbxPQMaxBrightness->blockSignals(false);
}

/*
 * InOutBox::onSLog3MasterGainChange
 *****************************************************************************/
void InOutBox::onSLog3MasterGainChange(int master_gain)
{
    d_data->m_ui->sbxSLog3MasterGain->blockSignals(true);
    d_data->m_ui->sbxSLog3MasterGain->setValue(master_gain);
    d_data->m_ui->sbxSLog3MasterGain->blockSignals(false);
}

/*
 * InOutBox::onColorSpaceChange
 *****************************************************************************/
void InOutBox::onColorSpaceChange(int value)
{
    const int index = d_data->m_ui->cbxColorSpace->findData(value);
    if (index != -1) {
        d_data->m_ui->cbxColorSpace->blockSignals(true);
        d_data->m_ui->cbxColorSpace->setCurrentIndex(index);
        d_data->m_ui->cbxColorSpace->blockSignals(false);
    }
}

/*
 * InOutBox::onOsdTestPatternChange
 *****************************************************************************/
void InOutBox::onOsdTestPatternChange(int value)
{
    // set value of checkbox
    d_data->m_ui->cbxTestPattern->blockSignals(true);
    d_data->m_ui->cbxTestPattern->setCheckState((value != 0) ? Qt::Checked : Qt::Unchecked);
    d_data->m_ui->cbxTestPattern->blockSignals(false);
}

/*
 * InOutBox::onChainAudioEnableChange
 *****************************************************************************/
void InOutBox::onChainAudioEnableChange(bool enable)
{
    // set value of checkbox
    d_data->m_ui->cbxAudioEnable->blockSignals(true);
    d_data->m_ui->cbxAudioEnable->setCheckState(enable ? Qt::Checked : Qt::Unchecked);
    d_data->m_ui->cbxAudioEnable->blockSignals(false);

    // Disable audio gain spin box if audio is disabled
    d_data->m_ui->sbxAudioGain->setEnabled(enable);
}

/*
 * InOutBox::onChainAudioGainChange
 *****************************************************************************/
void InOutBox::onChainAudioGainChange(double gain)
{
    // set value of spin box
    d_data->m_ui->sbxAudioGain->blockSignals(true);
    d_data->m_ui->sbxAudioGain->setValue(gain);
    d_data->m_ui->sbxAudioGain->blockSignals(false);
}

/*
 * InOutBox::onChainGenlockModeChange
 *****************************************************************************/
void InOutBox::onChainGenlockModeChange(int value)
{
    const int index = d_data->m_ui->cbxGenLockMode->findData(value);
    if (index != -1) {
        d_data->m_ui->cbxGenLockMode->blockSignals(true);
        d_data->m_ui->cbxGenLockMode->setCurrentIndex(index);
        d_data->m_ui->cbxGenLockMode->blockSignals(false);

        updateEnableOfGenlockSettings(value,
                                      d_data->m_ui->cbxGenlockCrosslockEnable->currentIndex());
    }
}

/*
 * InOutBox::onChainGenlockCrosslockChange
 *****************************************************************************/
void InOutBox::onChainGenlockCrosslockChange(int enable, int vmode)
{
    int index = d_data->m_ui->cbxGenlockCrosslockEnable->findData(enable);
    if (index != -1) {
        d_data->m_ui->cbxGenlockCrosslockEnable->blockSignals(true);
        d_data->m_ui->cbxGenlockCrosslockEnable->setCurrentIndex(index);
        d_data->m_ui->cbxGenlockCrosslockEnable->blockSignals(false);

        updateEnableOfGenlockSettings(d_data->m_ui->cbxGenLockMode->currentIndex(), index);
    }

    index = d_data->m_ui->cbxGenlockCrosslockVmode->findData(vmode);
    if (index != -1) {
        d_data->m_ui->cbxGenlockCrosslockVmode->blockSignals(true);
        d_data->m_ui->cbxGenlockCrosslockVmode->setCurrentIndex(index);
        d_data->m_ui->cbxGenlockCrosslockVmode->blockSignals(false);
    }
}

/*
 * InOutBox::onChainGenlockOffsetChange
 *****************************************************************************/
void InOutBox::onChainGenlockOffsetChange(int vertical, int horizontal)
{
    d_data->m_ui->sbxGenLockOffsetVertical->blockSignals(true);
    d_data->m_ui->sbxGenLockOffsetVertical->setValue(vertical);
    d_data->m_ui->sbxGenLockOffsetVertical->blockSignals(false);

    d_data->m_ui->sbxGenlockOffsetHorizontal->blockSignals(true);
    d_data->m_ui->sbxGenlockOffsetHorizontal->setValue(horizontal);
    d_data->m_ui->sbxGenlockOffsetHorizontal->blockSignals(false);
}

/*
 * InOutBox::onChainGenlockTerminationChange
 *****************************************************************************/
void InOutBox::onChainGenlockTerminationChange(int value)
{
    // set value of checkbox
    d_data->m_ui->cbxGenLockTermination->blockSignals(true);
    d_data->m_ui->cbxGenLockTermination->setCheckState((value != 0) ? Qt::Checked : Qt::Unchecked);
    d_data->m_ui->cbxGenLockTermination->blockSignals(false);
}

/*
 * InOutBox::onCbxBayerPatternChange
 *****************************************************************************/
void InOutBox::onCbxBayerPatternChange(int index)
{
    setWaitCursor();
    emit BayerPatternChanged(d_data->m_ui->cbxBayerPattern->itemData(index).toInt());
    setNormalCursor();
}

/*
 * InOutBox::onSldIsoChange
 *****************************************************************************/
void InOutBox::onSldIsoChange(int value)
{
    // Set the spin box to new value
    d_data->m_ui->sbxIso->blockSignals(true);
    d_data->m_ui->sbxIso->setValue(value);
    d_data->m_ui->sbxIso->blockSignals(false);

    // Note: the combo box is set with the released() event of the slider

    if ((d_data->m_ui->sldIso->isSliderDown()) && (d_data->m_cntEvents++ > d_data->m_maxEvents)) {
        d_data->m_cntEvents = 0;

        const int gain = isoToGain(value);

        setWaitCursor();
        emit CameraGainChanged(gain);
        setNormalCursor();

        checkGainClip(gain);
    }
}

/*
 * InOutBox::onSldIsoReleased
 *****************************************************************************/
void InOutBox::onSldIsoReleased()
{
    d_data->m_cntEvents = 0;

    /* Set the combo box to new value, blocking signals is not needed, as we only use
     * the activated() event of the combo box which is only triggerd by user interaction */
    UpdateIsoComboBox(d_data->m_ui->sldIso->value());

    const int gain = isoToGain(d_data->m_ui->sldIso->value());

    setWaitCursor();
    emit CameraGainChanged(gain);
    setNormalCursor();

    checkGainClip(gain);
}

/*
 * InOutBox::onSbxIsoChange
 *****************************************************************************/
void InOutBox::onSbxIsoChange(int value)
{
    // Set the slider to new value
    d_data->m_ui->sldIso->blockSignals(true);
    d_data->m_ui->sldIso->setValue(value);
    d_data->m_ui->sldIso->blockSignals(false);

    /* Set the combo box to new value, blocking signals is not needed, as we only use
     * the activated() event of the combo box which is only triggerd by user interaction */
    UpdateIsoComboBox(value);

    const int gain = isoToGain(value);

    setWaitCursor();
    emit CameraGainChanged(gain);
    setNormalCursor();

    checkGainClip(gain);
}

/*
 * InOutBox::onCbxIsoChange
 *****************************************************************************/
void InOutBox::onCbxIsoChange(int index)
{
    // Check if a valid entry was selected (not "Select ISO")
    if (index == 0) {
        return;
    }

    // Get iso value from combo box
    const int iso = d_data->m_ui->cbxIso->currentData().toInt();

    // Set spin box to new iso value
    d_data->m_ui->sbxIso->blockSignals(true);
    d_data->m_ui->sbxIso->setValue(iso);
    d_data->m_ui->sbxIso->blockSignals(false);

    // Set slider to new iso value
    d_data->m_ui->sldIso->blockSignals(true);
    d_data->m_ui->sldIso->setValue(iso);
    d_data->m_ui->sldIso->blockSignals(false);

    const int gain = isoToGain(iso);

    // Emit gain changed event
    setWaitCursor();
    emit CameraGainChanged(gain);
    setNormalCursor();

    checkGainClip(gain);
}

/*
 * InOutBox::onBtnIsoMinusClicked
 *****************************************************************************/
void InOutBox::onBtnIsoMinusClicked()
{
    // Get current ISO value
    const int currentIso = d_data->m_ui->sbxIso->value();

    // Loop over all available ISO values until the next smaller ISO is found
    int i = 0;
    for (i = d_data->m_ui->cbxIso->count() - 1; i >= 1; i--) {
        // Get ISO for this index
        const int iso = d_data->m_ui->cbxIso->itemData(i).toInt();

        // If ISO is smaller than the current ISO, this is the new ISO
        if (iso < currentIso) {
            break;
        }
    }

    // Clip to smallest possible ISO (0 is "Select ISO" text)
    if (i <= 0) {
        i = 1;
    }

    // Set new ISO value
    d_data->m_ui->cbxIso->setCurrentIndex(i);

    // Manually call combo box change event
    onCbxIsoChange(i);

    // Enable / disable the plus minus buttons
    UpdateIsoPlusMinusButtons();

    // If minus button is now disabled, change focus to the plus button
    if (!d_data->m_ui->btnIsoMinus->isEnabled()) {
        d_data->m_ui->btnIsoPlus->setFocus();
    }
}

/*
 * InOutBox::onBtnIsoPlusClicked
 *****************************************************************************/
void InOutBox::onBtnIsoPlusClicked()
{
    // Get current ISO value
    const int currentIso = d_data->m_ui->sbxIso->value();

    // Loop over all available ISO values until the next bigger ISO is found
    int i = 0;
    for (i = 1; i < d_data->m_ui->cbxIso->count(); i++) {
        // Get ISO for this index
        const int iso = d_data->m_ui->cbxIso->itemData(i).toInt();

        // If ISO is bigger than the current ISO, this is the new ISO
        if (iso > currentIso) {
            break;
        }
    }

    // Clip to smallest possible ISO (0 is "Select ISO" text)
    if (i >= d_data->m_ui->cbxIso->count()) {
        i = d_data->m_ui->cbxIso->count() - 1;
    }

    // Set new ISO value
    d_data->m_ui->cbxIso->setCurrentIndex(i);

    // Manually call combo box change event
    onCbxIsoChange(i);

    // Enable / disable the plus minus buttons
    UpdateIsoPlusMinusButtons();

    // If plus button is now disabled, change focus to the minus button
    if (!d_data->m_ui->btnIsoPlus->isEnabled()) {
        d_data->m_ui->btnIsoMinus->setFocus();
    }
}

/*
 * InOutBox::onSldExposureChange
 *****************************************************************************/
void InOutBox::onSldExposureChange(int value)
{
    // Set the spin box to new value
    d_data->m_ui->sbxExposure->blockSignals(true);
    d_data->m_ui->sbxExposure->setValue(value);
    d_data->m_ui->sbxExposure->blockSignals(false);

    // Note: the combo box is set with the released() event of the slider

    if ((d_data->m_ui->sldExposure->isSliderDown())
        && (d_data->m_cntEvents++ > d_data->m_maxEvents)) {
        d_data->m_cntEvents = 0;

        setWaitCursor();
        emit CameraExposureChanged(value);
        setNormalCursor();
    }
}

/*
 * InOutBox::onSldExposureReleased
 *****************************************************************************/
void InOutBox::onSldExposureReleased()
{
    d_data->m_cntEvents = 0;

    /* Set the combo box to new value, blocking signals is not needed, as we only use
     * the activated() event of the combo box which is only triggerd by user interaction */
    UpdateExposureComboBox(d_data->m_ui->sldExposure->value());

    setWaitCursor();
    emit CameraExposureChanged(d_data->m_ui->sldExposure->value());
    setNormalCursor();
}

/*
 * InOutBox::onSbxExposureChange
 *****************************************************************************/
void InOutBox::onSbxExposureChange(int value)
{
    // Set the slider to new value
    d_data->m_ui->sldExposure->blockSignals(true);
    d_data->m_ui->sldExposure->setValue(value);
    d_data->m_ui->sldExposure->blockSignals(false);

    /* Set the combo box to new value, blocking signals is not needed, as we only use
     * the activated() event of the combo box which is only triggerd by user interaction */
    UpdateExposureComboBox(value);

    setWaitCursor();
    emit CameraExposureChanged(value);
    setNormalCursor();
}

/*
 * InOutBox::onCbxExposureChange
 *****************************************************************************/
void InOutBox::onCbxExposureChange(int index)
{
    // Check if a valid entry was selected (not "Select Shutter")
    if (index == 0) {
        return;
    }

    // Get exposure value from combo box
    const int exposure = d_data->m_ui->cbxExposure->currentData().toInt();

    // Set spin box to new exposure value
    d_data->m_ui->sbxExposure->blockSignals(true);
    d_data->m_ui->sbxExposure->setValue(exposure);
    d_data->m_ui->sbxExposure->blockSignals(false);

    // Set slider to new exposure value
    d_data->m_ui->sldExposure->blockSignals(true);
    d_data->m_ui->sldExposure->setValue(exposure);
    d_data->m_ui->sldExposure->blockSignals(false);

    // Emit exposure changed event
    setWaitCursor();
    emit CameraExposureChanged(exposure);
    setNormalCursor();
}

/*
 * InOutBox::onBtnExposureMinusClicked
 *****************************************************************************/
void InOutBox::onBtnExposureMinusClicked()
{
    // Get current exposure value
    const int currentExposure = d_data->m_ui->sbxExposure->value();

    // Loop over all available exposures until the next smaller exposure is found
    int i = 0;
    for (i = d_data->m_ui->cbxExposure->count() - 1; i >= 1; i--) {
        // Get exposure for this index
        const int exposure = d_data->m_ui->cbxExposure->itemData(i).toInt();

        // If exposure is smaller than the current exposure, this is the new exposure
        if (exposure < currentExposure) {
            break;
        }
    }

    // Clip to smallest possible exposure (0 is "Select Shutter" text)
    if (i <= 0) {
        i = 1;
    }

    // Set new exposure
    d_data->m_ui->cbxExposure->setCurrentIndex(i);

    // Manually call combo box change event
    onCbxExposureChange(i);

    // Enable / disable the plus minus buttons
    UpdateExposurePlusMinusButtons();

    // If minus button is now disabled, change focus to the plus button
    if (!d_data->m_ui->btnExposureMinus->isEnabled()) {
        d_data->m_ui->btnExposurePlus->setFocus();
    }
}

/*
 * InOutBox::onBtnExposurePlusClicked
 *****************************************************************************/
void InOutBox::onBtnExposurePlusClicked()
{
    // Get current exposure value
    const int currentExposure = d_data->m_ui->sbxExposure->value();

    // Loop over all available exposures until the next bigger exposure is found
    int i = 0;
    for (i = 1; i < d_data->m_ui->cbxExposure->count(); i++) {
        // Get exposure for this index
        const int exposure = d_data->m_ui->cbxExposure->itemData(i).toInt();

        // If exposure is bigger than the current exposure, this is the new exposure
        if (exposure > currentExposure) {
            break;
        }
    }

    // Clip to smallest possible exposure (0 is "Select Shutter" text)
    if (i >= d_data->m_ui->cbxExposure->count()) {
        i = d_data->m_ui->cbxExposure->count() - 1;
    }

    // Set new exposure
    d_data->m_ui->cbxExposure->setCurrentIndex(i);

    // Manually call combo box change event
    onCbxExposureChange(i);

    // Enable / disable the plus minus buttons
    UpdateExposurePlusMinusButtons();

    // If plus button is now disabled, change focus to the minus button
    if (!d_data->m_ui->btnExposurePlus->isEnabled()) {
        d_data->m_ui->btnExposureMinus->setFocus();
    }
}

/*
 * InOutBox::onCbxFpsModeChange
 *****************************************************************************/
void InOutBox::onCbxFpsModeChange(int index)
{
    setWaitCursor();
    emit ChainFpsModeChanged(d_data->m_ui->cbxFpsMode->itemData(index).toInt());
    setNormalCursor();
}

/*
 * InOutBox::onSbxPhasesChange
 *****************************************************************************/
void InOutBox::onSbxPhasesChange(int value)
{
    setWaitCursor();
    emit ChainPhasesChanged(value);
    setNormalCursor();
}

/*
 * InOutBox::onCbxVideoModeChange
 *****************************************************************************/
void InOutBox::onCbxVideoModeChange(int index)
{
    setWaitCursor();
    emit ChainVideoModeChanged(d_data->m_ui->cbxVideoMode->itemData(index).toInt());
    show4kGenlockNote(d_data->m_ui->cbxVideoMode->itemData(index).toInt());
    setNormalCursor();
}

/*
 * InOutBox::onCbxSdi2ModeChange
 *****************************************************************************/
void InOutBox::onCbxSdi2ModeChange(int index)
{
    setWaitCursor();
    emit ChainSdi2ModeChanged(d_data->m_ui->cbxSdi2Mode->itemData(index).toInt());
    setNormalCursor();
}

/*
 * InOutBox::onCbxSdi1DownscalerChange
 *****************************************************************************/
void InOutBox::onCbxSdi1DownscalerChange(int index)
{
    setWaitCursor();

    EmitDownscaleChanged(1, index);

    setNormalCursor();
}

/*
 * InOutBox::onCbxSdi2DownscalerChange
 *****************************************************************************/
void InOutBox::onCbxSdi2DownscalerChange(int index)
{
    setWaitCursor();

    EmitDownscaleChanged(2, index);

    setNormalCursor();
}

/*
 * InOutBox::onCbxFlipModeChange
 *****************************************************************************/
void InOutBox::onCbxFlipModeChange(int index)
{
    setWaitCursor();
    emit ChainFlipModeChanged(d_data->m_ui->cbxFlipMode->itemData(index).toInt());
    setNormalCursor();
}

/*
 * InOutBox::onCbxLogModeChange
 *****************************************************************************/
void InOutBox::onCbxLogModeChange(int index)
{
    setWaitCursor();
    emit LogModeChanged(d_data->m_ui->cbxLogMode->itemData(index).toInt());
    setNormalCursor();

    // Show max PQ brightness settings if log mode is PQ
    d_data->m_ui->lblPQMaxBrightness->setVisible(index == LogModePQ);
    d_data->m_ui->sbxPQMaxBrightness->setVisible(index == LogModePQ);

    // Show S-Log3 master gain settings if log mode is S-Log3
    d_data->m_ui->lblSLog3MasterGain->setVisible(index == LogModeSLog3);
    d_data->m_ui->sbxSLog3MasterGain->setVisible(index == LogModeSLog3);
}

/*
 * InOutBox::onSbxPQMaxBrightnessChange
 *****************************************************************************/
void InOutBox::onSbxPQMaxBrightnessChange(int value)
{
    setWaitCursor();
    emit PQMaxBrightnessChanged(value);
    setNormalCursor();
}

/*
 * InOutBox::onSbxSLog3MasterGainChange
 *****************************************************************************/
void InOutBox::onSbxSLog3MasterGainChange(int value)
{
    setWaitCursor();
    emit SLog3MasterGainChanged(value);
    setNormalCursor();
}

/*
 * InOutBox::onCbxColorSpaceChange
 *****************************************************************************/
void InOutBox::onCbxColorSpaceChange(int index)
{
    setWaitCursor();
    emit ColorSpaceChanged(d_data->m_ui->cbxColorSpace->itemData(index).toInt());
    setNormalCursor();
}

/*
 * InOutBox::onCbxTestPatternChange
 *****************************************************************************/
void InOutBox::onCbxTestPatternChange(int value)
{
    setWaitCursor();
    emit OsdTestPatternChanged((value == Qt::Checked) ? 1 : 0);
    setNormalCursor();
}

/*
 * InOutBox::onCbxAudioEnableChange
 *****************************************************************************/
void InOutBox::onCbxAudioEnableChange(int value)
{
    setWaitCursor();

    emit ChainAudioEnableChanged(value == Qt::Checked);

    // Disable audio gain spin box if audio is disabled
    d_data->m_ui->sbxAudioGain->setEnabled(value == Qt::Checked);

    setNormalCursor();
}

/*
 * InOutBox::onSbxAudioGainChange
 *****************************************************************************/
void InOutBox::onSbxAudioGainChange(double gain)
{
    setWaitCursor();
    emit ChainAudioGainChanged(gain);
    setNormalCursor();
}

/*
 * InOutBox::onCbxGenlockModeChange
 *****************************************************************************/
void InOutBox::onCbxGenlockModeChange(int index)
{
    setWaitCursor();
    emit ChainGenlockModeChanged(d_data->m_ui->cbxGenLockMode->itemData(index).toInt());
    updateEnableOfGenlockSettings(index, d_data->m_ui->cbxGenlockCrosslockEnable->currentIndex());
    setNormalCursor();
}

/*
 * InOutBox::onCbxGenlockCrosslockEnableChange
 *****************************************************************************/
void InOutBox::onCbxGenlockCrosslockEnableChange(int index)
{
    setWaitCursor();
    emit ChainGenlockCrosslockChanged(
            d_data->m_ui->cbxGenlockCrosslockEnable->itemData(index).toInt(),
            d_data->m_ui->cbxGenlockCrosslockVmode->currentData().toInt());
    updateEnableOfGenlockSettings(d_data->m_ui->cbxGenLockMode->currentIndex(), index);
    setNormalCursor();
}

/*
 * InOutBox::onCbxGenlockCrosslockVmodeChange
 *****************************************************************************/
void InOutBox::onCbxGenlockCrosslockVmodeChange(int index)
{
    setWaitCursor();
    emit ChainGenlockCrosslockChanged(
            d_data->m_ui->cbxGenlockCrosslockEnable->currentData().toInt(),
            d_data->m_ui->cbxGenlockCrosslockVmode->itemData(index).toInt());
    setNormalCursor();
}

/*
 * InOutBox::onSbxGenlockOffsetVerticalChange
 *****************************************************************************/
void InOutBox::onSbxGenlockOffsetVerticalChange(int value)
{
    setWaitCursor();
    emit ChainGenlockOffsetChanged(value, GenLockOffsetHorizontal());

    /* Setting the genlock offset might take a while (if no lock is possible)
     * this can cause a timer event of the spinbox to trigger, which will then
     * fire the value changed event of the spinbox again.
     * See: https://github.com/MRtrix3/mrtrix3/issues/306
     *      https://bugreports.qt.io/browse/QTBUG-33128
     * To avoid this we block all signals from the spinbox and then allow the
     * application to process all pending events. */
    d_data->m_ui->sbxGenLockOffsetVertical->blockSignals(true);
    QApplication::processEvents();
    d_data->m_ui->sbxGenLockOffsetVertical->blockSignals(false);
    setNormalCursor();
}

/*
 * InOutBox::onSbxGenlockOffsetHorizontalChange
 *****************************************************************************/
void InOutBox::onSbxGenlockOffsetHorizontalChange(int value)
{
    setWaitCursor();
    emit ChainGenlockOffsetChanged(GenLockOffsetVertical(), value);

    /* Setting the genlock offset might take a while (if no lock is possible)
     * this can cause a timer event of the spinbox to trigger, which will then
     * fire the value changed event of the spinbox again.
     * See: https://github.com/MRtrix3/mrtrix3/issues/306
     *      https://bugreports.qt.io/browse/QTBUG-33128
     * To avoid this we block all signals from the spinbox and then allow the
     * application to process all pending events. */
    d_data->m_ui->sbxGenLockOffsetVertical->blockSignals(true);
    QApplication::processEvents();
    d_data->m_ui->sbxGenLockOffsetVertical->blockSignals(false);
    setNormalCursor();
}

/*
 * InOutBox::onCbxGenlockTerminationChange
 *****************************************************************************/
void InOutBox::onCbxGenlockTerminationChange(int value)
{
    if ((GenLockMode() == GetGenlockModeName(GenLockModeMaster)) && (value != 0)) {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Termination enabled in Master Mode");
        msgBox.setText(
                "You have enabled Genlock Termination while the device is in Genlock Master "
                "Mode.\n\n"
                "In Master Mode the Termination is always disabled, regardless of the Genlock "
                "Termination setting. It will get enabled as soon as you switch to Slave Mode.");
        msgBox.exec();
    }

    setWaitCursor();
    emit ChainGenlockTerminationChanged((value == Qt::Checked) ? 1 : 0);
    setNormalCursor();
}

/*
 * InOutBox::onCbxAecEnableChange
 *****************************************************************************/
void InOutBox::onCbxAecEnableChange(int value)
{
    const bool enable = (value == Qt::Checked);

    d_data->m_AecSetup.run = enable;

    enableAecWidgets(enable);
    enableCamConfWidgets(!enable);

    setWaitCursor();
    emit AecEnableChanged(static_cast<int>(d_data->m_AecSetup.run));
    setNormalCursor();

    if (value == 0) {
        // aec disabled, update exposure, gain, aperture widgets
        setWaitCursor();
        emit ResyncRequest();
        setNormalCursor();
    }
}

/*
 * InOutBox::onCbxAecWeightChange
 *****************************************************************************/
void InOutBox::onCbxAecWeightChange(int value)
{
    const bool enable = (value == Qt::Checked);

    d_data->m_AecSetup.useCustomWeighting = enable;

    // Run enable aec widgets to enable / disable the set weighting button
    enableAecWidgets(d_data->m_AecSetup.run);

    setWaitCursor();
    emit AecSetupChanged(createAecVector());
    setNormalCursor();
}

/*
 * InOutBox::onAecEnableChange
 *****************************************************************************/
void InOutBox::onAecEnableChange(int enable)
{
    enableAecWidgets(enable != 0);
    enableCamConfWidgets(enable == 0);

    d_data->m_ui->cbxAecEnable->blockSignals(true);
    d_data->m_ui->cbxAecEnable->setCheckState((enable != 0) ? Qt::Checked : Qt::Unchecked);
    d_data->m_ui->cbxAecEnable->blockSignals(false);
}

/*
 * InOutBox::onAecSetupChange
 *****************************************************************************/
void InOutBox::onAecSetupChange(QVector<int> values)
{
    if (values.count() == 10) {
        d_data->m_AecSetup.run = (values[0] != 0);
        d_data->m_AecSetup.setPoint = values[1];
        d_data->m_AecSetup.speed = values[2];
        d_data->m_AecSetup.ClmTolerance = values[3];
        d_data->m_AecSetup.costGain = values[4];
        d_data->m_AecSetup.costTInt = values[5];
        d_data->m_AecSetup.costAperture = values[6];
        d_data->m_AecSetup.tAf = values[7];
        d_data->m_AecSetup.maxGain = values[8];
        d_data->m_AecSetup.useCustomWeighting = (values[9] != 0);

        // Run enable aec widgets to enable / disable the set weighting button
        enableAecWidgets(d_data->m_AecSetup.run);

        updateAecSetupWidgets();
    }
}

/*
 * InOutBox::onAecWeightsChange
 *****************************************************************************/
void InOutBox::onAecWeightsChange(QVector<int> weights)
{
    // Change weights in weight dialog
    emit WeightDialogAecWeightsChanged(weights); // NOLINT(performance-unnecessary-value-param)
}

/*
 * InOutBox::onIrisAvailableChange
 *****************************************************************************/
void InOutBox::onIrisAvailableChange(const bool available)
{
    // If iris control is available, show the radio buttons for auto/manual iris/shutter selection
    d_data->m_ui->rdbAecAutoIris->setVisible(available);
    d_data->m_ui->rdbAecManIris->setVisible(available);
    d_data->m_ui->rdbAecManShutter->setVisible(available);

    // If manual shutter has been active (auto-iris) and iris control is now lost,
    // make sure to switch back to manual iris (auto-shutter).
    if (d_data->m_ui->rdbAecManShutter->isChecked() && !available) {
        d_data->m_ui->rdbAecManIris->setChecked(true);

        d_data->m_ui->rdbAecAutoIris->blockSignals(true);
        d_data->m_ui->rdbAecAutoIris->setChecked(false);
        d_data->m_ui->rdbAecAutoIris->blockSignals(false);

        d_data->m_ui->rdbAecManShutter->blockSignals(true);
        d_data->m_ui->rdbAecManShutter->setChecked(false);
        d_data->m_ui->rdbAecManShutter->blockSignals(false);
    }
}

/*
 * InOutBox::onSldSetPointChange
 *****************************************************************************/
void InOutBox::onSldSetPointChange(int value)
{
    d_data->m_ui->sbxSetPoint->blockSignals(true);
    d_data->m_ui->sbxSetPoint->setValue(value);
    d_data->m_ui->sbxSetPoint->blockSignals(false);

    d_data->m_AecSetup.setPoint = value;

    if ((d_data->m_ui->sldSetPoint->isSliderDown())
        && (d_data->m_cntEvents++ > d_data->m_maxEvents)) {
        d_data->m_cntEvents = 0;

        setWaitCursor();
        emit AecSetupChanged(createAecVector());
        setNormalCursor();
    }
}

/*
 * InOutBox::onSldSetPointReleased
 *****************************************************************************/
void InOutBox::onSldSetPointReleased()
{
    d_data->m_cntEvents = 0;

    setWaitCursor();
    emit AecSetupChanged(createAecVector());
    setNormalCursor();
}

/*
 * InOutBox::onSbxSetPointChange
 *****************************************************************************/
void InOutBox::onSbxSetPointChange(int value)
{
    d_data->m_ui->sldSetPoint->blockSignals(true);
    d_data->m_ui->sldSetPoint->setValue(value);
    d_data->m_ui->sldSetPoint->blockSignals(false);

    d_data->m_AecSetup.setPoint = value;

    setWaitCursor();
    emit AecSetupChanged(createAecVector());
    setNormalCursor();
}

/*
 * InOutBox::onSldMaxIsoChange
 *****************************************************************************/
void InOutBox::onSldMaxIsoChange(int value)
{
    d_data->m_ui->sbxMaxIso->blockSignals(true);
    d_data->m_ui->sbxMaxIso->setValue(value);
    d_data->m_ui->sbxMaxIso->blockSignals(false);

    d_data->m_AecSetup.maxGain = isoToGain(value);

    if ((d_data->m_ui->sldMaxIso->isSliderDown())
        && (d_data->m_cntEvents++ > d_data->m_maxEvents)) {
        d_data->m_cntEvents = 0;

        setWaitCursor();
        emit AecSetupChanged(createAecVector());
        setNormalCursor();
    }
}

/*
 * InOutBox::onSldMaxIsoReleased
 *****************************************************************************/
void InOutBox::onSldMaxIsoReleased()
{
    d_data->m_cntEvents = 0;

    setWaitCursor();
    emit AecSetupChanged(createAecVector());
    setNormalCursor();
}

/*
 * InOutBox::onSbxMaxIsoChange
 *****************************************************************************/
void InOutBox::onSbxMaxIsoChange(int value)
{
    d_data->m_ui->sldMaxIso->blockSignals(true);
    d_data->m_ui->sldMaxIso->setValue(value);
    d_data->m_ui->sldMaxIso->blockSignals(false);

    d_data->m_AecSetup.maxGain = isoToGain(value);

    setWaitCursor();
    emit AecSetupChanged(createAecVector());
    setNormalCursor();
}

/*
 * InOutBox::onSldControlSpeedChange
 *****************************************************************************/
void InOutBox::onSldControlSpeedChange(int value)
{
    d_data->m_ui->sbxControlSpeed->blockSignals(true);
    d_data->m_ui->sbxControlSpeed->setValue(value);
    d_data->m_ui->sbxControlSpeed->blockSignals(false);

    d_data->m_AecSetup.speed = value;

    if ((d_data->m_ui->sldControlSpeed->isSliderDown())
        && (d_data->m_cntEvents++ > d_data->m_maxEvents)) {
        d_data->m_cntEvents = 0;

        setWaitCursor();
        emit AecSetupChanged(createAecVector());
        setNormalCursor();
    }
}

/*
 * InOutBox::onSldControlSpeedReleased
 *****************************************************************************/
void InOutBox::onSldControlSpeedReleased()
{
    d_data->m_cntEvents = 0;

    setWaitCursor();
    emit AecSetupChanged(createAecVector());
    setNormalCursor();
}

/*
 * InOutBox::onSbxControlSpeedChange
 *****************************************************************************/
void InOutBox::onSbxControlSpeedChange(int value)
{
    d_data->m_ui->sldControlSpeed->blockSignals(true);
    d_data->m_ui->sldControlSpeed->setValue(value);
    d_data->m_ui->sldControlSpeed->blockSignals(false);

    d_data->m_AecSetup.speed = value;

    setWaitCursor();
    emit AecSetupChanged(createAecVector());
    setNormalCursor();
}

/*
 * InOutBox::onAecStatChange
 *****************************************************************************/
// NOLINTNEXTLINE
void InOutBox::onAecStatChange(QVector<int> values)
{
    (void)values;
    /*
    15 Parameter:
    z
    lumaDeviation
    semSetPoint
    meanLuma
    meanLumaObject
    meanHistogram
    clmHistogramSize
    sumHistogram
    maxExposure
    newExposure
    targetExposure
    realExposure
    gain
    t_int
    aperture
    */
}

/*
 * InOutBox::UpdateIsoPlusMinusButtons
 *****************************************************************************/
void InOutBox::UpdateIsoPlusMinusButtons()
{
    // Get current ISO value
    const int iso = d_data->m_ui->sbxIso->value();

    /* Disable minus button, if the value is smaller than the value of the first combo box item
     * or the iso spin box is diabled (aec enabled) */
    if (iso <= d_data->m_ui->cbxIso->itemData(1).toInt() || !d_data->m_ui->sbxIso->isEnabled()) {
        d_data->m_ui->btnIsoMinus->setEnabled(false);
    }
    // Else enable it
    else {
        d_data->m_ui->btnIsoMinus->setEnabled(true);
    }

    /* Disable plus button, if this the value is bigger than the value of the last combo box item
     * or the iso spin box is disabled (aec enabled) */
    if (iso >= d_data->m_ui->cbxIso->itemData(d_data->m_ui->cbxIso->count() - 1).toInt()
        || !d_data->m_ui->sbxIso->isEnabled()) {
        d_data->m_ui->btnIsoPlus->setEnabled(false);
    }
    // Else enable it
    else {
        d_data->m_ui->btnIsoPlus->setEnabled(true);
    }
}

/*
 * InOutBox::UpdateExposurePlusMinusButtons
 *****************************************************************************/
void InOutBox::UpdateExposurePlusMinusButtons()
{
    // Get current exposure value
    const int exposure = d_data->m_ui->sbxExposure->value();

    const int currentExposure = d_data->m_ui->cbxExposure->itemData(1).toInt();

    /* Disable minus button, if the value is smaller than the value of the first combo box item
     * or the exposure spin box is diabled (aec enabled) */
    if (exposure <= currentExposure || !d_data->m_ui->sbxExposure->isEnabled()) {
        d_data->m_ui->btnExposureMinus->setEnabled(false);
    }
    // Else enable it
    else {
        d_data->m_ui->btnExposureMinus->setEnabled(true);
    }

    /* Disable plus button, if this the value is bigger than the value of the last combo box item
     * or the exposure spin box is diabled (aec enabled) */
    if (exposure >= d_data->m_ui->cbxExposure->itemData(d_data->m_ui->cbxExposure->count() - 1)
                            .toInt()
        || !d_data->m_ui->sbxExposure->isEnabled()) {
        d_data->m_ui->btnExposurePlus->setEnabled(false);
    }
    // Else enable it
    else {
        d_data->m_ui->btnExposurePlus->setEnabled(true);
    }
}

/*
 * InOutBox::UpdateIsoComboBox
 *****************************************************************************/
void InOutBox::UpdateIsoComboBox(int iso)
{
    // Check if the selected ISO value is one of the fixed ISO values
    int index = d_data->m_ui->cbxIso->findData(iso);

    /* In case nothing was found, findData() returns -1, in this case set index to 0
     * to show the default "Select ISO" text in the combo box */
    if (index == -1) {
        index = 0;
    }

    // Set the current combo box index
    d_data->m_ui->cbxIso->setCurrentIndex(index);

    // Enable / disable the plus minus buttons
    UpdateIsoPlusMinusButtons();
}

/*
 * InOutBox::UpdateExposureComboBox
 *****************************************************************************/
void InOutBox::UpdateExposureComboBox(int exposure)
{
    // Check if the selected exposure value is one of the fixed exposure values
    int index = d_data->m_ui->cbxExposure->findData(exposure);

    /* In case nothing was found, findData() returns -1, in this case set index to 0
     * to show the default "Select Shutter" text in the combo box */
    if (index == -1) {
        index = 0;
    }

    // Set the current combo box index
    d_data->m_ui->cbxExposure->setCurrentIndex(index);

    // Enable / disable the plus minus buttons
    UpdateExposurePlusMinusButtons();
}

/*
 * InOutBox::updateAecSetupWidgets
 *****************************************************************************/
void InOutBox::updateAecSetupWidgets()
{
    // Custom weighting
    d_data->m_ui->cbxAecWeight->blockSignals(true);
    d_data->m_ui->cbxAecWeight->setChecked(d_data->m_AecSetup.useCustomWeighting);
    d_data->m_ui->cbxAecWeight->blockSignals(false);

    // Set point
    d_data->m_ui->sbxSetPoint->blockSignals(true);
    d_data->m_ui->sbxSetPoint->setValue(d_data->m_AecSetup.setPoint);
    d_data->m_ui->sbxSetPoint->blockSignals(false);
    d_data->m_ui->sldSetPoint->blockSignals(true);
    d_data->m_ui->sldSetPoint->setValue(d_data->m_AecSetup.setPoint);
    d_data->m_ui->sldSetPoint->blockSignals(false);

    // Max ISO
    d_data->m_ui->sbxMaxIso->blockSignals(true);
    d_data->m_ui->sbxMaxIso->setValue(gainToIso(d_data->m_AecSetup.maxGain));
    d_data->m_ui->sbxMaxIso->blockSignals(false);
    d_data->m_ui->sldMaxIso->blockSignals(true);
    d_data->m_ui->sldMaxIso->setValue(gainToIso(d_data->m_AecSetup.maxGain));
    d_data->m_ui->sldMaxIso->blockSignals(false);

    // Control Speed
    d_data->m_ui->sbxControlSpeed->blockSignals(true);
    d_data->m_ui->sbxControlSpeed->setValue(d_data->m_AecSetup.speed);
    d_data->m_ui->sbxControlSpeed->blockSignals(false);
    d_data->m_ui->sldControlSpeed->blockSignals(true);
    d_data->m_ui->sldControlSpeed->setValue(d_data->m_AecSetup.speed);
    d_data->m_ui->sldControlSpeed->blockSignals(false);

    // flicker
    d_data->m_ui->rdbTaf50Hz->blockSignals(true);
    d_data->m_ui->rdbTaf50Hz->setChecked(d_data->m_AecSetup.tAf == 10000);
    d_data->m_ui->rdbTaf50Hz->blockSignals(false);

    d_data->m_ui->rdbTaf60Hz->blockSignals(true);
    d_data->m_ui->rdbTaf60Hz->setChecked(d_data->m_AecSetup.tAf == 8333);
    d_data->m_ui->rdbTaf60Hz->blockSignals(false);

    // Manual Cost Values
    d_data->m_ui->rdbAecManShutter->blockSignals(true);
    d_data->m_ui->rdbAecManShutter->setChecked(d_data->m_AecSetup.costTInt == 0);
    d_data->m_ui->rdbAecManShutter->blockSignals(false);

    d_data->m_ui->rdbAecManIris->blockSignals(true);
    d_data->m_ui->rdbAecManIris->setChecked(d_data->m_AecSetup.costAperture == 0);
    d_data->m_ui->rdbAecManIris->blockSignals(false);

    d_data->m_ui->rdbAecAutoIris->blockSignals(true);
    d_data->m_ui->rdbAecAutoIris->setChecked(
            ((d_data->m_AecSetup.costAperture != 0) && (d_data->m_AecSetup.costTInt != 0)));
    d_data->m_ui->rdbAecAutoIris->blockSignals(false);
}

/*
 * InOutBox::updateLscWidgets
 *****************************************************************************/
void InOutBox::updateLscWidgets()
{
    // enable
    d_data->m_ui->cbxLscEnable->blockSignals(true);
    d_data->m_ui->cbxLscEnable->setChecked(d_data->m_LscSetup.enable);
    d_data->m_ui->cbxLscEnable->blockSignals(false);

    // k
    d_data->m_ui->sbxK->blockSignals(true);
    d_data->m_ui->sbxK->setValue(static_cast<double>(d_data->m_LscSetup.k));
    d_data->m_ui->sbxK->setEnabled(d_data->m_LscSetup.enable);
    d_data->m_ui->sbxK->blockSignals(false);
    d_data->m_ui->sldK->blockSignals(true);
    d_data->m_ui->sldK->setValue(static_cast<int>(d_data->m_LscSetup.k * 100.0f));
    d_data->m_ui->sldK->setEnabled(d_data->m_LscSetup.enable);
    d_data->m_ui->sldK->blockSignals(false);

    // offset
    d_data->m_ui->sbxOffset->blockSignals(true);
    d_data->m_ui->sbxOffset->setValue(static_cast<double>(d_data->m_LscSetup.offset));
    d_data->m_ui->sbxOffset->setEnabled(d_data->m_LscSetup.enable);
    d_data->m_ui->sbxOffset->blockSignals(false);
    d_data->m_ui->sldOffset->blockSignals(true);
    d_data->m_ui->sldOffset->setValue(static_cast<int>(d_data->m_LscSetup.offset * 100.0f));
    d_data->m_ui->sldOffset->setEnabled(d_data->m_LscSetup.enable);
    d_data->m_ui->sldOffset->blockSignals(false);

    // slope
    d_data->m_ui->sbxSlope->blockSignals(true);
    d_data->m_ui->sbxSlope->setValue(static_cast<double>(d_data->m_LscSetup.slope));
    d_data->m_ui->sbxSlope->setEnabled(d_data->m_LscSetup.enable);
    d_data->m_ui->sbxSlope->blockSignals(false);
    d_data->m_ui->sldSlope->blockSignals(true);
    d_data->m_ui->sldSlope->setValue(static_cast<int>(d_data->m_LscSetup.slope * 100.0f));
    d_data->m_ui->sldSlope->setEnabled(d_data->m_LscSetup.enable);
    d_data->m_ui->sldSlope->blockSignals(false);
}

/*
 * InOutBox::enableAecWidgets
 *****************************************************************************/
void InOutBox::enableAecWidgets(bool enable)
{
    d_data->m_ui->cbxAecWeight->setEnabled(enable);

    /* The configure weight button shall only be enabled, if custom weighting
     * is enabled */
    if (d_data->m_AecSetup.useCustomWeighting && enable) {
        d_data->m_ui->btnAecWeight->setEnabled(true);
    } else {
        d_data->m_ui->btnAecWeight->setEnabled(false);

        // Make sure the weights dialog is closed
        d_data->m_weightDialog->close();
    }

    d_data->m_ui->sbxSetPoint->setEnabled(enable);
    d_data->m_ui->sldSetPoint->setEnabled(enable);

    d_data->m_ui->sbxMaxIso->setEnabled(enable);
    d_data->m_ui->sldMaxIso->setEnabled(enable);

    d_data->m_ui->sbxControlSpeed->setEnabled(enable);
    d_data->m_ui->sldControlSpeed->setEnabled(enable);

    d_data->m_ui->rdbTaf50Hz->setEnabled(enable);
    d_data->m_ui->rdbTaf60Hz->setEnabled(enable);

    d_data->m_ui->rdbAecAutoIris->setEnabled(enable);
    d_data->m_ui->rdbAecManIris->setEnabled(enable);
    d_data->m_ui->rdbAecManShutter->setEnabled(enable);
}

/*
 * InOutBox::enableCamConfWidgets
 *****************************************************************************/
void InOutBox::enableCamConfWidgets(bool enable)
{
    d_data->m_ui->sbxIso->setEnabled(enable);
    d_data->m_ui->sldIso->setEnabled(enable);
    d_data->m_ui->cbxIso->setEnabled(enable);
    UpdateIsoPlusMinusButtons();

    /* Enable exposure controls if enable is true, or if the AEC is in manual
     * exposure mode.
     * Additinally disable the exposure controls if the gain is curently higher
     * than the clip value. */
    bool exposureEnable = false;
    if ((enable || (d_data->m_AecSetup.run && d_data->m_AecSetup.costTInt == 0))
        && !(d_data->m_gain > d_data->m_gainClip)) {
        exposureEnable = true;
    }

    // Show note on locked exposure if needed
    d_data->m_ui->lblExposureNote->setVisible(d_data->m_gain > d_data->m_gainClip);

    d_data->m_ui->lblExposureNote->setEnabled(enable);
    d_data->m_ui->sbxExposure->setEnabled(exposureEnable);
    d_data->m_ui->sldExposure->setEnabled(exposureEnable);
    d_data->m_ui->cbxExposure->setEnabled(exposureEnable);
    UpdateExposurePlusMinusButtons();
}

/*
 * InOutBox::updateEnableOfGenlockSettings
 *****************************************************************************/
void InOutBox::updateEnableOfGenlockSettings(int genlockMode, int crosslockMode)
{
    d_data->m_ui->pbGenlockSync->setEnabled(false);

    if (genlockMode == GenLockModeMaster) {
        d_data->m_ui->cbxGenlockCrosslockEnable->setEnabled(false);
        d_data->m_ui->cbxGenlockCrosslockVmode->setEnabled(false);
        d_data->m_ui->sbxGenLockOffsetVertical->setEnabled(false);
        d_data->m_ui->sbxGenlockOffsetHorizontal->setEnabled(false);
        d_data->m_ui->cbxGenLockTermination->setEnabled(false);
    } else {
        d_data->m_ui->cbxGenlockCrosslockEnable->setEnabled(true);

        if (crosslockMode == GenLockCrosslockEnableOtherHDMode) {
            d_data->m_ui->cbxGenlockCrosslockVmode->setEnabled(true);
        } else {
            d_data->m_ui->cbxGenlockCrosslockVmode->setEnabled(false);
        }

        d_data->m_ui->sbxGenLockOffsetVertical->setEnabled(true);
        d_data->m_ui->sbxGenlockOffsetHorizontal->setEnabled(true);
        d_data->m_ui->cbxGenLockTermination->setEnabled(true);
        if (genlockMode == GenLockModeSlave) {
            d_data->m_ui->pbGenlockSync->setEnabled(true);
        }
    }
}

/*
 * InOutBox::createAecVector
 *****************************************************************************/
QVector<int> InOutBox::createAecVector()
{
    // pack aec setup values into vector
    QVector<int> values(10);
    values[0] = static_cast<int>(d_data->m_AecSetup.run);
    values[1] = d_data->m_AecSetup.setPoint;
    values[2] = d_data->m_AecSetup.speed;
    values[3] = d_data->m_AecSetup.ClmTolerance;
    values[4] = d_data->m_AecSetup.costGain;
    values[5] = d_data->m_AecSetup.costTInt;
    values[6] = d_data->m_AecSetup.costAperture;
    values[7] = d_data->m_AecSetup.tAf;
    values[8] = d_data->m_AecSetup.maxGain;
    values[9] = static_cast<int>(d_data->m_AecSetup.useCustomWeighting);
    return (values);
}

/*
 * InOutBox::createLscVector
 *****************************************************************************/
QVector<uint> InOutBox::createLscVector()
{
    // pack LSC setup values into vector
    QVector<uint> values(4);
    values[0] = static_cast<unsigned int>(d_data->m_LscSetup.enable);

    /* The parameters k, offset and slope have to be converted from
     * float to Q2.30 fixed point format */
    values[1] = static_cast<uint>(d_data->m_LscSetup.k) * (1u << 30u);
    values[2] = static_cast<uint>(d_data->m_LscSetup.offset) * (1u << 30u);
    values[3] = static_cast<uint>(d_data->m_LscSetup.slope) * (1u << 30u);
    return (values);
}

/*
 * InOutBox::onBtnTimecodeSetClicked
 *****************************************************************************/
void InOutBox::onBtnTimecodeSetClicked()
{
    QVector<int> v_time(3);

    const QTime &time = d_data->m_ui->tmeTimecode->time();
    v_time[0] = time.hour();
    v_time[1] = time.minute();
    v_time[2] = time.second();

    setWaitCursor();
    emit ChainTimecodeSetChanged(v_time);
    setNormalCursor();
}

/*
 * InOutBox::onBtnTimecodeGetClicked
 *****************************************************************************/
void InOutBox::onBtnTimecodeGetClicked()
{
    setWaitCursor();
    emit ChainTimecodeGetRequested();
    setNormalCursor();
}

/*
 * InOutBox::onBtnTimecodeHoldClicked
 *****************************************************************************/
void InOutBox::onBtnTimecodeHoldClicked(bool checked)
{
    setWaitCursor();
    emit ChainTimecodeHoldChanged(checked);
    setNormalCursor();
}

/*
 * InOutBox::onChainTimecodeChange
 *****************************************************************************/
void InOutBox::onChainTimecodeChange(QVector<int> time)
{
    d_data->m_ui->tmeTimecode->setTime(QTime(time[0], time[1], time[2]));
}

/*
 * InOutBox::onChainTimecodeHoldChange
 *****************************************************************************/
void InOutBox::onChainTimecodeHoldChange(bool enable)
{
    d_data->m_ui->btnHoldTimecode->blockSignals(true);
    d_data->m_ui->btnHoldTimecode->setChecked(enable);
    d_data->m_ui->btnHoldTimecode->blockSignals(false);
}

/*
 * InOutBox::onTaf50Toggle
 *****************************************************************************/
void InOutBox::onTaf50Toggle(bool checked)
{
    if (checked) {
        // 50 Hz
        d_data->m_AecSetup.tAf = 10000;

        setWaitCursor();
        emit AecSetupChanged(createAecVector());
        setNormalCursor();
    }
}

/*
 * InOutBox::onTaf60Toggle
 *****************************************************************************/
void InOutBox::onTaf60Toggle(bool checked)
{
    if (checked) {
        // 60 Hz
        d_data->m_AecSetup.tAf = 8333;

        setWaitCursor();
        emit AecSetupChanged(createAecVector());
        setNormalCursor();
    }
}

/*
 * InOutBox::onAecAutoIrisToggle
 *****************************************************************************/
void InOutBox::onAecAutoIrisToggle(bool checked)
{
    if (checked) {
        // Auto iris
        d_data->m_AecSetup.costAperture = 250;
        d_data->m_AecSetup.costTInt = 250;

        enableCamConfWidgets(!d_data->m_AecSetup.run);

        setWaitCursor();
        auto aecVector = createAecVector();
        emit AecSetupChanged(aecVector);
        emit AecSetupChangedForLensDriver(aecVector);
        setNormalCursor();
    }
}

/*
 * InOutBox::onAecManualIrisToggle
 *****************************************************************************/
void InOutBox::onAecManualIrisToggle(bool checked)
{
    if (checked) {
        // Manual iris
        d_data->m_AecSetup.costAperture = 0;
        d_data->m_AecSetup.costTInt = 250;

        enableCamConfWidgets(!d_data->m_AecSetup.run);

        setWaitCursor();
        auto aecVector = createAecVector();
        emit AecSetupChanged(aecVector);
        emit AecSetupChangedForLensDriver(aecVector);
        setNormalCursor();
    }
}

/*
 * InOutBox::onAecManualShutterToggle
 *****************************************************************************/
void InOutBox::onAecManualShutterToggle(bool checked)
{
    if (checked) {
        // Manual shutter
        d_data->m_AecSetup.costAperture = 250;
        d_data->m_AecSetup.costTInt = 0;

        enableCamConfWidgets(!d_data->m_AecSetup.run);

        setWaitCursor();
        auto aecVector = createAecVector();
        emit AecSetupChanged(aecVector);
        emit AecSetupChangedForLensDriver(aecVector);
        setNormalCursor();
    }
}

/*
 * InOutBox::onLscChange
 *****************************************************************************/
void InOutBox::onLscChange(QVector<uint> values)
{
    if (values.count() == 4) {
        d_data->m_LscSetup.enable = static_cast<bool>(values[0]);

        /* The parameters k, offset and slope have to be converted from
         * Q2.30 fixed point format to float */
        d_data->m_LscSetup.k = static_cast<float>(values[1]) / static_cast<float>(1u << 30u);
        d_data->m_LscSetup.offset = static_cast<float>(values[2]) / static_cast<float>(1u << 30u);
        d_data->m_LscSetup.slope = static_cast<float>(values[3]) / static_cast<float>(1u << 30u);

        updateLscWidgets();
    }
}

/*
 * InOutBox::cbxLscEnableChange
 *****************************************************************************/
void InOutBox::onCbxLscEnableChange(int value)
{
    const bool enable = (value == Qt::Checked);

    d_data->m_LscSetup.enable = enable;

    updateLscWidgets();

    setWaitCursor();
    emit LscChanged(createLscVector());
    setNormalCursor();
}

/*
 * InOutBox::onSldKChange
 *****************************************************************************/
void InOutBox::onSldKChange(int value)
{
    const float sbxValue = static_cast<float>(value) / 100.0f;

    d_data->m_ui->sbxK->blockSignals(true);
    d_data->m_ui->sbxK->setValue(static_cast<double>(sbxValue));
    d_data->m_ui->sbxK->blockSignals(false);

    d_data->m_LscSetup.k = sbxValue;

    if (!d_data->m_ui->sldK->isSliderDown() || (d_data->m_cntEvents++ > d_data->m_maxEvents)) {
        d_data->m_cntEvents = 0;

        setWaitCursor();
        emit LscChanged(createLscVector());
        setNormalCursor();
    }
}

/*
 * InOutBox::onSldKReleased
 *****************************************************************************/
void InOutBox::onSldKReleased()
{
    d_data->m_cntEvents = 0;

    setWaitCursor();
    emit LscChanged(createLscVector());
    setNormalCursor();
}

/*
 * InOutBox::onSbxKChange
 *****************************************************************************/
void InOutBox::onSbxKChange(double value)
{
    const int sldValue = static_cast<int>(value * 100.0);

    d_data->m_ui->sldK->blockSignals(true);
    d_data->m_ui->sldK->setValue(sldValue);
    d_data->m_ui->sldK->blockSignals(false);

    d_data->m_LscSetup.k = static_cast<float>(value);

    setWaitCursor();
    emit LscChanged(createLscVector());
    setNormalCursor();
}

/*
 * InOutBox::onSldOffsetChange
 *****************************************************************************/
void InOutBox::onSldOffsetChange(int value)
{
    auto sbxValue = static_cast<float>(value / 100.0);

    d_data->m_ui->sbxOffset->blockSignals(true);
    d_data->m_ui->sbxOffset->setValue(static_cast<double>(sbxValue));
    d_data->m_ui->sbxOffset->blockSignals(false);

    d_data->m_LscSetup.offset = sbxValue;

    if (!d_data->m_ui->sldOffset->isSliderDown() || (d_data->m_cntEvents++ > d_data->m_maxEvents)) {
        d_data->m_cntEvents = 0;

        setWaitCursor();
        emit LscChanged(createLscVector());
        setNormalCursor();
    }
}

/*
 * InOutBox::onSldOffsetReleased
 *****************************************************************************/
void InOutBox::onSldOffsetReleased()
{
    d_data->m_cntEvents = 0;

    setWaitCursor();
    emit LscChanged(createLscVector());
    setNormalCursor();
}

/*
 * InOutBox::onSbxOffsetChange
 *****************************************************************************/
void InOutBox::onSbxOffsetChange(double value)
{
    const int sldValue = static_cast<int>(value * 100.0);

    d_data->m_ui->sldOffset->blockSignals(true);
    d_data->m_ui->sldOffset->setValue(sldValue);
    d_data->m_ui->sldOffset->blockSignals(false);

    d_data->m_LscSetup.offset = static_cast<float>(value);

    setWaitCursor();
    emit LscChanged(createLscVector());
    setNormalCursor();
}

/*
 * InOutBox::onSldSlopeChange
 *****************************************************************************/
void InOutBox::onSldSlopeChange(int value)
{
    auto sbxValue = static_cast<float>(value / 100.0);

    d_data->m_ui->sbxSlope->blockSignals(true);
    d_data->m_ui->sbxSlope->setValue(static_cast<double>(sbxValue));
    d_data->m_ui->sbxSlope->blockSignals(false);

    d_data->m_LscSetup.slope = sbxValue;

    if (!d_data->m_ui->sldOffset->isSliderDown() || (d_data->m_cntEvents++ > d_data->m_maxEvents)) {
        d_data->m_cntEvents = 0;

        setWaitCursor();
        emit LscChanged(createLscVector());
        setNormalCursor();
    }
}

/*
 * InOutBox::onSldSlopeReleased
 *****************************************************************************/
void InOutBox::onSldSlopeReleased()
{
    d_data->m_cntEvents = 0;

    setWaitCursor();
    emit LscChanged(createLscVector());
    setNormalCursor();
}

/*
 * InOutBox::onSbxSlopeChange
 *****************************************************************************/
void InOutBox::onSbxSlopeChange(double value)
{
    const int sldValue = static_cast<int>(value * 100.0);

    d_data->m_ui->sldSlope->blockSignals(true);
    d_data->m_ui->sldSlope->setValue(sldValue);
    d_data->m_ui->sldSlope->blockSignals(false);

    d_data->m_LscSetup.slope = static_cast<float>(value);

    setWaitCursor();
    emit LscChanged(createLscVector());
    setNormalCursor();
}
