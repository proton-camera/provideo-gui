/*
 * Copyright (C) 2017 Dream Chip Technologies GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************/
/**
 * @file    gammabox.cpp
 *
 * @brief   Implementation of gamma setting box
 *
 *****************************************************************************/
#include <QtDebug>
#include <QMessageBox>

#include <simple_math/float.h>

#include "knee_interpolation.h"
#include "kneebox.h"
#include "ui_kneebox.h"

/*
 * namespaces
 *****************************************************************************/
namespace Ui {
class UI_KneeBox;
}

/*
 * local definitions
 *****************************************************************************/
#define KNEE_CURVE_ID (0)
#define KNEE_POINT_CURVE_ID (1)

/*
 * Settings
 *****************************************************************************/
#define KNEE_SETTINGS_SECTION_NAME ("KNEE")
#define KNEE_SETTINGS_ENABLE ("enable")
#define KNEE_SETTINGS_POINT ("point")
#define KNEE_SETTINGS_SLOPE ("slope")

/*
 * KneeBox::PrivateData
 *****************************************************************************/
class KneeBox::PrivateData
{
public:
    PrivateData(QWidget *parent) : m_ui(new Ui::UI_KneeBox), m_interpolate(new KneeInterpolation)
    {
        // initialize UI
        m_ui->setupUi(parent);

        // kneepoint
        m_ui->KneePoint->setRange(KNEE_POINT_MIN, KNEE_POINT_MAX);
        m_ui->KneePoint->setCommaPos(KNEE_POINT_COMMA_POSITION);
        m_ui->KneePoint->setDisplayMultiplier(KNEE_POINT_DISPLAY_MULTIPLIER);
        m_ui->KneePoint->setBase(KNEE_POINT_BASE);
        m_ui->KneePoint->setMaxAngle(180);
        m_ui->KneePoint->setMaxRounds(1);
        m_ui->KneePoint->setFmt(KNEE_POINT_DISPLAY_MASK);
        m_ui->KneePoint->setMaxEvent(1);

        // kneeslope
        m_ui->KneeSlope->setRange(KNEE_SLOPE_MIN, KNEE_SLOPE_MAX);
        m_ui->KneeSlope->setCommaPos(KNEE_SLOPE_COMMA_POSITION);
        m_ui->KneeSlope->setDisplayMultiplier(KNEE_SLOPE_DISPLAY_MULTIPLIER);
        m_ui->KneeSlope->setBase(KNEE_SLOPE_BASE);
        m_ui->KneeSlope->setMaxAngle(180);
        m_ui->KneeSlope->setMaxRounds(1);
        m_ui->KneeSlope->setFmt(KNEE_SLOPE_DISPLAY_MASK);
        m_ui->KneeSlope->setMaxEvent(1);

        // knee-plot
        initKneePlot(m_ui->KneePlot);
        QLinearGradient axisRectGradient;
        axisRectGradient.setStart(0, 0);
        axisRectGradient.setFinalStop(0, 350);
        axisRectGradient.setColorAt(0, QColor(120, 120, 120));
        axisRectGradient.setColorAt(1, QColor(48, 48, 48));
        m_ui->KneePlot->axisRect()->setBackground(axisRectGradient);
    };

    ~PrivateData()
    {
        delete m_ui;
        delete m_interpolate;
    };

    void initKneePlot(QCustomPlot *plot)
    {
        // knee curve
        QPen penKnee(Qt::white);
        plot->addGraph();
        plot->graph(KNEE_CURVE_ID)->setPen(penKnee);

        // knee point curve
        QPen penKneePoint(Qt::cyan);
        plot->addGraph();
        plot->graph(KNEE_POINT_CURVE_ID)->setPen(penKneePoint);

        plot->setBackground(QBrush(QColor(48, 47, 47)));
        plot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);
        plot->axisRect()->setupFullAxesBox(true);
        plot->xAxis->setLabel("");
        plot->xAxis->setLabelColor(QColor(177, 177, 177));
        plot->yAxis->setLabel("");
        plot->yAxis->setLabelColor(QColor(177, 177, 177));
        plot->xAxis->setRange(0.0, 1.0);
        plot->yAxis->setRange(0.0, 1.05);
        plot->xAxis->setBasePen(QPen(QColor(177, 177, 177), 1));
        plot->yAxis->setBasePen(QPen(QColor(177, 177, 177), 1));
        plot->xAxis->setTickPen(QPen(QColor(177, 177, 177), 1));
        plot->yAxis->setTickPen(QPen(QColor(177, 177, 177), 1));
        plot->xAxis->setSubTickPen(QPen(QColor(177, 177, 177), 1));
        plot->yAxis->setSubTickPen(QPen(QColor(177, 177, 177), 1));
        plot->xAxis->setTickLabelColor(QColor(177, 177, 177));
        plot->yAxis->setTickLabelColor(QColor(177, 177, 177));

        plot->xAxis2->setBasePen(QPen(QColor(177, 177, 177), 1));
        plot->yAxis2->setBasePen(QPen(QColor(177, 177, 177), 1));
        plot->xAxis2->setTickPen(QPen(QColor(177, 177, 177), 1));
        plot->yAxis2->setTickPen(QPen(QColor(177, 177, 177), 1));
        plot->xAxis2->setSubTickPen(QPen(QColor(177, 177, 177), 1));
        plot->yAxis2->setSubTickPen(QPen(QColor(177, 177, 177), 1));
        plot->xAxis2->setTickLabelColor(QColor(177, 177, 177));
        plot->yAxis2->setTickLabelColor(QColor(177, 177, 177));
    }

    void setKneeConfig(int point, int slope)
    {
        // Range check parameters
        if (point < KNEE_POINT_MIN || point > KNEE_POINT_MAX || slope < KNEE_SLOPE_MIN
            || slope > KNEE_SLOPE_MAX) {
            QMessageBox msg;
            msg.setIcon(QMessageBox::Warning);
            msg.setWindowTitle("Unsopperted Knee Function");
            msg.setText("The knee function in your camera is not compatible with this version of "
                        "the ProVideo GUI.\n\n"
                        "Upate the camera to the latest firmware to use the knee function or use "
                        "an older GUI version.");
            msg.exec();
            return;
        }

        m_ui->KneePlot->graph(KNEE_CURVE_ID)->data()->clear();
        m_ui->KneePlot->graph(KNEE_POINT_CURVE_ID)->data()->clear();

        QVector<double> x(1024);
        QVector<double> y(1024);
        m_interpolate->setConfig(point, slope);
        for (int i = 0; i < 1024; i += 1) {
            x[i] = ((double)(i << 2)) / (double)(1 << CFG_WDR_INPUT_WIDTH);
            y[i] = ((double)m_interpolate->interpolate((i << 2))
                    / (double)(1 << CFG_WDR_GAIN_COMMA))
                    * x[i];

            // Clip y to 1.0
            if (y[i] > 1.0) {
                y[i] = 1.0;
            }
        }

        QVector<double> x1(2);
        QVector<double> y1(2);
        x1[0] = 0.0;
        x1[1] = 1.0;
        y1[0] = y1[1] = fastpow(((((double)point / 100.0) + 0.099) / 1.099), (1.0 / 0.45));

        m_ui->KneePlot->graph(KNEE_CURVE_ID)->setData(x, y);
        m_ui->KneePlot->graph(KNEE_POINT_CURVE_ID)->setData(x1, y1);

        m_ui->KneePlot->replot();
    }

    Ui::UI_KneeBox *m_ui;
    KneeInterpolation *m_interpolate; /**< interpolation class */
};

/*
 * KneeBox::KneeBox
 *****************************************************************************/
KneeBox::KneeBox(QWidget *parent) : DctWidgetBox(parent), d_data(new PrivateData(this))
{
    // signal transition
    connect(d_data->m_ui->cbxEnable, SIGNAL(stateChanged(int)), this,
            SLOT(onKneeEnableChange(int)));

    connect(d_data->m_ui->KneePoint, SIGNAL(ValueChanged(int)), this, SLOT(onKneePointChange(int)));
    connect(d_data->m_ui->KneeSlope, SIGNAL(ValueChanged(int)), this, SLOT(onKneeSlopeChange(int)));
}

/*
 * KneeBox::~KneeBox
 *****************************************************************************/
KneeBox::~KneeBox()
{
    delete d_data;
}

/*
 * KneeBox::KneeEnable
 *****************************************************************************/
bool KneeBox::KneeEnable() const
{
    return (d_data->m_ui->cbxEnable->isChecked());
}

/*
 * KneeBox::setKneeEnable
 *****************************************************************************/
void KneeBox::setKneeEnable(const bool enable)
{
    d_data->m_ui->cbxEnable->setCheckState(enable ? Qt::Checked : Qt::Unchecked);
}

/*
 * KneeBox::KneePoint
 *****************************************************************************/
int KneeBox::KneePoint() const
{
    return (d_data->m_ui->KneePoint->value());
}

/*
 * KneeBox::setKneePoint
 *****************************************************************************/
void KneeBox::setKneePoint(const int value)
{
    d_data->setKneeConfig(value, KneeSlope());
    if (KneeEnable()) {
        emit KneeConfigChanged((int)KneeEnable(), value, KneeSlope());
    }
}

/*
 * KneeBox::KneeSlope
 *****************************************************************************/
int KneeBox::KneeSlope() const
{
    return (d_data->m_ui->KneeSlope->value());
}

/*
 * KneeBox::setKneeSlope
 *****************************************************************************/
void KneeBox::setKneeSlope(const int value)
{
    d_data->setKneeConfig(KneePoint(), value);
    if (KneeEnable()) {
        emit KneeConfigChanged((int)KneeEnable(), KneePoint(), value);
    }
}

/*
 * KneeBox::prepareMode
 *****************************************************************************/
void KneeBox::prepareMode(const Mode)
{
    // @TODO do nothing here
}

/*
 * KneeBox::loadSettings (no looping over channels)
 *****************************************************************************/
void KneeBox::loadSettings(QSettings &s)
{
    s.beginGroup(KNEE_SETTINGS_SECTION_NAME);
    setKneeEnable(s.value(KNEE_SETTINGS_ENABLE).toBool());
    setKneePoint(s.value(KNEE_SETTINGS_POINT).toInt());
    setKneeSlope(s.value(KNEE_SETTINGS_SLOPE).toInt());
    s.endGroup();
}

/*
 * KneeBox::saveSettings (no looping over channels)
 *****************************************************************************/
void KneeBox::saveSettings(QSettings &s)
{
    s.beginGroup(KNEE_SETTINGS_SECTION_NAME);
    s.setValue(KNEE_SETTINGS_ENABLE, KneeEnable());
    s.setValue(KNEE_SETTINGS_POINT, KneePoint());
    s.setValue(KNEE_SETTINGS_SLOPE, KneeSlope());
    s.endGroup();
}

/*
 * KneeBox::applySettings (no looping over channels)
 *****************************************************************************/
void KneeBox::applySettings(void)
{
    emit KneeConfigChanged((int)KneeEnable(), KneePoint(), KneeSlope());
}

/*
 * KneeBox::onKneeChange
 *****************************************************************************/
void KneeBox::onKneeConfigChange(int enable, int point, int slope)
{
    d_data->m_ui->cbxEnable->blockSignals(true);
    d_data->m_ui->cbxEnable->setCheckState(enable ? Qt::Checked : Qt::Unchecked);
    d_data->m_ui->cbxEnable->blockSignals(false);

    d_data->m_ui->KneePoint->blockSignals(true);
    d_data->m_ui->KneePoint->setValue(point);
    d_data->m_ui->KneePoint->blockSignals(false);

    d_data->m_ui->KneeSlope->blockSignals(true);
    d_data->m_ui->KneeSlope->setValue(slope);
    d_data->m_ui->KneeSlope->blockSignals(false);

    d_data->setKneeConfig(point, slope);
}

/*
 * KneeBox::onKneeEnableChange
 *****************************************************************************/
void KneeBox::onKneeEnableChange(int)
{
    setWaitCursor();
    emit KneeConfigChanged((int)KneeEnable(), KneePoint(), KneeSlope());
    setNormalCursor();
}

/*
 * KneeBox::onKneePointChange
 *****************************************************************************/
void KneeBox::onKneePointChange(int value)
{
    d_data->setKneeConfig(value, KneeSlope());
    if (KneeEnable()) {
        setWaitCursor();
        emit KneeConfigChanged((int)KneeEnable(), value, KneeSlope());
        setNormalCursor();
    }
}

/*
 * KneeBox::onKneeSlopeChange
 *****************************************************************************/
void KneeBox::onKneeSlopeChange(int value)
{
    d_data->setKneeConfig(KneePoint(), value);
    if (KneeEnable()) {
        setWaitCursor();
        emit KneeConfigChanged((int)KneeEnable(), KneePoint(), value);
        setNormalCursor();
    }
}
