set

PATH=C:\Qt\5.12.1\mingw730_64\bin;%PATH%
PATH=C:\Qt\Tools\mingw730_64\bin;%PATH%

rem Build application
mkdir build
cd build
qmake ..\application.pro
mingw32-make
cd ..

rem Copy files to deploy folder
mkdir deploy
copy build\release\*.exe deploy
mkdir deploy\tools_and_configs
copy tools_and_configs\cygwin1.dll deploy\tools_and_configs
copy tools_and_configs\flashloader.exe deploy\tools_and_configs
copy tools_and_configs\SupportedLenses.txt deploy\tools_and_configs

	
rem Run qt deploy to copy needed DLLs
windeployqt deploy

